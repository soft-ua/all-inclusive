<?php

use yii\db\Migration;

class m160630_111930_insert_admin_into_user extends Migration
{
    public function up()
    {
        $this->insert('user', [
            'id' => '1',
            'username' => 'admin',
            'password_hash' => '$2y$13$7QEY9Bo4P/rvp6kCbluwwujV7msROs2leE6Leua/qFwytp8QHzn.G', //password = "adminadmin"
            'email' => 'admin@test.ru',
            'created_at' => $date_created = time(),
            'updated_at' => $date_created,
        ]);
    }

    public function down()
    {
        $this->delete('user', ['id' => 1]);

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
