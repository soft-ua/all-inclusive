<?php

use yii\db\Migration;

class m160324_104616_tbl_donate extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('donate', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
            'description' => $this->text()->notNull(),
            'str_imgs_donates' => $this->text(),
            'img_donates' => $this->string(255),
            'office_donate' => $this->string(255),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'available_d' => $this->boolean(),
            'user_id_donates' => $this->integer(11),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('donate');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
