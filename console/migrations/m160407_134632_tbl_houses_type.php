<?php

use yii\db\Migration;

class m160407_134632_tbl_houses_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('houses_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
        ], $tableOptions);

        $this->insert('houses_type', [
            'id' => '1',
            'name' => 'Готовые дома к продаже',
        ]);

        $this->insert('houses_type', [
            'id' => '2',
            'name' => '	Реализованные проекты',
        ]);

    }

    public function down()
    {
        $this->dropTable('houses_type');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
