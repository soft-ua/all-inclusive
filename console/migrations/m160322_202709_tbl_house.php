<?php

use yii\db\Migration;

class m160322_202709_tbl_house extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('house', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'price' => $this->integer(11),
            'address' => $this->string(255)->notNull(),
            'tour_link' => $this->string(255),
            'str_imgs_house' => $this->text(),
            'img_house' => $this->string(255),
            'description' => $this->text()->notNull(),
            'location' => $this->string(100)->notNull(),
            'type' => $this->smallInteger(5)->notNull(),
            'available' => $this->boolean(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
        ], $tableOptions);

        $this->createIndex('fk_house_user_idx', 'house', 'user_id');

        $this->addForeignKey("fk_house_user", "house", "user_id", "user", "id");
//        $this->addForeignKey("fk_house_area", "house", "area_id", "area", "id");
//        $this->addForeignKey("fk_house_slider", "house", "slider_id", "slider", "id");
    }

    public function down()
    {
        $this->dropTable('house');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
