<?php

use yii\db\Migration;

class m160409_170402_tbl_comment extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('comment', [
            'id_comment' => $this->primaryKey(),
            'author_id' => $this->integer(11)->notNull(),
            'comment_img' => $this->string(255)->notNull(),
            'age' => $this->integer(5),
            'guest_name' => $this->string(50)->notNull(),
            'guest_email' => $this->string(100)->notNull(),
            'text_comment' => $this->text()->notNull(),
            'available_comment' => $this->boolean(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ], $tableOptions);

        $this->createIndex('fk_comment_user1_idx', 'comment', 'author_id');

//        $this->addForeignKey("fk_comment_user1", "comment", "author_id", "user", "id");
    }

    public function down()
    {
        $this->dropTable('comment');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
