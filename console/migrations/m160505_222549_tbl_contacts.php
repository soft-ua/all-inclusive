<?php

use yii\db\Migration;

class m160505_222549_tbl_contacts extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('contacts_info', [
            'idcontacts' => $this->primaryKey(),
            'email' => $this->string(100)->notNull(),
            'skype' => $this->string(100),
            'phone' => $this->string(100)->notNull(),
            'address' => $this->string(255)->notNull(),
            'working_hours' => $this->string(255)->notNull(),
            'yandexmaps' => $this->string(255)->notNull(),
        ], $tableOptions);

        $this->insert('contacts_info', [
            'idcontacts' => '1',
            'email' => 'allinclusive@gmail.com',
            'phone' => '+7 (495) 255-51-91',
            'address' => 'Московская область, г. Чехов, Симферопольское ш., д. 4',
            'working_hours' => 'с 9-00 до 20-00 без обеда и выходных',
            'yandexmaps' => '55.1647,37.4664',
        ]);
    }

    public function down()
    {
        $this->dropTable("contacts_info");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
