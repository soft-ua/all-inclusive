<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'resource/css/backstyles.css',
//        'wysiwyg/colorpicker.css',
//        'wysiwyg/font-awesome.css',
//        'wysiwyg/summernote.css',
        'wysiwyg/ckeditor/_all-skins.min.css',
        'resource/css/jquery.fancybox.css',
//        'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
//        'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
    ];
    public $js = [
//        'resource/js/yandex.api2.js',
//        'resource/js/ya.map.js',
//        'wysiwyg/jquery.slimscroll.min.js',
//        'wysiwyg/waves.min.js',
//        'wysiwyg/summernote.min.js',
//        'wysiwyg/bootstrap-colorpicker.js',
//        'wysiwyg/form-elements.js',
        'wysiwyg/ckeditor/fastclick.min.js',
//        'wysiwyg/ckeditor/app.min.js',
        'wysiwyg/ckeditor/demo.js',
        'wysiwyg/ckeditor/ckeditor.js',
        'wysiwyg/ckeditor/bootstrap3-wysihtml5.all.min.js',
        '/resource/js/jquery.fancybox.pack.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = [
        'position' =>  View::POS_HEAD,
    ];

}
