<?php

namespace backend\controllers;

use common\controllers\AuthController;
use common\models\User;
use frontend\models\ChangePasswordForm;
use yii\web\Controller;
use Yii;
use yii\web\UploadedFile;
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\imagine\Image;

class DefaultController extends AuthController
{

    public function actionIndex()
    {
        return $this->render('index');
    }


    public function uploadAvatar(){
//        if(Yii::$app->request->isPost){
//            $id = Yii::$app->user->id;
//            $path = Yii::getAlias("@frontend/web/uploads/users");
//            $file = UploadedFile::getInstanceByName('avatar');
////            $file->permissions = '0777';
////            var_dump($file);die;
//            if($file) {
//                $name = $id.'.jpg';
//                $file->saveAs($path . DIRECTORY_SEPARATOR . $name);
//
//
//                $image = $path . DIRECTORY_SEPARATOR . $name;
//                $new_name = $path . DIRECTORY_SEPARATOR . "small_" . $name;
//
//                Image::frame($image, 0, '666', 0)
//                    ->thumbnail(new Box(200, 200))
//                    ->save($new_name, ['quality' => 100]);
//                $this->refresh();
//
//                return true;
//            }
//            $this->refresh();
//        }
        if(Yii::$app->request->isPost){

//            var_dump($_POST);
//            var_dump($_FILES);
//            die;

            $id = Yii::$app->user->id;
            $path = Yii::getAlias("@frontend/web/uploads/users");
            $file = UploadedFile::getInstanceByName('avatar');
            if($file) {
                $name = $id . '.jpg';
                $file->saveAs($path . DIRECTORY_SEPARATOR . $name);


                $image = $path . DIRECTORY_SEPARATOR . $name;
                $new_name = $path . DIRECTORY_SEPARATOR . "small_" . $name;

                $size = getimagesize($image);
                $width = $size[0];
                $height = $size[1];


                $ratio = round($width/$height, 3);

                if($ratio>1){
                    $height = 241;
                    $width = $height*$ratio;
                    $xW = ($width-241)/2;
                    \yii\imagine\Image::frame($image, 0, '666', 0)
                        ->resize(new Box($width, $height))
                        ->crop(new Point($xW, 0), new Box(241, $height))
                        ->save($new_name, ['quality' => 100]);
                }elseif($ratio<1){
                    $width = 241;
                    $height = $width/$ratio;
                    $hW = ($height-241)/2;
                    \yii\imagine\Image::frame($image, 0, '666', 0)
                        ->resize(new Box($width, $height))
                        ->crop(new Point(0, 0), new Box($width, 241))
                        ->save($new_name, ['quality' => 100]);
                }else{
                    \yii\imagine\Image::frame($image, 0, '666', 0)
                        ->crop(new Point(0, 0), new Box($width, $height))
                        ->resize(new Box(241,241))
                        ->save($new_name, ['quality' => 100]);
                }
                return true;
            }
        }

    }

    public function actionChangePassword(){

        $model = new ChangePasswordForm();
        if($model->load(\Yii::$app->request->post()) && $model->changepassword()){

            $this->refresh();

        }
        if($model->load(\Yii::$app->request->post())){
            Yii::$app->session->setFlash('success', 'Вы успешно изменили пароль!');
        }
        return $this->render('change-password',['model' => $model]);
    }

    public function actionSettings(){

        $model = User::findOne(\Yii::$app->user->id);
        $model->scenario = 'setting';


        if($model->load(\Yii::$app->request->post())){
            if ($model->load(\Yii::$app->request->post()) && $model->save()) {
                $this->uploadAvatar();
//            $this->refresh();

            }
            Yii::$app->session->setFlash('success', 'Ваши данные успешно обновлены! Страница будет перезагружена через 3 секунды');
//            sleep(3);
//            $this->redirect('settings');
        }
        return $this->render('setting',['model' => $model]);

    }
}