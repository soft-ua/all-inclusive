<?php

namespace backend\controllers;

use common\controllers\AuthController;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\NotFoundHttpException;

class UserController extends AuthController{

    public function actionIndex(){

        \Yii::$app->view->title = "User Manager";
        $dataProvider = new ActiveDataProvider([
            'query' => User::find()
        ]);

        return $this->render("index", ['dataProvider' => $dataProvider]);
    }
//
//    public function actionDelete($id)
//    {
//        $model = $this->findModel($id);
//
//        $query = new Query();
//        $modelImageMain = $query->from('house')
//            ->innerJoin('image', 'house.id = image.entity_id')
//            ->where("image.entity_id = :id",[':id'=>$id])
//            ->andWhere('image.main = true')
//            ->andWhere('house.type = 1')
//            ->all();
////
////
////        $modelImage = new Query();
////        $resultImage = $modelImage->from('user')
////            ->innerJoin('image', 'user.id = user_id')
////            ->where('user_id = :id',[':id' => $id])
////            ->all();
////
////        $modelHouse = new Query();
////        $resultHouse = $modelHouse->from('user')
////            ->innerJoin('house', 'user.id = user_id')
////            ->where('user_id = :id',[':id' => $id])
////            ->all();
////
////        $modelDonatesImg = new Query();
////        $resultDonatesImg = $modelDonatesImg->from('user')
////            ->innerJoin('donates_img', 'user.id = user_id')
////            ->where('user_id = :id',[':id' => $id])
////            ->all();
////
////        var_dump($modelImageMain);
////        var_dump($resultImage);
////        var_dump($resultHouse);
////        var_dump($resultDonatesImg);
////        die;
//
////        if($resultImage != null || $resultHouse != null || $resultDonatesImg != null){
//////            if($model->load(\Yii::$app->request->post())){
////                \Yii::$app->session->setFlash('error','У пользователя есть связанные злементы. Удалите сначала их!');
//////            }
////        }else{
////            \Yii::$app->session->setFlash('success','Пользователь был удален!');
////
////            $model->delete();
////        }
//
//        $model->delete();
//
//        return $this->redirect(['index']);
//    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запращиваемой страницы не существует.');
        }
    }


}