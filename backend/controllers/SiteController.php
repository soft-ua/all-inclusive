<?php
namespace backend\controllers;

use common\models\Comment;
use common\models\Donate;
use common\models\House;
use common\models\OurTeam;
use common\models\User;
//use common\models\Image;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        Yii::$app->view->registerJsFile('/resource/js/Chart.min.js',['position' => \yii\web\View::POS_END]);

        $modelHouse = House::find()->all();

        $modelHouseFive = House::find()
//            ->innerJoin('image', 'house.id = image.entity_id')
            ->limit(6)
            ->orderBy('house.id DESC')
            ->all();

        $modelReadyHouses = House::find()->where('type = 1')->all();

        $modelProjects = House::find()->where('type = 2')->all();

//        $modelImage = Image::find()->all();

        $modelUser = User::find()->all();

        $modelDonate = Donate::find()->all();

        $modelComment = Comment::find()->all();

        $modelCommentFive = Comment::find()
            ->limit(5)
            ->orderBy('id_comment DESC')
            ->all();

        $modelTeam = OurTeam::find()->all();

        $modelTeamEight = OurTeam::find()
            ->limit(4)
            ->orderBy('id_team DESC')
            ->all();

        return $this->render('index',[
            'modelHouse' => $modelHouse,
            'modelHouseFive' => $modelHouseFive,
            'modelReadyHouses' => $modelReadyHouses,
            'modelProjects' => $modelProjects,
//            'modelImage' => $modelImage,
            'modelUser' => $modelUser,
            'modelDonate' => $modelDonate,
            'modelComment' => $modelComment,
            'modelCommentFive' => $modelCommentFive,
            'modelTeam' => $modelTeam,
            'modelTeamEight' => $modelTeamEight,
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}