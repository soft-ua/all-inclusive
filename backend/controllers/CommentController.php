<?php

namespace backend\controllers;

use Yii;
use common\models\Comment;
use common\models\User;
use common\models\Search\CommentSearch;
use yii\helpers\BaseFileHelper;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use Imagine\Image\Point;
use Imagine\Image\Box;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Comment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);



        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Comment model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Comment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Comment();
//        $modelId = Comment::find()
//            ->orderBy('id_comment DESC')
//            ->limit(1)
//            ->all();
//        if($modelId != null){
////            var_dump($modelId);die;
//            foreach($modelId as $row):
//            $id = $row['id_comment'];
//            endforeach;
//        }else{
////            var_dump('hello');die;
//            $id = 1;
//        }
//        var_dump($id);die;

//        var_dump($id[0]['id_comment']);die;

        if($model->load(\Yii::$app->request->post()) && $model->save()){
//            UploadedFile::getInstance($model, 'comment_img');
//            $this->uploadAvatar(time(),$id);

            if(Yii::$app->request->isPost){
                $path = Yii::getAlias("@frontend/web/uploads/comment/".date('d.m.Y_H-i-s',$model['created_at']));
                BaseFileHelper::createDirectory($path);
                $file = UploadedFile::getInstance($model,'comment_img');

                if($file) {
                    $name = $this->actionTransliterate($file->baseName).'.'.$file->extension;
                    $file->saveAs($path . DIRECTORY_SEPARATOR . $name);

                    $model->comment_img = $name;
                    $model->save();

                    $image = $path . DIRECTORY_SEPARATOR . $name;
                    $new_name = $path . DIRECTORY_SEPARATOR . "small_" . $name;

//                    Image::frame($image, 0, '666', 0)
//                        ->thumbnail(new Box(500, 500))
//                        ->save($new_name, ['quality' => 100]);

                    $size = getimagesize($image);
                    $width = $size[0];
                    $height = $size[1];

                    $ratio = round($width/$height, 3);

//                    var_dump($ratio);die;

                    if($ratio>1){
                        $height = 500;
                        $width = $height*$ratio;
                        $xW = ($width-500)/2;
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->resize(new Box($width, $height))
                            ->crop(new Point($xW, 0), new Box(500, $height))
                            ->save($new_name, ['quality' => 100]);
                    }elseif($ratio<1){
                        $width = 500;
                        $height = $width / $ratio;
                        $heightSec = $width / 1.42;
                        $hW = ($height - $heightSec) / 2;
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->resize(new Box($width, $height))
                            ->crop(new Point(0, 0), new Box($width, 500))
                            ->save($new_name, ['quality' => 100]);
                    }else{
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->crop(new Point(0, 0), new Box($width, $height))
                            ->resize(new Box(500,500))
                            ->save($new_name, ['quality' => 100]);
                    }

                    return $this->redirect(['index']);
                    return true;
                }
            }
            $this->refresh();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Comment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        $before_img = $model->comment_img;

        $path = Yii::getAlias("@frontend/web/uploads/comment/".date('d.m.Y_H-i-s',$model->created_at));

            if ($model->load(\Yii::$app->request->post()) && $model->save()) {
//            $this->uploadAvatar($model->created_at,$id);

//                var_dump(\Yii::$app->request->post());die;

                if (Yii::$app->request->isPost) {
                    $path_dir = Yii::getAlias("@frontend/web/images/main/watermark/");
                    $allink_01 = $path_dir . "allink_01.png";
                    $path = Yii::getAlias("@frontend/web/uploads/comment/" . date('d.m.Y_H-i-s', $model->created_at));
                    BaseFileHelper::createDirectory($path);

                    $file = UploadedFile::getInstance($model, 'comment_img');

                    if(is_dir($path) && $file != null){

                        try {
                            if(is_dir($path)) {
                                $files = \yii\helpers\FileHelper::findFiles($path);

                                foreach ($files as $fileIn) {
                                    unlink($fileIn);
                                }
                            }
                        }
                        catch(\yii\base\Exception $e){}

                    }

//                    var_dump($file);die;

                    if ($file) {
                        $name = $this->actionTransliterate($file->baseName).'.'.$file->extension;

                        $file->saveAs($path . DIRECTORY_SEPARATOR . $name);

                        $model->comment_img = $name;
                        $model->save();

                        $image = $path . DIRECTORY_SEPARATOR . $name;
                        $new_name = $path . DIRECTORY_SEPARATOR . "small_" . $name;

//                        Image::frame($image, 0, '666', 0)
//                            ->thumbnail(new Box(500, 500))
//                            ->save($new_name, ['quality' => 100]);

                        $size = getimagesize($image);
                        $width = $size[0];
                        $height = $size[1];

                        $ratio = round($width/$height, 3);

//                    var_dump($ratio);die;

                        if($ratio>1){
                            $height = 500;
                            $width = $height*$ratio;
                            $xW = ($width-500)/2;
                            \yii\imagine\Image::frame($image, 0, '666', 0)
                                ->resize(new Box($width, $height))
                                ->crop(new Point($xW, 0), new Box(500, $height))
                                ->save($new_name, ['quality' => 100]);
                        }elseif($ratio<1){
                            $width = 500;
                            $height = $width / $ratio;
                            $heightSec = $width / 1.42;
                            $hW = ($height - $heightSec) / 2;
                            \yii\imagine\Image::frame($image, 0, '666', 0)
                                ->resize(new Box($width, $height))
                                ->crop(new Point(0, 0), new Box($width, 500))
                                ->save($new_name, ['quality' => 100]);
                        }else{
                            \yii\imagine\Image::frame($image, 0, '666', 0)
                                ->crop(new Point(0, 0), new Box($width, $height))
                                ->resize(new Box(500,500))
                                ->save($new_name, ['quality' => 100]);
                        }


//                        try {
//                            if(is_dir($path)) {
//
//                                if (is_file($path.'/'.$name) && !is_file($path.'/wtmarked_'.($name))) {
//                                    $images_arr[] = ($name);
//                                    $image = ''.($name);
//                                    $pos = mb_strripos($name, '.');
//                                    $new_str = mb_substr($name, $pos);
//
//                                    $size = getimagesize($path.'/'.$name);
//                                    $width = $size[0];
//                                    $height = $size[1];
//
//                                    $sizeWMark = getimagesize($allink_01);
//                                    $widthWMark = $sizeWMark[0];
//                                    $heightWMark = $sizeWMark[1];
//
//
//
//                                    if($width <= 5000 && $height <= 5000){
//
//                                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                                        $new_name = $path. '/' .time()."_allink.png";
//
//
//                                        //открываем исходное изображение
//                                        $src = imagecreatefrompng($allink_01);
//
//                                        //создаем дескриптор для измененного изображения
//                                        //                        $dst = imagecreatetruecolor($width, $height);
//                                        if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                            $dst = imagecreatefromjpeg($path.'/'.$image);
//                                        }elseif($new_str == '.png'){
//                                            $dst = imagecreatefrompng($path.'/'.$image);
//                                        }elseif($new_str == '.gif'){
//                                            $dst = imagecreatefromgif($path.'/'.$image);
//                                        }
//
//                                        //устанавливаем прозрачность
//                                        \frontend\components\Common::setTransparency($dst, $src);
//
//                                        //изменяем размер
//                                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $height);
//
//                                        //сохраняем уменьшенное изображение в файл
//                                        ImagePNG($dst, $new_name);
//
//                                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                                        //закрываем дескрипторы исходного и уменьшенного изображений
//                                        ImageDestroy($src);
//                                        ImageDestroy($dst);
//
//                                        unlink($new_name);
//
//                                    }elseif($width > 5000 && $height <= 5000){
//
//                                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                                        $new_name = $path. '/' .time()."_allink.png";
//
//
//                                        $src = imagecreatefrompng($allink_01);
//
//                                        if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                            $dst = imagecreatefromjpeg($path.'/'.$image);
//                                        }elseif($new_str == '.png'){
//                                            $dst = imagecreatefrompng($path.'/'.$image);
//                                        }elseif($new_str == '.gif'){
//                                            $dst = imagecreatefromgif($path.'/'.$image);
//                                        }
//
//                                        \frontend\components\Common::setTransparency($dst, $src);
//
//                                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $height);
//
//                                        ImagePNG($dst, $new_name);
//
//                                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                                        ImageDestroy($src);
//                                        ImageDestroy($dst);
//
//                                        unlink($new_name);
//
//                                    }elseif($width <= 5000 && $height > 5000){
//
//                                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                                        $new_name = $path. '/' .time()."_allink.png";
//
//
//                                        $src = imagecreatefrompng($allink_01);
//
//                                        if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                            $dst = imagecreatefromjpeg($path.'/'.$image);
//                                        }elseif($new_str == '.png'){
//                                            $dst = imagecreatefrompng($path.'/'.$image);
//                                        }elseif($new_str == '.gif'){
//                                            $dst = imagecreatefromgif($path.'/'.$image);
//                                        }
//
//                                        \frontend\components\Common::setTransparency($dst, $src);
//
//                                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $heightWMark);
//
//                                        ImagePNG($dst, $new_name);
//
//                                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                                        ImageDestroy($src);
//                                        ImageDestroy($dst);
//
//                                        unlink($new_name);
//
//                                    }else{
//
//                                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                                        $new_name = $path. '/' .time()."_allink.png";
//
//
//                                        $src = imagecreatefrompng($allink_01);
//
//                                        if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                            $dst = imagecreatefromjpeg($path.'/'.$image);
//                                        }elseif($new_str == '.png'){
//                                            $dst = imagecreatefrompng($path.'/'.$image);
//                                        }elseif($new_str == '.gif'){
//                                            $dst = imagecreatefromgif($path.'/'.$image);
//                                        }
//
//                                        \frontend\components\Common::setTransparency($dst, $src);
//
//                                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $heightWMark);
//
//                                        ImagePNG($dst, $new_name);
//
//                                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                                        ImageDestroy($src);
//                                        ImageDestroy($dst);
//
//                                        unlink($new_name);
//
//                                    }
//                                }
//                            }
//                        }
//                        catch(\yii\base\Exception $e){}


                        return $this->redirect(['index']);
//                        return true;
                    }
                }

                $this->refresh();
            }

        $image = [];
        if($name = $model->comment_img){
            $image[] =  '<img src="//'.\Yii::$app->params['baseUrl'].'/uploads/comment/' . $model->id_comment .'/'. $name . '">';
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->comment_img == ''){
                $model->comment_img = $before_img;
                $model->save();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'image' => $image,
            ]);
        }
    }

    /**
     * Deletes an existing Comment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($path = Yii::getAlias("@frontend/web/uploads/comment/".date('d.m.Y_H-i-s',$model->created_at))){
            BaseFileHelper::removeDirectory($path);
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }

    function actionTransliterate($string) {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
            ' ' => '_',
        );
        return strtr($string, $converter);
    }
}
