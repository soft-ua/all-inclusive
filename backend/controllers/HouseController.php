<?php

namespace backend\controllers;

use common\controllers\AuthController;
use common\models\User;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\ImageInterface;
use Yii;
use common\models\House;
use common\models\Search\HouseSearch;
use yii\db\Query;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * HouseController implements the CRUD actions for House model.
 */
class HouseController extends AuthController
{


    public function init(){
//        Yii::$app->view->registerJsFile('https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU',['position' => \yii\web\View::POS_HEAD]);
//        Yii::$app->view->registerJsFile('http://maps.googleapis.com/maps/api/js?sensor=false',['position' => \yii\web\View::POS_HEAD]);


        Yii::$app->view->registerJsFile('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU',['position' => \yii\web\View::POS_HEAD]);
        Yii::$app->view->registerJsFile('/resource/js/ya.map.house.js',['position' => \yii\web\View::POS_HEAD]);
    }

    /**
     * Lists all House models.
     * @return mixed
     */
    public function actionIndex()
    {
        $housetype = \common\models\HousesType::find()->all();
        $housestype = \yii\helpers\ArrayHelper::map($housetype,'id','name');

        $searchModel = new HouseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'housestype' => $housestype,
        ]);
    }


    /**
     * Creates a new House model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
//        Yii::$app->view->registerJsFile('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU',['position' => \yii\web\View::POS_HEAD]);
//        Yii::$app->view->registerJsFile('/resource/js/ya.map.house.js',['position' => \yii\web\View::POS_HEAD]);

        $model = new House();

        $housetype = \common\models\HousesType::find()->all();
        $housestype = \yii\helpers\ArrayHelper::map($housetype,'id','name');

        $image = '';
        $images_add = [];
        $images_db = [];
        $images_dir = [];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $this->actionUploadMain($model->id);

            $this->actionUploadMany($model->id);

            return $this->redirect(['index']);

        } else {

            $arrConfRaw = [];
            $arrLinksRaw = [];
            if($images_db != null){
                for($i=0; $i<count($images_db); $i++){
                    $arrConfRaw [$i] = array(
                        'caption' => $images_db[$i],
                        'url' => '/house/file',
                        'extra' => array(
                            'id' => $model['id'],
                            'caption' => $images_db[$i],
                        ),
                        'key' => $i,
                    );
                }

                for($i=0; $i<count($images_db); $i++){
                    $arrLinksRaw[$i] =  array(
                        $images_db[$i],
                    );
                }
            }

            return $this->render('create', [
                'model' => $model,
                'housestype' => $housestype,
                'image' => $image,
                'images_db' => $images_db,
                'images_add' => $images_add,
                'images_dir' => $images_dir,
                'arrConfRaw' => $arrConfRaw,
                'arrLinksRaw' => $arrLinksRaw,
            ]);
        }
    }

    /**
     * Updates an existing House model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
//        Yii::$app->view->registerJsFile('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU',['position' => \yii\web\View::POS_HEAD]);
//        Yii::$app->view->registerJsFile('/resource/js/ya.map.house.js',['position' => \yii\web\View::POS_HEAD]);

        $housetype = \common\models\HousesType::find()->all();
        $housestype = \yii\helpers\ArrayHelper::map($housetype,'id','name');

        $model = $this->findModel($id);
        $image = '';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $this->actionUploadMain($model->id);

            $this->actionUploadMany($model->id);

            return $this->redirect(['index']);
        } else {

            if($name = $model->img_house){
                $image[] =  '<img src="//'.\Yii::$app->params['baseUrl'].'/uploads/house/' . $model->id . '/small_' . $name . '" width=200>';
            }

            if(Yii::$app->request->isPost){
                $this->redirect(Url::to(['house/']));
            }


            $path = Yii::getAlias("@frontend/web/uploads/house/".$model->id);
            $images_add = [];
            $images_db = [];
            $images_dir = [];

            try {
                if(is_dir($path)) {
                    $files = \yii\helpers\FileHelper::findFiles($path);

                    foreach ($files as $file) {
                        if (!strstr($file, "small_") && !strstr($file, "main_") && !strstr($file, "wtmarked_")) {
                            $images_add[] = '<img src="//'.\Yii::$app->params['baseUrl'].'/uploads/house/' . $model->id . '/' . basename($file) . '" width=200>';
                            $images_db[] = basename($file);
                            $images_dir[] = '//'.\Yii::$app->params['baseUrl'].'/uploads/house/' . $model->id . '/small_' .basename($file);
                        }
                    }
                }
            }
            catch(\yii\base\Exception $e){}



            $arrConfRaw = [];
            $arrLinksRaw = [];
            if($images_db != null){
                for($i=0; $i<count($images_db); $i++){
                    $arrConfRaw [$i] = array(
                        'caption' => $images_db[$i],
                        'url' => '/house/file',
                        'extra' => array(
                            'id' => $model['id'],
                            'caption' => $images_db[$i],
                        ),
                        'key' => $i,
                    );
                }

                for($i=0; $i<count($images_db); $i++){
                    $arrLinksRaw[$i] =  array(
                        $images_db[$i],
                    );
                }
            }


            $model->str_imgs_house = implode(",", $images_add);
//            $model->str_imgs_house = implode(",", $images_dir);
            $model->save();

            return $this->render('update', [
                'model' => $model,
                'housestype' => $housestype,
                'image' => $image,
                'images_db' => $images_db,
                'images_add' => $images_add,
                'images_dir' => $images_dir,
                'arrConfRaw' => $arrConfRaw,
                'arrLinksRaw' => $arrLinksRaw,
            ]);
        }
    }

    /**
     * Deletes an existing House model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        if($path = Yii::getAlias("@frontend/web/uploads/house/".$model->id)){
            BaseFileHelper::removeDirectory($path);
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    public function actionUploadMain($id){

        $model = House::findOne($id);

        if(Yii::$app->request->isPost && UploadedFile::getInstanceByName('main')){

            $path_dir = Yii::getAlias("@frontend/web/images/main/watermark/");
            $allink_01 = $path_dir . "allink_01.png";
            $path = Yii::getAlias("@frontend/web/uploads/house/".$id);
            BaseFileHelper::createDirectory($path);
            $images_arr = [];

            if(is_dir($path)){

                try {
                    if(is_dir($path)) {
                        $files = \yii\helpers\FileHelper::findFiles($path);

                        foreach ($files as $file) {
                            if (strstr($file, "small_main_") || strstr($file, "main_")) {
                                unlink($file);
                            }
                        }
                    }
                }
                catch(\yii\base\Exception $e){}

            }


            $file = UploadedFile::getInstanceByName('main');

            $name_original = $this->actionTransliterate($file->baseName).'.'.$file->extension;
            $name = 'main_'.$name_original;

            $file->saveAs($path .'/' .$name);

            $image  = $path .'/' .$name;
            $new_name = $path .'/'."small_".$name;


//            $original = $path_original . '/' . $name_original;

            $model->img_house = $name;
            $model->save();

            $size = getimagesize($image);
            $width = $size[0];
            $height = $size[1];

            $sizeWMark = getimagesize($allink_01);
            $widthWMark = $sizeWMark[0];
            $heightWMark = $sizeWMark[1];

            $ratio = $width/$height;


            if($ratio>1){
                $height = 500;
                $width = $height*$ratio;
                $xW = ($width-500)/2;
                \yii\imagine\Image::frame($image, 0, '666', 0)
                    ->resize(new Box($width, $height))
                    ->crop(new Point($xW, 0), new Box(500, $height))
                    ->save($new_name, ['quality' => 100]);

//                \yii\imagine\Image::frame($image, 0, '666', 0)
//                    ->save($original, ['quality' => 100]);

            }elseif($ratio<1){
                $width = 500;
                $height = $width/$ratio;
//                $heightSec = $width/1.42;
                $hW = ($height-500)/2;
                \yii\imagine\Image::frame($image, 0, '666', 0)
                    ->resize(new Box($width, $height))
                    ->crop(new Point(0, $hW), new Box($width, 500))
                    ->save($new_name, ['quality' => 100]);

//                \yii\imagine\Image::frame($image, 0, '666', 0)
//                    ->save($original, ['quality' => 100]);

            }else{
                \yii\imagine\Image::frame($image, 0, '666', 0)
                    ->crop(new Point(0, 0), new Box($width, $height))
                    ->resize(new Box(500,500))
                    ->save($new_name, ['quality' => 100]);

//                \yii\imagine\Image::frame($image, 0, '666', 0)
//                    ->save($original, ['quality' => 100]);
            }

        }

        if($name = $model->img_house){
            $image =  '<img src="//'.\Yii::$app->params['baseUrl'].'/uploads/house/' . $model->id . '/small_' . $name . '" width=200>';
        }

        if(Yii::$app->request->isPost){
            $this->redirect(Url::to(['house/']));
        }

    }

    public function actionUploadMany($id){


        if(Yii::$app->request->isPost  && Yii::$app->request->post("files")) {
//            $id = $model['id'];
            $path_dir = Yii::getAlias("@frontend/web/images/main/watermark/");
            $allink_01 = $path_dir . "allink_01.png";
            $path_mark_bk_lg = $path_dir . "logo_black_300.png";
            $path_mark_bk = $path_dir . "logo_black.png";
            $path_mark_bk_md = $path_dir . "logo_black_md.png";
            $path_mark_bk_sm = $path_dir . "logo_black_sm.png";
            $path_mark_bl_lg = $path_dir . "logo_blue_300.png";
            $path_mark_bl = $path_dir . "logo_blue.png";
            $path_mark_bl_md = $path_dir . "logo_blue_md.png";
            $path_mark_bl_sm = $path_dir . "logo_blue_sm.png";
            $path_mark_rd_lg = $path_dir . "logo_red_300.png";
            $path_mark_rd = $path_dir . "logo_red.png";
            $path_mark_rd_md = $path_dir . "logo_red_md.png";
            $path_mark_rd_sm = $path_dir . "logo_red_sm.png";
            $path = Yii::getAlias("@frontend/web/uploads/house/" . $id);
            BaseFileHelper::createDirectory($path);
            $images_arr = [];


            $model = House::findOne($id);

            $file = UploadedFile::getInstancesByName('files');
            foreach($file as $row){
                $name = array($this->actionTransliterate($row->baseName).'.'.$row->extension);

                foreach($name as $fileName){
                    $row->saveAs($path . '/' .$fileName);

                    $image = $path . '/' . $fileName;

                    $new_name = $path . '/' . "small_" . $fileName;
                    $model->str_imgs_house = $fileName;
                    $model->save();

                    $size = getimagesize($image);
                    $width = $size[0];
                    $height = $size[1];

                    $sizeWMark = getimagesize($allink_01);
                    $widthWMark = $sizeWMark[0];
                    $heightWMark = $sizeWMark[1];


                    $ratio = $width / $height;

                    if ($ratio > 1) {
                        $height = 500;
                        $width = $height * $ratio;
                        $xW = ($width - 500) / 2;
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->resize(new Box($width, $height))
                            ->crop(new Point($xW, 0), new Box(500, $height))
                            ->save($new_name, ['quality' => 100]);
                    } elseif ($ratio < 1) {
                        $width = 500;
                        $height = $width / $ratio;
//                        $heightSec = $width / 1.42;
                        $hW = ($height - 500) / 2;
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->resize(new Box($width, $height))
                            ->crop(new Point(0, $hW), new Box($width, 500))
                            ->save($new_name, ['quality' => 100]);
                    } else {
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->crop(new Point(0, 0), new Box($width, $height))
                            ->resize(new Box(500, 500))
                            ->save($new_name, ['quality' => 100]);
                    }

                    sleep(1);
                }
            }

        }




        $path = Yii::getAlias("@frontend/web/uploads/house/".$id);
        $images_add = [];
        $images_db = [];
        $images_dir = [];

        try {
            if(is_dir($path)) {
                $files = \yii\helpers\FileHelper::findFiles($path);

                foreach ($files as $file) {
                    if (!strstr($file, "small_") && !strstr($file, "main_") && !strstr($file, "wtmarked_")) {
                        $images_add[] = '<img src="//'.\Yii::$app->params['baseUrl'].'/uploads/house/' . $model->id . '/' . basename($file) . '" width=200>';
                        $images_db[] = basename($file);
                        $images_dir[] = '//'.\Yii::$app->params['baseUrl'].'/uploads/house/' . $model->id . '/small_' .basename($file);
                    }
                }
            }
        }
        catch(\yii\base\Exception $e){}


        $model->str_imgs_house = implode(",", $images_add);
//        $model->str_imgs_house = implode(",", $images_dir);
        $model->save();


    }

    public function actionFile(){
        $dataPost = Yii::$app->request->post();
        $data1 = Yii::$app->request->post('data');
        $data = json_decode($data1, false);

        if (Yii::$app->request->isAjax) {
            $id = $dataPost["id"];
            $name = $dataPost['caption'];
            $smallName = 'small_'.$dataPost['caption'];
            $waterName = 'wtmarked_'.$dataPost['caption'];
            $path = Yii::getAlias("@frontend/web/uploads/house/".$id);
            $file = $path.DIRECTORY_SEPARATOR.$name;
            $smallFile = $path.DIRECTORY_SEPARATOR.$smallName;
            $waterFile = $path.DIRECTORY_SEPARATOR.$waterName;

            if(is_file($file)){
                unlink($file);
            }
            if(is_file($smallFile)){
                unlink($smallFile);
            }
            try {
                if(is_dir($path)) {
                    $files = \yii\helpers\FileHelper::findFiles($path);

                    foreach ($files as $file) {
                        if (strstr($file, $name) && strstr($file, "wtmarked_")) {
                            unlink($file);
                        }
                    }
                }
            }
            catch(\yii\base\Exception $e){}

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $items = $dataPost['caption']." успешно удален!";
            return $items;
        }
    }

    /**
     * Finds the House model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return House the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = House::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }

    function actionTransliterate($string) {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
            ' ' => '_',
        );
        return strtr($string, $converter);
    }
}
