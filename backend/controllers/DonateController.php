<?php

namespace backend\controllers;

use common\controllers\AuthController;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Yii;
use common\models\Donate;
use common\models\Search\DonateSearch;
use yii\db\Query;
use yii\helpers\BaseFileHelper;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DonateController implements the CRUD actions for Donate model.
 */
class DonateController extends AuthController
{

    /**
     * Lists all Donate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DonateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Donate model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Donate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Donate();

        $image = '';
        $images_add = [];
        $images_db = [];
        $images_dir = [];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $this->actionUploadMain($model->id);

            $this->actionUploadMany($model->id);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'image' => $image,
                'images_db' => $images_db,
                'images_dir' => $images_dir,
                'images_add' => $images_add,
            ]);
        }
    }

    /**
     * Updates an existing Donate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image = '';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $this->actionUploadMain($model->id);

            $this->actionUploadMany($model->id);

            return $this->redirect(['index']);

        } else {

            if($name = $model->img_donates){
                $image[] =  '<img src="//'.\Yii::$app->params['baseUrl'].'/uploads/donate/' . $model->id . '/small_' . $name . '" width=200>';
            }

            if(Yii::$app->request->isPost){
                $this->redirect(Url::to(['donate/']));
            }


            $path = Yii::getAlias("@frontend/web/uploads/donate/".$model->id);
            $images_add = [];
            $images_db = [];
            $images_dir = [];

            try {
                if(is_dir($path)) {
                    $files = \yii\helpers\FileHelper::findFiles($path);

                    foreach ($files as $file) {
                        if (!strstr($file, "small_") && !strstr($file, "main_") && !strstr($file, "wtmarked_")) {
                            $images_add[] = '<img src="//'.\Yii::$app->params['baseUrl'].'/uploads/donate/' . $model->id . '/' . basename($file) . '" width=200>';
                            $images_db[] = basename($file);
                            $images_dir[] = '//'.\Yii::$app->params['baseUrl'].'/uploads/donate/' . $model->id . '/small_' .basename($file);
                        }
                    }
                }
            }
            catch(\yii\base\Exception $e){}


            $model->str_imgs_donates = implode(",", $images_add);
            $model->save();

            return $this->render('update', [
                'model' => $model,
                'image' => $image,
                'images_db' => $images_db,
                'images_dir' => $images_dir,
                'images_add' => $images_add,
            ]);
        }
    }

    /**
     * Deletes an existing Donate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($path = Yii::getAlias("@frontend/web/uploads/donate/".$model->id)){
            BaseFileHelper::removeDirectory($path);
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Donate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Donate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Donate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }

    public function actionUploadMain($id){

        $model = Donate::findOne($id);

//        var_dump(UploadedFile::getInstanceByName('main'));die;

        if(Yii::$app->request->isPost && UploadedFile::getInstanceByName('main')){

            $path_dir = Yii::getAlias("@frontend/web/images/main/watermark/");
            $allink_01 = $path_dir . "allink_01.png";
            $path = Yii::getAlias("@frontend/web/uploads/donate/".$id);
            BaseFileHelper::createDirectory($path);

            if(is_dir($path)){

                try {
                    if(is_dir($path)) {
                        $files = \yii\helpers\FileHelper::findFiles($path);

                        foreach ($files as $file) {
                            if (strstr($file, "small_main_") || strstr($file, "main_")) {
                                unlink($file);
                            }
                        }
                    }
                }
                catch(\yii\base\Exception $e){}

            }

            $file = UploadedFile::getInstanceByName('main');
//            var_dump($file);die;
//            $name = 'main_'.basename($file);
            $name = 'main_'.$this->actionTransliterate($file->baseName).'.'.$file->extension;
            $file->saveAs($path .DIRECTORY_SEPARATOR .$name);

            $image  = $path .DIRECTORY_SEPARATOR .$name;
            $new_name = $path .DIRECTORY_SEPARATOR."small_".$name;

            $model->img_donates = $name;
            $model->save();

            $size = getimagesize($image);
            $width = $size[0];
            $height = $size[1];

            $ratio = round($width/$height, 3);

            if($ratio>1.515){
                $height = 564;
                $width = $height*$ratio;
                $xW = ($width-855)/2;
                \yii\imagine\Image::frame($image, 0, '666', 0)
                    ->resize(new Box($width, $height))
                    ->crop(new Point($xW, 0), new Box(855, $height))
                    ->save($new_name, ['quality' => 100]);
            }elseif($ratio<1.515){
                $width = 855;
                $height = $width/$ratio;
                $hW = ($height-564)/2.4;
                \yii\imagine\Image::frame($image, 0, '666', 0)
                    ->resize(new Box($width, $height))
                    ->crop(new Point(0, $hW), new Box($width, 564))
                    ->save($new_name, ['quality' => 100]);
            }else{
                $width = 855;
                $height = $width/$ratio;
                \yii\imagine\Image::frame($image, 0, '666', 0)
                    ->resize(new Box($width, $height))
                    ->crop(new Point(0, 0), new Box(758,564))
                    ->save($new_name, ['quality' => 100]);
            }


//            try {
//                if(is_dir($path)) {
//
//                    if (is_file($path.'/'.$name) && !is_file($path.'/wtmarked_'.($name))) {
//                        $images_arr[] = ($name);
//                        $image = ''.($name);
//                        $pos = mb_strripos($name, '.');
//                        $new_str = mb_substr($name, $pos);
//
//                        $size = getimagesize($path.'/'.$name);
//                        $width = $size[0];
//                        $height = $size[1];
//
//                        $sizeWMark = getimagesize($allink_01);
//                        $widthWMark = $sizeWMark[0];
//                        $heightWMark = $sizeWMark[1];
//
//
//
//                        if($width <= 5000 && $height <= 5000){
//
//                            \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                            $new_name = $path. '/' .time()."_allink.png";
//
//
//                            //открываем исходное изображение
//                            $src = imagecreatefrompng($allink_01);
//
//                            //создаем дескриптор для измененного изображения
//                            //                        $dst = imagecreatetruecolor($width, $height);
//                            if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                $dst = imagecreatefromjpeg($path.'/'.$image);
//                            }elseif($new_str == '.png'){
//                                $dst = imagecreatefrompng($path.'/'.$image);
//                            }elseif($new_str == '.gif'){
//                                $dst = imagecreatefromgif($path.'/'.$image);
//                            }
//
//                            //устанавливаем прозрачность
//                            \frontend\components\Common::setTransparency($dst, $src);
//
//                            //изменяем размер
//                            ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $height);
//
//                            //сохраняем уменьшенное изображение в файл
//                            ImagePNG($dst, $new_name);
//
//                            $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                            //закрываем дескрипторы исходного и уменьшенного изображений
//                            ImageDestroy($src);
//                            ImageDestroy($dst);
//
//                            unlink($new_name);
//
//                        }elseif($width > 5000 && $height <= 5000){
//
//                            \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                            $new_name = $path. '/' .time()."_allink.png";
//
//
//                            $src = imagecreatefrompng($allink_01);
//
//                            if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                $dst = imagecreatefromjpeg($path.'/'.$image);
//                            }elseif($new_str == '.png'){
//                                $dst = imagecreatefrompng($path.'/'.$image);
//                            }elseif($new_str == '.gif'){
//                                $dst = imagecreatefromgif($path.'/'.$image);
//                            }
//
//                            \frontend\components\Common::setTransparency($dst, $src);
//
//                            ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $height);
//
//                            ImagePNG($dst, $new_name);
//
//                            $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                            ImageDestroy($src);
//                            ImageDestroy($dst);
//
//                            unlink($new_name);
//
//                        }elseif($width <= 5000 && $height > 5000){
//
//                            \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                            $new_name = $path. '/' .time()."_allink.png";
//
//
//                            $src = imagecreatefrompng($allink_01);
//
//                            if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                $dst = imagecreatefromjpeg($path.'/'.$image);
//                            }elseif($new_str == '.png'){
//                                $dst = imagecreatefrompng($path.'/'.$image);
//                            }elseif($new_str == '.gif'){
//                                $dst = imagecreatefromgif($path.'/'.$image);
//                            }
//
//                            \frontend\components\Common::setTransparency($dst, $src);
//
//                            ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $heightWMark);
//
//                            ImagePNG($dst, $new_name);
//
//                            $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                            ImageDestroy($src);
//                            ImageDestroy($dst);
//
//                            unlink($new_name);
//
//                        }else{
//
//                            \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                            $new_name = $path. '/' .time()."_allink.png";
//
//
//                            $src = imagecreatefrompng($allink_01);
//
//                            if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                $dst = imagecreatefromjpeg($path.'/'.$image);
//                            }elseif($new_str == '.png'){
//                                $dst = imagecreatefrompng($path.'/'.$image);
//                            }elseif($new_str == '.gif'){
//                                $dst = imagecreatefromgif($path.'/'.$image);
//                            }
//
//                            \frontend\components\Common::setTransparency($dst, $src);
//
//                            ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $heightWMark);
//
//                            ImagePNG($dst, $new_name);
//
//                            $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                            ImageDestroy($src);
//                            ImageDestroy($dst);
//
//                            unlink($new_name);
//
//                        }
//                    }
//                }
//            }
//            catch(\yii\base\Exception $e){}

//                return true;

        }


//            step2
//--------------------------------
        if($name = $model->img_donates){
            $image =  '<img src="//'.\Yii::$app->params['baseUrl'].'/uploads/house/' . $model->id . '/small_' . $name . '" width=200>';
        }

        if(Yii::$app->request->isPost){
            $this->redirect(Url::to(['donate/']));
        }
//            end step2
//--------------------------------

    }

    public function actionUploadMany($id){

//        var_dump(Yii::$app->request->post("files"));die;

//            Upload Many
//--------------------------------
        if(Yii::$app->request->isPost  && Yii::$app->request->post("files")) {
//            $id = $model['id'];
            $path_dir = Yii::getAlias("@frontend/web/images/main/watermark/");
            $allink_01 = $path_dir . "allink_01.png";
            $path = Yii::getAlias("@frontend/web/uploads/donate/" . $id);
            BaseFileHelper::createDirectory($path);
            $images_arr = [];


            $model = Donate::findOne($id);

            $file = UploadedFile::getInstancesByName('files');
            foreach($file as $row){
//                $name = array(basename($row));
                $name = array($this->actionTransliterate($row->baseName).'.'.$row->extension);
                foreach($name as $fileName){
                    $row->saveAs($path .DIRECTORY_SEPARATOR .$fileName);
                    $row->saveAs($path .DIRECTORY_SEPARATOR .$fileName);
                    $image = $path . DIRECTORY_SEPARATOR . $fileName;
                    $new_name = $path . DIRECTORY_SEPARATOR . "small_" . $fileName;
                    $model->str_imgs_donates = $fileName;
                    $model->save();

                    $size = getimagesize($image);
                    $width = $size[0];
                    $height = $size[1];

                    $ratio = round($width / $height, 3);

                    if($ratio>1.515){
                        $height = 564;
                        $width = $height*$ratio;
                        $xW = ($width-855)/2;
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->resize(new Box($width, $height))
                            ->crop(new Point($xW, 0), new Box(855, $height))
                            ->save($new_name, ['quality' => 100]);
                    }elseif($ratio<1.515){
                        $width = 855;
                        $height = $width/$ratio;
                        $hW = ($height-564)/2.4;
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->resize(new Box($width, $height))
                            ->crop(new Point(0, $hW), new Box($width, 564))
                            ->save($new_name, ['quality' => 100]);
                    }else{
                        $width = 855;
                        $height = $width/$ratio;
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->resize(new Box($width, $height))
                            ->crop(new Point(0, 0), new Box(758,564))
                            ->save($new_name, ['quality' => 100]);
                    }

//                    try {
//                        if(is_dir($path)) {
//
//                            if (is_file($path.'/'.$fileName) && !is_file($path.'/wtmarked_'.($fileName))) {
//                                $images_arr[] = ($fileName);
//                                $image = ''.($fileName);
//                                $pos = mb_strripos($fileName, '.');
//                                $new_str = mb_substr($fileName, $pos);
//
//                                $size = getimagesize($path.'/'.$fileName);
//                                $width = $size[0];
//                                $height = $size[1];
//
//                                $sizeWMark = getimagesize($allink_01);
//                                $widthWMark = $sizeWMark[0];
//                                $heightWMark = $sizeWMark[1];
//
//
//
//                                if($width <= 5000 && $height <= 5000){
//
//                                    \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                                    $new_name = $path. '/' .time()."_allink.png";
//
//
//                                    //открываем исходное изображение
//                                    $src = imagecreatefrompng($allink_01);
//
//                                    //создаем дескриптор для измененного изображения
//                                    //                        $dst = imagecreatetruecolor($width, $height);
//                                    if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                        $dst = imagecreatefromjpeg($path.'/'.$image);
//                                    }elseif($new_str == '.png'){
//                                        $dst = imagecreatefrompng($path.'/'.$image);
//                                    }elseif($new_str == '.gif'){
//                                        $dst = imagecreatefromgif($path.'/'.$image);
//                                    }
//
//                                    //устанавливаем прозрачность
//                                    \frontend\components\Common::setTransparency($dst, $src);
//
//                                    //изменяем размер
//                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $height);
//
//                                    //сохраняем уменьшенное изображение в файл
//                                    ImagePNG($dst, $new_name);
//
//                                    $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                                    //закрываем дескрипторы исходного и уменьшенного изображений
//                                    ImageDestroy($src);
//                                    ImageDestroy($dst);
//
//                                    unlink($new_name);
//
//                                }elseif($width > 5000 && $height <= 5000){
//
//                                    \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                                    $new_name = $path. '/' .time()."_allink.png";
//
//
//                                    $src = imagecreatefrompng($allink_01);
//
//                                    if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                        $dst = imagecreatefromjpeg($path.'/'.$image);
//                                    }elseif($new_str == '.png'){
//                                        $dst = imagecreatefrompng($path.'/'.$image);
//                                    }elseif($new_str == '.gif'){
//                                        $dst = imagecreatefromgif($path.'/'.$image);
//                                    }
//
//                                    \frontend\components\Common::setTransparency($dst, $src);
//
//                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $height);
//
//                                    ImagePNG($dst, $new_name);
//
//                                    $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                                    ImageDestroy($src);
//                                    ImageDestroy($dst);
//
//                                    unlink($new_name);
//
//                                }elseif($width <= 5000 && $height > 5000){
//
//                                    \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                                    $new_name = $path. '/' .time()."_allink.png";
//
//
//                                    $src = imagecreatefrompng($allink_01);
//
//                                    if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                        $dst = imagecreatefromjpeg($path.'/'.$image);
//                                    }elseif($new_str == '.png'){
//                                        $dst = imagecreatefrompng($path.'/'.$image);
//                                    }elseif($new_str == '.gif'){
//                                        $dst = imagecreatefromgif($path.'/'.$image);
//                                    }
//
//                                    \frontend\components\Common::setTransparency($dst, $src);
//
//                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $heightWMark);
//
//                                    ImagePNG($dst, $new_name);
//
//                                    $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                                    ImageDestroy($src);
//                                    ImageDestroy($dst);
//
//                                    unlink($new_name);
//
//                                }else{
//
//                                    \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);
//
//                                    $new_name = $path. '/' .time()."_allink.png";
//
//
//                                    $src = imagecreatefrompng($allink_01);
//
//                                    if($new_str == '.jpg' || $new_str == '.jpeg'){
//                                        $dst = imagecreatefromjpeg($path.'/'.$image);
//                                    }elseif($new_str == '.png'){
//                                        $dst = imagecreatefrompng($path.'/'.$image);
//                                    }elseif($new_str == '.gif'){
//                                        $dst = imagecreatefromgif($path.'/'.$image);
//                                    }
//
//                                    \frontend\components\Common::setTransparency($dst, $src);
//
//                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $heightWMark);
//
//                                    ImagePNG($dst, $new_name);
//
//                                    $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);
//
//                                    ImageDestroy($src);
//                                    ImageDestroy($dst);
//
//                                    unlink($new_name);
//
//                                }
//                            }
//                        }
//                    }
//                    catch(\yii\base\Exception $e){}

                    sleep(1);
                }
            }

        }


        $path = Yii::getAlias("@frontend/web/uploads/donate/".$id);
        $images_add = [];
        $images_db = [];

        try {
            if(is_dir($path)) {
                $files = \yii\helpers\FileHelper::findFiles($path);

                foreach ($files as $file) {
                    if (!strstr($file, "small_") && !strstr($file, "main_") && !strstr($file, "wtmarked_")) {
                        $images_add[] = '<img src="//'.\Yii::$app->params['baseUrl'].'/uploads/donate/' . $model->id . '/' . basename($file) . '" width=200>';
                        $images_db[] = basename($file);
                        $images_dir[] = '//'.\Yii::$app->params['baseUrl'].'/uploads/donate/' . $model->id . '/small_' .basename($file);
                    }
                }
            }
        }
        catch(\yii\base\Exception $e){}


        $model->str_imgs_donates = implode(",", $images_add);
        $model->save();

    }

    public function actionFile(){
        $dataPost = Yii::$app->request->post();
        $data1 = Yii::$app->request->post('data');
        $data = json_decode($data1, false);

        if (Yii::$app->request->isAjax) {
            $id = $dataPost["id"];
            $name = $dataPost['caption'];
            $smallName = 'small_'.$dataPost['caption'];
            $path = Yii::getAlias("@frontend/web/uploads/donate/".$id);
            $file = $path.DIRECTORY_SEPARATOR.$name;
            $smallFile = $path.DIRECTORY_SEPARATOR.$smallName;

            unlink($file);
            unlink($smallFile);


            try {
                if(is_dir($path)) {
                    $files = \yii\helpers\FileHelper::findFiles($path);

                    foreach ($files as $file) {
                        if (strstr($file, $name) && strstr($file, "wtmarked_")) {
                            unlink($file);
                        }
                    }
                }
            }
            catch(\yii\base\Exception $e){}

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $items = $dataPost['caption']." успешно удален!";
            return $items;
        }
    }

    function actionTransliterate($string) {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
            ' ' => '_',
        );
        return strtr($string, $converter);
    }

}
