<?php

namespace backend\controllers;

use Yii;
use common\models\OurTeam;
use common\models\Search\OurTeamSearch;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use Imagine\Image\Point;
use Imagine\Image\Box;

/**
 * OurTeamController implements the CRUD actions for OurTeam model.
 */
class OurTeamController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OurTeam models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OurTeamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OurTeam model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new OurTeam model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OurTeam();
        $model->scenario = 'create_team';

        if($model->load(\Yii::$app->request->post()) && $model->save()){
//            $this->uploadAvatar(time());

            if(Yii::$app->request->isPost){
                $path = Yii::getAlias("@frontend/web/uploads/team/".date('d.m.Y_H-i-s',$model['created_at']));
                BaseFileHelper::createDirectory($path);
                $file = UploadedFile::getInstance($model,'img_team');

                if($file) {
                    $name = $file->name;
                    $file->saveAs($path . DIRECTORY_SEPARATOR . $name);

                    $model->img_team = $name;
                    $model->save();

                    $image = $path . DIRECTORY_SEPARATOR . $name;
                    $new_name = $path . DIRECTORY_SEPARATOR . "small_" . $name;

                    $size = getimagesize($image);
                    $width = $size[0];
                    $height = $size[1];


                    $ratio = round($width/$height, 3);

                    if($ratio>1){
                        $height = 500;
                        $width = $height*$ratio;
                        $xW = ($width-500)/2;
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->resize(new Box($width, $height))
                            ->crop(new Point($xW, 0), new Box(500, $height))
                            ->save($new_name, ['quality' => 100]);
                    }elseif($ratio<1){
                        $width = 500;
                        $height = $width/$ratio;
                        $hW = ($height-500)/2;
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->resize(new Box($width, $height))
                            ->crop(new Point(0, $hW), new Box($width, 500))
                            ->save($new_name, ['quality' => 100]);
                    }else{
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->crop(new Point(0, 0), new Box($width, $height))
                            ->resize(new Box(500,500))
                            ->save($new_name, ['quality' => 100]);
                    }

                    return $this->redirect(['index']);
//                    return true;
                }
            }


            $this->refresh();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OurTeam model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $before_img = $model->img_team;

        if($model->load(\Yii::$app->request->post()) && $model->save()){
            if(Yii::$app->request->isPost){
//                if(Yii::getAlias("@frontend/web/uploads/team/".date('d.m.Y_H-i-s',$model['created_at']))){
//                    BaseFileHelper::removeDirectory(Yii::getAlias("@frontend/web/uploads/team/".date('d.m.Y_H-i-s',$model['created_at'])));
//                }
                $path = Yii::getAlias("@frontend/web/uploads/team/".date('d.m.Y_H-i-s',$model->created_at));
                BaseFileHelper::createDirectory($path);
                $file = UploadedFile::getInstance($model,'img_team');

//                var_dump($file);die;

                if(is_dir($path) && $file != null){

                    try {
                        if(is_dir($path)) {
                            $files = \yii\helpers\FileHelper::findFiles($path);

                            foreach ($files as $fileIn) {
                                unlink($fileIn);
                            }
                        }
                    }
                    catch(\yii\base\Exception $e){}

                }

                if($file) {
//                    $name = $file->name;
                    $name = $this->actionTransliterate($file->baseName).'.'.$file->extension;
                    $file->saveAs($path . DIRECTORY_SEPARATOR . $name);

                    $model->img_team = $name;
                    $model->save();

                    $image = $path . DIRECTORY_SEPARATOR . $name;
                    $new_name = $path . DIRECTORY_SEPARATOR . "small_" . $name;

                    $size = getimagesize($image);
                    $width = $size[0];
                    $height = $size[1];


                    $ratio = round($width/$height, 3);

                    if($ratio>1){
                        $height = 500;
                        $width = $height*$ratio;
                        $xW = ($width-500)/2;
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->resize(new Box($width, $height))
                            ->crop(new Point($xW, 0), new Box(500, $height))
                            ->save($new_name, ['quality' => 100]);
                    }elseif($ratio<1){
                        $width = 500;
                        $height = $width/$ratio;
                        $hW = ($height-500)/2;
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->resize(new Box($width, $height))
                            ->crop(new Point(0, $hW), new Box($width, 500))
                            ->save($new_name, ['quality' => 100]);
                    }else{
                        \yii\imagine\Image::frame($image, 0, '666', 0)
                            ->crop(new Point(0, 0), new Box($width, $height))
                            ->resize(new Box(500,500))
                            ->save($new_name, ['quality' => 100]);
                    }

                    return $this->redirect(['index']);
//                    return true;
                }
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->img_team == ''){
                $model->img_team = $before_img;
                $model->save();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OurTeam model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($path = Yii::getAlias("@frontend/web/uploads/team/".date('d.m.Y_H-i-s',$model->created_at))){
            BaseFileHelper::removeDirectory($path);
        }
        $model->delete();

        return $this->redirect(['index']);
    }

//    public function uploadAvatar($time){
//        if(Yii::$app->request->isPost){
//
//            $post_arr = Yii::$app->request->post("OurTeam");
//            $path = Yii::getAlias("@frontend/web/uploads/team/".date('d.m.Y_H-i-s',$time));
//            BaseFileHelper::createDirectory($path);
//            $model = Comment::findOne($id);
////            $file = UploadedFile::getInstanceByName('avatar');
//            $file = UploadedFile::getInstance($model,'comment_img');
//
//            if($file) {
//                $name = 'avatar.jpg';
//                $file->saveAs($path . DIRECTORY_SEPARATOR . $name);
//
//                $model->comment_img = $name;
//                $model->save();
//
//                $image = $path . DIRECTORY_SEPARATOR . $name;
//                $new_name = $path . DIRECTORY_SEPARATOR . "small_" . $name;
//
//                Image::frame($image, 0, '666', 0)
//                    ->thumbnail(new Box(200, 200))
//                    ->save($new_name, ['quality' => 100]);
//
//                return true;
//            }
//        }
//
//    }

    /**
     * Finds the OurTeam model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OurTeam the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OurTeam::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }

    function upload(){
        $model = new OurTeam();
        if(Yii::$app->request->isPost){
            $path = Yii::getAlias("@frontend/web/uploads/team/".date('d.m.Y_H-i-s',time()));
            BaseFileHelper::createDirectory($path);
            $file = UploadedFile::getInstance($model,'img_team');

            if($file) {
                $name = 'avatar.jpg';
                $file->saveAs($path . DIRECTORY_SEPARATOR . $name);

                $model->img_team = $name;
                $model->save();

                $image = $path . DIRECTORY_SEPARATOR . $name;
                $new_name = $path . DIRECTORY_SEPARATOR . "small_" . $name;

                Image::frame($image, 0, '666', 0)
                    ->thumbnail(new Box(500, 500))
                    ->save($new_name, ['quality' => 100]);

                return $this->redirect(['index']);
//                return true;
            }
        }
    }

    function actionTransliterate($string) {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
            ' ' => '_',
        );
        return strtr($string, $converter);
    }
}
