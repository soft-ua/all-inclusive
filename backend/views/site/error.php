<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<!-- Main content -->
<section class="content">

    <div class="error-page">
        <h2 class="headline text-info"><i class="fa fa-warning text-yellow"></i></h2>

        <div class="error-content">
            <h2><?= $name ?></h2>

            <h2>
                <?= nl2br(Html::encode($message)) ?>
            </h2>

                <a href='<?= Yii::$app->homeUrl ?>'>На главную</a>

<!--            <form class='search-form'>-->
<!--                <div class='input-group'>-->
<!--                    <input type="text" name="search" class='form-control' placeholder="Search"/>-->
<!---->
<!--                    <div class="input-group-btn">-->
<!--                        <button type="submit" name="submit" class="btn btn-primary"><i class="fa fa-search"></i>-->
<!--                        </button>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </form>-->
        </div>
    </div>

</section>
