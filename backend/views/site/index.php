<?php

/* @var $this yii\web\View */

$this->title = 'Admin Panel';
?>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?php echo ($modelHouse != null) ?  count($modelHouse) : '0';?></h3>
                    <p>Дома</p>
                </div>
                <div class="icon">
                    <i class="fa fa-home"></i>
                </div>
                <a href="/house" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php echo ($modelComment != null) ?  count($modelComment) : '0';?></h3>
                    <p>Отзывы</p>
                </div>
                <div class="icon">
                    <i class="fa fa-commenting"></i>
                </div>
                <a href="/comment" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?php echo ($modelDonate != null) ?  count($modelDonate) : '0';?></h3>
                    <p>Благодарности</p>
                </div>
                <div class="icon">
                    <i class="fa fa-gift"></i>
                </div>
                <a href="/donate" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3><?php echo ($modelTeam != null) ?  count($modelTeam) : '0';?></h3>
                    <p>Наша команда</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="/our-team" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->

    <div class="row">
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">

                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Доли типов объявлений</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">


                            <div class="chart-responsive">
                                <canvas id="pieChart" height="150"></canvas>
                            </div><!-- ./chart-responsive -->
                            <ul class="chart-legend clearfix">
                                <li><i class="fa fa-circle-o text-red"></i> Готовые дома <?php echo round((((($modelReadyHouses != null) ?  count($modelReadyHouses) : 0) / (($modelHouse != null) ?  count($modelHouse) : 1))*100),2); ?> %</li>
                                <li><i class="fa fa-circle-o text-green"></i> Реализованные проекты <?php echo round((((($modelProjects != null) ?  count($modelProjects) : 0) / (($modelHouse != null) ?  count($modelHouse) : 1))*100),2); ?> %</li>
                            </ul>

                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix">
<!--                            <a href="/house" class="btn btn-sm btn-info btn-flat pull-left">Просмотреть все дома</a>-->
                            <p>Всего объявлений о домах: <?php echo(($modelHouse != null) ?  count($modelHouse) : 0)?> шт.;</p>
                            <p>Готовых домов: <?php echo(($modelReadyHouses != null) ?  count($modelReadyHouses) : 0)?> шт.;</p>
                            <p>Реализованных проектов: <?php echo(($modelProjects != null) ?  count($modelProjects) : 0)?> шт.;</p>
                        </div><!-- /.box-footer -->
                    </div><!-- /.box -->


                </div><!-- /.col -->
                <div class="col-md-8">

                    <!-- TABLE: LATEST ORDERS -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Последние объявления о домах</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>Цена, руб</th>
                                        <th>Адрес</th>
                                        <th>Создано</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        if($modelHouseFive != null){foreach($modelHouseFive as $row):
                                        ?>

                                        <tr>
                                            <td><?php echo $row['title']?></td>
                                            <td><span class="label label-success"><?php echo $row['price']?></span></td>
                                            <td><?php echo $row['address']?></td>
                                            <td><?php echo date('d.m.Y H:i',$row['created_at'])?></td>
                                        </tr>

                                        <?php
                                        endforeach;}
                                        ?>


                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <a href="/house" class="btn btn-sm btn-info btn-flat pull-right">Просмотреть все дома</a>
                        </div><!-- /.box-footer -->
                    </div><!-- /.box -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.box-body -->
    </div><!-- /.row -->


    <div class="row">
        <div class="col-md-6">
            <!-- DIRECT CHAT -->
            <div class="box box-warning direct-chat direct-chat-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Последние отзывы</h3>
                    <div class="box-tools pull-right">
                        <span data-toggle="tooltip" title="Последние <?php echo ($modelCommentFive != null) ? count($modelCommentFive) : '0'; ?>" class="badge bg-yellow"> <?php echo ($modelCommentFive != null) ? count($modelCommentFive) : '0'; ?></span>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages">
                        <!-- Message. Default to the left -->
                        <div class="direct-chat-msg">

                            <?php
                            if($modelCommentFive != null){foreach($modelCommentFive as $row):
                                ?>

                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left"><?php echo $row['guest_name']?></span>
                                    <span class="direct-chat-timestamp pull-right"><?php echo date('d.m.Y H:i',$row['created_at'])?></span>
                                </div><!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="<?php echo \common\components\UserComponent::getCommentImage($row['created_at'],$row['comment_img'],false);?>" alt="отзыв от <?php echo $row['guest_name']?>"><!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    <?php echo $row['text_comment']?>
                                </div><!-- /.direct-chat-text -->

                                <?php
                            endforeach;}
                            ?>

                        </div><!-- /.direct-chat-msg -->
                    </div><!--/.direct-chat-messages-->
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="/comment" class="uppercase">Просмотреть все отзывы</a>
                </div><!-- /.box-footer -->
            </div><!--/.direct-chat -->
        </div><!-- /.col -->

        <div class="col-md-6">
            <!-- USERS LIST -->
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Наша команда</h3>
                    <div class="box-tools pull-right">
                        <span class="label label-danger">Количество: <?php echo ($modelTeam != null) ?  count($modelTeam) : '0';?></span>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <ul class="users-list clearfix">

                        <?php
                        if($modelTeamEight != null){foreach($modelTeamEight as $row):
                            ?>

                            <li>
                                <img src="<?php echo \common\components\UserComponent::getTeamImage($row['created_at'],$row['img_team'],false);?>" alt="Staff Image">
                                <p class="users-list-name"><?php echo $row['name_team']?></p>
                                <span class="users-list-date"><?php echo date('d.m.Y',$row['created_at'])?></span>
                            </li>

                            <?php
                        endforeach;}
                        ?>

                    </ul><!-- /.users-list -->
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="/our-team" class="uppercase">Просмотреть всю команду</a>
                </div><!-- /.box-footer -->
            </div><!--/.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

</section>

<?php
$this->registerJs("



");
?>
<script>
    $(function () {

        'use strict';

        /* ChartJS
         * -------
         * Here we will create a few charts using ChartJS
         */

        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);
        var PieData = [
            {
                value: <?php echo ($modelReadyHouses != null) ?  count($modelReadyHouses) : '0';?>,
                color: "#f56954",
                highlight: "#f56954",
                label: "Готовые дома"
            },
            {
                value: <?php echo ($modelProjects != null) ?  count($modelProjects) : '0';?>,
                color: "#00a65a",
                highlight: "#00a65a",
                label: "Реализованные проекты"
            },
        ];
        var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 1,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: false,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
            //String - A tooltip template
            tooltipTemplate: "<%=value %> <%=label%>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions);
        //-----------------
        //- END PIE CHART -
        //-----------------


    });

</script>



