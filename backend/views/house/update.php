<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\House */

$this->title = 'Обновить объявление о доме: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Дома', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title.' - Обновление'];
//$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="house-update inner">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php echo $this->render('_form', [
        'model' => $model,
        'housestype' => $housestype,
        'image' => $image,
        'images_db' => $images_db,
        'images_dir' => $images_dir,
        'images_add' => $images_add,
        'arrConfRaw' => $arrConfRaw,
        'arrLinksRaw' => $arrLinksRaw,
    ]) ?>

</div>
