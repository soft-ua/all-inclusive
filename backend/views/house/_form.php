<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\House */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="house-form inner">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <div id="YMapsID"></div>

    <?php echo $form->field($model, 'tour_link')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'location')->textInput()->hiddenInput()->label(false) ?>

    <?php echo $form->field($model, 'type')->dropDownList($housestype) ?>

    <?php echo $form->field($model, 'available')->radioList([false=>'Нет', true=>'Да']) ?>

    <?php
    echo $form->field($model, 'img_house')->widget(\kartik\file\FileInput::classname(),[
        'name' => 'main_photo',
        'language' => 'ru',
        'options' => [
            'accept' => 'image/*',
            'id_main' => $model->id,
            'name' => 'main',
        ],
        'pluginOptions' => [
            'allowedFileExtensions' =>  ['jpg', 'png','gif','jpeg'],
            'initialPreview' => $image,
            'showUpload' => false,
            'showRemove' => false,
            'dropZoneEnabled' => false,
            'overwriteInitial'=>true,
            'maxFilePreviewSize'=>8192,
            'maxPreviewFileSize'=>8192,
        ]
    ]);
    ?>
    

    <?php
    echo $form->field($model, 'str_imgs_house')->widget(\kartik\file\FileInput::classname(),[
        'name' => 'images',
        'language' => 'ru',
        'options'=>[
            'accept' => 'image/*',
            'multiple'=>true,
            'name'=> 'files[]',
        ],
        'pluginOptions' => [
            'deleteUrl' => \yii\helpers\Url::to(['file']),
            'overwriteInitial' => false,
            'initialPreviewAsData' => true,
            'allowedFileExtensions' =>  ['jpg', 'png','gif','jpeg'],
            'initialPreview' => $images_dir,
            'showUpload' => false,
            'showRemove' => false,
            'dropZoneEnabled' => false,
            'uploadAsync' => false,
            'showPreview' => true,
            'showCaption' => true,
            'overwriteInitial'=>false,
            'previewFileType' => 'image',
            'initialPreviewConfig' => $arrConfRaw,
            'initialPreviewShowDelete'=>true,
            'maxPreviewFileSize'=>8192,
            'maxFilePreviewSize'=>8192,
        ]
    ]);
    ?>


    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs("
    $(function () {
        CKEDITOR.replace('house-description');
    });
");
?>