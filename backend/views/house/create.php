<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\House */

$this->title = 'Создать объявление о доме';
$this->params['breadcrumbs'][] = ['label' => 'Дома', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="house-create inner">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php echo $this->render('_form', [
        'model' => $model,
        'housestype' => $housestype,
        'image' => $image,
        'images_db' => $images_db,
        'images_dir' => $images_dir,
        'images_add' => $images_add,
        'arrConfRaw' => $arrConfRaw,
        'arrLinksRaw' => $arrLinksRaw,
    ]) ?>

</div>
