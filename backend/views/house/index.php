<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\HouseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Дома';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

$this->registerJs("
    $(document).ready(function() {
        $('.fancybox').fancybox();
        $('.fancybox-thumb').fancybox({
            prevEffect	: 'none',
            nextEffect	: 'none',
            helpers	: {
                title	: {
                    type: 'outside'
                },
                thumbs	: {
                    width	: 50,
                    height	: 50
                }
            }
        });
    });
");
?>
<div class="house-index inner">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать объявление о доме', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'label' => 'Главное Фото',
                'format' => 'raw',
//                'options' => ['width' => '300'],
                'value' => function($data){
//                    return Html::a("<img src='".\common\components\UserComponent::getHouseImage($data->id,$data->img_house)."' alt='".$data->img_house." - main' width='100'>",
//                        [\common\components\UserComponent::getDonateImg($data->id,$data->img_house,true)],
//                        ['class' => "fancybox","data-fancybox-group" => $data->img_house]
//                    );
                    if(is_file(Yii::getAlias("@frontend/web/uploads/house/".$data->id.'/'.$data->img_house))) {
                        return "<a href='" . \common\components\UserComponent::getHouseImage($data->id, $data->img_house, true) . "' class='fancybox' data-fancybox-group='" . $data->img_house . "'><img src='" . \common\components\UserComponent::getHouseImage($data->id, $data->img_house) . "' class='img img-responsive' alt='" . $data->img_house . "' width='100'></a>";
                    } else {
                        return "<a href='".\common\components\UserComponent::getNoImage()."' class='fancybox' data-fancybox-group='".$data->id."'><img src='".\common\components\UserComponent::getNoImage()."' class='img img-responsive' alt='no photo' width='100'></a>";
                    }
                },
            ],
//            'img_house',
            [
                'label' => 'Фотографии',
                'format' => 'raw',
                'options' => ['width' => '165'],
                'value' => function($data){
                    $allImages = [];

                    $allImg = str_replace("<img src=\"", "", $data->str_imgs_house);
                    $allImg = str_replace("\" width=200>", "", $allImg);

                    $allImages = explode(',',$allImg);
                    for($i=0; $i<count($allImages); $i++){
                        $strAll[$i] = strrpos($allImages[$i], '/', -1);
                        $str = substr($allImages[$i], $strAll[$i]+1);
                        $all[$i] = $str;
//                        var_dump($str);die;


//                        $photoes[$i] =  Html::a("<img src='".\common\components\UserComponent::getHouseImage($data->id,$str)."' alt='".$str." - photoes' width='50'>",
//                            [\common\components\UserComponent::getDonateImg($data->id,$str,true)],
//                            ['class' => "fancybox","data-fancybox-group" => $data->id]
//                        );
//                        var_dump(is_file(Yii::getAlias("@frontend/web/uploads/house/".$data->id.'/'.$str)));die;
                        if(is_file(Yii::getAlias("@frontend/web/uploads/house/".$data->id.'/'.$str))){
                            $photoes[$i] = "<a href='".\common\components\UserComponent::getHouseImage($data->id,$str,true)."' class='fancybox' data-fancybox-group='".$data->id."'><img src='".\common\components\UserComponent::getHouseImage($data->id,$str)."' class='img img-responsive' alt='".$str."' width='49'></a>";
                        }else{
                            $photoes[$i] = "<a href='".\common\components\UserComponent::getNoImage()."' class='fancybox' data-fancybox-group='".$data->id."'><img src='".\common\components\UserComponent::getNoImage()."' class='img img-responsive' alt='no photo' width='100'></a>";
                        }
                    }
                    $str_photoes = implode(' ',$photoes);
                    return $str_photoes;
                },
            ],
            'title',
            'price'=> array(
                'attribute' => 'price',
//                'filter' =>function($data) {
//                    array(null => "Нет",true => $data['price']);
//                },
                'format' => 'raw',
                'content'=>function($data){
                    return ($data['price'] != null) ? $data['price'] : 'Нет';
                },
            ),
            'address',
            'tour_link'
            => array(
                'attribute' => 'tour_link',
//                'filter' => array(false=>'Нет',true=>'Да'),
                'format' => 'raw',
                'content'=>function($data){
                    return ($data['tour_link'] == true) ? $data['tour_link'] : 'Нет';
                },
            ),
            //'bedroom',
            // 'livingroom',
            // 'kitchen',
            // 'garage',
//            'room',
//            'floor',
            // 'description:ntext',
            // 'location',
            // 'hot',
            // 'new',
             'type' => array(
                 'attribute' => 'type',
                 'filter' => $housestype,
                 'content'=>function($data){
                     return ($data['type'] == 1) ? 'Дома на продажу' : 'Реализованные проекты';
                 },
             ),
//             'available',
            'available'
            => array(
                'attribute' => 'available',
                'filter' => array(false=>'Нет',true=>'Да'),
                'format' => 'raw',
                'content'=>function($data){
                    return ($data['available'] == true) ? 'Показано' : 'Спрятано';
                },
            ),
            // 'recommend',
            'created_at'=>[
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],
//            'updated_at'=>[
//                'attribute' => 'updated_at',
//                'format' => ['date', 'php:d.m.Y H:i:s'],
//            ],
            // 'deleted_at',
            // 'user_id',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
