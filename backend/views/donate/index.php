<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DonateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Благотворительные отзывы';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

$this->registerJs("
    $(document).ready(function() {
        $('.fancybox').fancybox();
        $('.fancybox-thumb').fancybox({
            prevEffect	: 'none',
            nextEffect	: 'none',
            helpers	: {
                title	: {
                    type: 'outside'
                },
                thumbs	: {
                    width	: 50,
                    height	: 50
                }
            }
        });
    });
");
?>
<div class="donate-index inner">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать благотворительный отзыв', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'label' => 'Главное Фото',
                'format' => 'raw',
                'value' => function($data){
//                    return Html::img(\common\components\UserComponent::getDonateImg($data->id,$data->img_donates,true),[
//                        'alt'=>"$data->img_donates - main",
//                        'style' => 'width:100px;'
//                    ]);
//                    return Html::a("<img src='".\common\components\UserComponent::getDonateImg($data->id,$data->img_donates,false)."' alt='".$data->img_donates." - main' width='100'>",
//                        [\common\components\UserComponent::getDonateImg($data->id,$data->img_donates,true)],
//                        ['class' => "fancybox","data-fancybox-group" => $data->img_donates]
//                    );
                    if(is_file(Yii::getAlias("@frontend/web/uploads/donate/".$data->id.'/'.$data->img_donates))) {
                        return "<a href='".\common\components\UserComponent::getDonateImg($data->id,$data->img_donates,true)."' class='fancybox' data-fancybox-group='".$data->img_donates."'><img src='".\common\components\UserComponent::getDonateImg($data->id,$data->img_donates)."' class='img img-responsive' alt='".$data->img_donates."' width='100'></a>";
                    } else {
                        return "<a href='".\common\components\UserComponent::getNoImage()."' class='fancybox' data-fancybox-group='".$data->id."'><img src='".\common\components\UserComponent::getNoImage()."' class='img img-responsive' alt='no photo' width='100'></a>";
                    }
                },
            ],
//            'img_donates',
            [
                'label' => 'Фотографии',
                'format' => 'raw',
                'options' => ['width' => '165'],
                'value' => function($data){
                    $allImages = [];

                    $allImg = str_replace("<img src=\"", "", $data->str_imgs_donates);
                    $allImg = str_replace("\" width=200>", "", $allImg);

                    $allImages = explode(',',$allImg);
                    for($i=0; $i<count($allImages); $i++){
                        $strAll[$i] = strrpos($allImages[$i], '/', -1);
                        $str = substr($allImages[$i], $strAll[$i]+1);
                        $all[$i] = $str;
                    }
                    $allImages = $all;

                    for($i=0; $i<count($allImages); $i++){
//                        $photoes[$i] =  Html::img(\common\components\UserComponent::getDonateStrImgs($data->id,$allImages[$i],true),[
//                            'alt'=>"$allImages[$i] - photoes",
//                            'style' => 'width:50px;'
//                        ]);
//                        $photoes[$i] =  Html::a("<img src='".\common\components\UserComponent::getDonateImg($data->id,$allImages[$i],false)."' alt='".$allImages[$i]." - photoes' width='50'>",
//                            [\common\components\UserComponent::getDonateImg($data->id,$allImages[$i],true)],
//                            ['class' => "fancybox","data-fancybox-group" => $data->id]
//                        );
                        if(is_file(Yii::getAlias("@frontend/web/uploads/donate/".$data->id.'/'.$allImages[$i]))) {
                            $photoes[$i] = "<a href='".\common\components\UserComponent::getDonateImg($data->id,$allImages[$i],true)."' class='fancybox' data-fancybox-group='".$data->id."'><img src='".\common\components\UserComponent::getDonateImg($data->id,$allImages[$i])."' class='img img-responsive' alt='".$data->id."' width='49'></a>";
                        } else {
                            $photoes[$i] = "<a href='".\common\components\UserComponent::getNoImage()."' class='fancybox' data-fancybox-group='".$data->id."'><img src='".\common\components\UserComponent::getNoImage()."' class='img img-responsive' alt='no photo' width='100'></a>";
                        }
                    }
                    $str_photoes = implode(' ',$photoes);
                    return $str_photoes;
                },
            ],
//            'str_imgs_donates',
            'name',
//            'description:ntext',
            'office_donate',
            'available_d'
            => array(
                'attribute' => 'available_d',
                'filter' => array(false=>'Нет',true=>'Да'),
                'format' => 'raw',
                'content'=>function($data){
                    return ($data['available_d'] == true) ? 'Показано' : 'Спрятано';
                },
            ),
            'created_at'=>[
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],
            'updated_at'=>[
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],
//            [
//                'label' => 'Ссылка',
//                'format' => 'raw',
//                'value' => function($data){
//                    return Html::a(
//                        '+ Фото',
//                        'donates-img/create/',
//                        [
//                            'title' => 'Создать Фото',
//                            'target' => '_blank'
//                        ]
//                    );
//                }
//            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
