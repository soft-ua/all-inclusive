<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\donate */

$this->title = 'Обновить благотворительный отзыв: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Благотворительные отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id.' - Обновление'];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="donate-update inner">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'image' => $image,
        'images_db' => $images_db,
        'images_dir' => $images_dir,
        'images_add' => $images_add,
    ]) ?>

</div>
