<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\donate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="donate-form inner">

    <?php
    $arrConfRaw = [];
    $arrLinksRaw = [];
    if($images_db != null){
        for($i=0; $i<count($images_db); $i++){
            $arrConfRaw [$i] = array(
                'caption' => $images_db[$i],
                'url' => '/donate/file',
                'extra' => array(
                    'id' => $model['id'],
                    'caption' => $images_db[$i],
                ),
                'key' => $i,
            );
        }

        for($i=0; $i<count($images_db); $i++){
            $arrLinksRaw[$i] =  array(
                $images_db[$i],
            );
        }
    }

    ?>

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'office_donate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'available_d')->radioList([false=>'Нет', true=>'Да']) ?>

    <?php
    echo $form->field($model, 'img_donates')->widget(\kartik\file\FileInput::classname(),[
        'name' => 'main_photo',
        'language' => 'ru',
        'options' => [
            'accept' => 'image/*',
            'id_main' => $model->id,
            'name' => 'main'
        ],
        'pluginOptions' => [
            'allowedFileExtensions' =>  ['jpg', 'png','gif','jpeg'],
            'initialPreview' => $image,
            'showUpload' => false,
            'showRemove' => false,
            'dropZoneEnabled' => false,
            'overwriteInitial'=>true,
            'maxPreviewFileSize'=>8192,
            'maxFilePreviewSize'=>8192,
        ]
    ]);
    ?>

    <?php
    echo $form->field($model, 'str_imgs_donates')->widget(\kartik\file\FileInput::classname(),[
        'name' => 'images',
        'language' => 'ru',
        'options'=>[
            'accept' => 'image/*',
            'multiple'=>true,
            'name'=> 'files[]'
        ],
        'pluginOptions' => [
            'deleteUrl' => \yii\helpers\Url::to(['file']),
            'maxFileCount' => 4,
            'overwriteInitial' => false,
            'initialPreviewAsData' => true,
            'allowedFileExtensions' =>  ['jpg', 'png','gif','jpeg'],
            'initialPreview' => $images_dir,
            'showUpload' => false,
            'showRemove' => false,
            'dropZoneEnabled' => false,
            'overwriteInitial'=>false,
            'maxPreviewFileSize'=>8192,
            'maxFilePreviewSize'=>8192,

            'uploadAsync' => false,
            'showPreview' => true,
            'showCaption' => true,
            'previewFileType' => 'image',
            'initialPreviewConfig' => $arrConfRaw,
            'initialPreviewShowDelete'=>true,
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $(function () {
        CKEDITOR.replace('donate-description');
    });
</script>
