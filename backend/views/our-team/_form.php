<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OurTeam */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="our-team-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div><?php
        if(!isset($model->img_team) || $model->img_team == null || $model->img_team == ''){
            echo \yii\helpers\Html::img(\common\components\UserComponent::getNoImage(), ['width' => 320]);
        }else{
            echo \yii\helpers\Html::img(\common\components\UserComponent::getTeamImage($model->created_at,$model->img_team,true), ['width' => 320,'class'=>'img img-responsive']);
        }
        ?>
    </div><br>

    <?= $form->field($model, 'id_team')->fileInput()->hiddenInput()->label(false) ?>

<!--    --><?php //echo $form->field($model, 'img_team')->fileInput(['class'=>'file-loading', 'multiple' => false, 'accept' => 'image/*']) ?>

    <?php
    echo $form->field($model, 'img_team')->widget(\kartik\file\FileInput::classname(),[
        'name' => 'team_img',
        'language' => 'ru',
        'options' => [
            'accept' => 'image/*',
            'multiple' => false,
            'ourteam_img' => $model->img_team,
        ],
        'pluginOptions' => [
            'allowedFileExtensions' =>  ['jpg', 'png','gif','jpeg'],
//            'initialPreview' => $image,
            'showUpload' => false,
            'showRemove' => false,
            'dropZoneEnabled' => false,
            'overwriteInitial'=>false,
//            'maxPreviewFileSize'=>8192,
//            'maxFilePreviewSize'=>8192,
        ]
    ]);
    ?>

<!--    <div class="">-->
<!--        --><?php
//        echo $form->field($model, 'img_team')->widget(\kartik\file\FileInput::classname(),[
//            'options' => [
//                'accept' => 'image/*',
//            ],
//            'pluginOptions' => [
//                'uploadUrl' => \yii\helpers\Url::to(['upload']),
////                'uploadExtraData' => [
////                    'id_image' => $model->id_image,
////                ],
//                'allowedFileExtensions' =>  ['jpg', 'png','gif'],
////                'initialPreview' => $image,
//                'showUpload' => false,
//                'showRemove' => false,
//                'dropZoneEnabled' => false
//            ]
//        ]);
//        ?>

    </div>

    <?= $form->field($model, 'name_team')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'office_team')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
