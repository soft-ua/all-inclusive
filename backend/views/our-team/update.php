<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OurTeam */

$this->title = 'Обновить данные: ' . $model->id_team;
$this->params['breadcrumbs'][] = ['label' => 'Наша команда', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title];
//$this->params['breadcrumbs'][] = ['label' => $model->id_team, 'url' => ['view', 'id' => $model->id_team]];
//$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="our-team-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
