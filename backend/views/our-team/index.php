<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\OurTeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Наша команда';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

$this->registerJs("
    $(document).ready(function() {
        $('.fancybox').fancybox();
        $('.fancybox-thumb').fancybox({
            prevEffect	: 'none',
            nextEffect	: 'none',
            helpers	: {
                title	: {
                    type: 'outside'
                },
                thumbs	: {
                    width	: 50,
                    height	: 50
                }
            }
        });
    });
");
?>
<div class="our-team-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить должность', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id_team',
//            'img_team',
            [
                'label' => 'Фотография',
                'format' => 'raw',
                'value' => function($data){
//                    return Html::img(\frontend\components\Common::getImageTeam($data),[
//                    return Html::img(\common\components\UserComponent::getTeamImage($data->created_at,$data->img_team,true),[
//                        'alt'=>"$data->img_team - gridview",
//                        'style' => 'width:70px;'
//                    ]);
                    if(is_file(Yii::getAlias("@frontend/web/uploads/team/".date("d.m.Y_H-i-s",$data->created_at).'/'.$data->img_team))) {
                        return "<a href='" . \common\components\UserComponent::getTeamImage($data->created_at,$data->img_team,true) . "' class='fancybox' data-fancybox-group='" . $data->img_team . "'><img src='" . \common\components\UserComponent::getTeamImage($data->created_at,$data->img_team) . "' class='img img-responsive' alt='" . $data->img_team . "' width='100'></a>";
                    } else {
                        return "<a href='".\common\components\UserComponent::getNoImage()."' class='fancybox' data-fancybox-group='".$data->created_at."'><img src='".\common\components\UserComponent::getNoImage()."' class='img img-responsive' alt='no photo' width='100'></a>";
                    }
                },
            ],
            'name_team',
            'office_team',
            'created_at'=>[
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],
            'updated_at'=>[
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
