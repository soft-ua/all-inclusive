<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Администратор';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-12 inner">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="">


            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered'
                ],
                'columns' => [
//                    'id',
                    'username',
                    'email',
                    'created_at'=>[
                        'attribute' => 'created_at',
                        'format' => ['date', 'php:d.m.Y H:i:s'],
                    ],
                    'updated_at'=>[
                        'attribute' => 'updated_at',
                        'format' => ['date', 'php:d.m.Y H:i:s'],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
//                        'template' => '{delete}',
                        'template' => '',
                    ],
                ],
            ]) ?>


        </div>
    </div>

</div>