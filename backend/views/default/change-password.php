<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\OurTeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Смена пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-form">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = \yii\bootstrap\ActiveForm::begin(); ?>

    <?php echo $form->field($model,'password')->passwordInput() ?>
    <?php echo $form->field($model,'repassword')->passwordInput() ?>


    <?php echo Html::submitButton('Сменить пароль', ['class' => 'btn btn-primary']) ?>

    <?php \yii\bootstrap\ActiveForm::end() ?>


</div>