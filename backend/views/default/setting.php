<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\OurTeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Профиль';
$this->params['breadcrumbs'][] = $this->title;

if(\Yii::$app->session->getFlash('success')){
    header("Refresh:3");
}
?>
<div class="default-form">

    <h1><?= Html::encode($this->title) ?></h1>
    <p></p>

    <div class="col-sm-2">
        <?php $form = \yii\bootstrap\ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

<!--        --><?php //echo \yii\helpers\Html::img(\common\components\UserComponent::getUserImage(Yii::$app->user->id), ['width' => 200]) ?>
        <?php if(is_file(\Yii::getAlias("@frontend/web/uploads/users/small_".\Yii::$app->user->id.".jpg")))
        { ?>
            <?php echo \yii\helpers\Html::img(\common\components\UserComponent::getUserImage(Yii::$app->user->id), ['width' => 200, 'alt'=>'user photo', 'class'=>'img img-responsive', 'style'=>'margin:auto']) ?>
        <?php } else { ?>
            <img src="<?=\common\components\UserComponent::getNoImageSqr() ?>" class="img img-responsive" alt="User Image" width="200" style="margin: auto;"/>
        <?php }?>
    </div>

    <div class="col-md-10">
        <?=$form->field($model,'username') ?>
        <?=$form->field($model,'email') ?>
        <?=\yii\helpers\Html::label('Фото администратора') ?>
        <?=\yii\helpers\Html::fileInput('avatar') ?>
<!--        <input type="file" name="img" multiple>-->
        <br>
        <br>


        <?php echo  Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success btn-right' : 'btn btn-primary']) ?>

        <?php \yii\bootstrap\ActiveForm::end() ?>
    </div>

</div>