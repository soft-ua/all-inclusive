<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index inner">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    if($slider == null) {
//    var_dump($slider);die;
    ?>
<!--        <p>-->
<!--            --><?php //echo Html::a('Добавить слайдер', ['create'], ['class' => 'btn btn-success']) ?>
<!--        </p>-->

        <?php $form = \yii\bootstrap\ActiveForm::begin([
            'method' => 'post',
            'action' => ['slider/create'],
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton('Добавить слайдер', ['class' => 'btn btn-success']);
            ?>
        </div>

        <?php \yii\bootstrap\ActiveForm::end(); ?>

    <?php
    }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
        ],
    ]); ?>
</div>
