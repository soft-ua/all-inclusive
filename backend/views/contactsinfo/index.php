<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContactsInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Контактые данные';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-info-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
    if($contactData == null) {
    ?>
<!--    <p>-->
<!--        --><?php //echo Html::a('Создать Адресные данные', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

        <?php $form = \yii\bootstrap\ActiveForm::begin([
            'method' => 'post',
            'action' => ['contactsinfo/create'],
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton('Добавить данные', ['class' => 'btn btn-success']);
            ?>
        </div>

        <?php \yii\bootstrap\ActiveForm::end(); ?>

    <?php
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'idcontacts',
            'email:email',
//            'skype',
            'phone',
            'working_hours',
            'address',
            // 'googlemaps',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
        ],
    ]); ?>

</div>
