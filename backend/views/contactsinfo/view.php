<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ContactsInfo */

$this->title = $model->idcontacts;
$this->params['breadcrumbs'][] = ['label' => 'Контактные данные', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-info-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idcontacts], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcontacts',
            'email:email',
            'skype',
            'phone',
            'address',
            'working_hours',
            'yandexmaps',
        ],
    ]) ?>

</div>
