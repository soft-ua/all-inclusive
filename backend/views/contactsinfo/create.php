<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContactsInfo */

$this->title = 'Создать контактные данные';
$this->params['breadcrumbs'][] = ['label' => 'Контактные данные', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
