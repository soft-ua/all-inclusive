<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ContactsInfo */

$this->title = 'Обновить контактные данные';
$this->params['breadcrumbs'][] = ['label' => 'Контактные данные', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title];
//$this->params['breadcrumbs'][] = ['label' => $model->idcontacts, 'url' => ['view', 'id' => $model->idcontacts]];
//$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="contacts-info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
