<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>



<!--    <div>--><?php //echo \yii\helpers\Html::label('Фото') ?><!--</div>-->
    <div><?php
        if(!isset($model->comment_img) || $model->comment_img == null || $model->comment_img == ''){
            echo \yii\helpers\Html::img(\common\components\UserComponent::getNoImage(), ['width' => 320,'class' => 'img img-responsive']);

        }else{
            echo \yii\helpers\Html::img(\common\components\UserComponent::getCommentImage($model->created_at,$model->comment_img,true), ['width' => 320,'class' => 'img img-responsive']);
        }
        ?>
    </div><br>
<!--    <div>--><?php //echo \yii\helpers\Html::fileInput('avatar') ?><!--</div><br>-->
<!--    --><?php //echo $form->field($model, 'comment_img')->fileInput(['multiple' => false, 'accept' => 'image/*']) ?>


    <?php
    echo $form->field($model, 'comment_img')->widget(\kartik\file\FileInput::classname(),[
        'name' => 'testimonials_img',
        'language' => 'ru',
        'options' => [
            'accept' => 'image/*',
            'multiple' => false,
            'testim_img' => $model->comment_img,
        ],
        'pluginOptions' => [
            'allowedFileExtensions' =>  ['jpg', 'png','gif','jpeg'],
//            'initialPreview' => $image,
            'showUpload' => false,
            'showRemove' => false,
            'dropZoneEnabled' => false,
            'overwriteInitial'=>false,
//            'maxPreviewFileSize'=>8192,
//            'maxFilePreviewSize'=>8192,
        ]
    ]);
    ?>

<!--    --><?php //echo $form->field($model, 'name_comment')->textInput(['maxlength' => true]) ?>

<!--    --><?php //echo $form->field($model, 'author_id')->textInput() ?>

<!--    --><?php //echo $form->field($model, 'id_comment')->textInput()->hiddenInput()->label(false) ?>

    <?php echo $form->field($model, 'guest_name')->textInput(['maxlength' => true]) ?>

<!--    --><?php //echo $form->field($model, 'guest_email')->textInput(['maxlength' => true]) ?>





    <?php echo $form->field($model, 'age')->textInput() ?>

    <?php echo $form->field($model, 'text_comment')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'available_comment')->textInput()->radioList([false=>'Нет', true=>'Да']) ?>

<!--    --><?php //echo $form->field($model, 'created_at')->textInput() ?>

<!--    --><?php //echo $form->field($model, 'updated_at')->textInput() ?>

<!--    --><?php //echo $form->field($model, 'deleted_at')->textInput() ?>

<!--    --><?php //echo $form->field($model, 'entity_id')->textInput() ?>

<!--    --><?php //echo $form->field($model, 'entity_type')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

