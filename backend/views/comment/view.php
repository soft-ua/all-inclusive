<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Comment */

$this->title = $model->id_comment;
$this->params['breadcrumbs'][] = ['label' => 'Отзыв', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id_comment], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id_comment], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот комментарий?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_comment',
            'name_comment',
//            'author_id',
            'comment_img.image',
            'age',
            'guest_name',
//            'guest_email:email',
            'text_comment:ntext',
            'available_comment',
            'created_at',
            'updated_at',
            'deleted_at',
//            'entity_id',
//            'entity_type',
        ],
    ]) ?>

</div>
