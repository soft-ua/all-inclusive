<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

$this->registerJs("
    $(document).ready(function() {
        $('.fancybox').fancybox();
        $('.fancybox-thumb').fancybox({
            prevEffect	: 'none',
            nextEffect	: 'none',
            helpers	: {
                title	: {
                    type: 'outside'
                },
                thumbs	: {
                    width	: 50,
                    height	: 50
                }
            }
        });
    });
");
?>
<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать отзыв', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id_comment',
//            'name_comment',
//            'author_id',
//            'comment_img',
            [
                'label' => 'Фотография',
                'format' => 'raw',
                'value' => function($data){
//                    return Html::img(\frontend\components\Common::getImageComment($data),[
                    if(is_file(Yii::getAlias("@frontend/web/uploads/comment/".date("d.m.Y_H-i-s",$data->created_at).'/'.$data->comment_img))) {
//                        return Html::img(\common\components\UserComponent::getCommentImage($data->created_at,$data->comment_img,true),[
//                            'alt'=>"$data->comment_img - gridview",
//                            'style' => 'width:90px;'
//                        ]);
                        return "<a href='" . \common\components\UserComponent::getCommentImage($data->created_at, $data->comment_img, true) . "' class='fancybox' data-fancybox-group='" . $data->comment_img . "'><img src='" . \common\components\UserComponent::getCommentImage($data->created_at, $data->comment_img) . "' class='img img-responsive' alt='" . $data->comment_img . "' width='100'></a>";
                    } else {
                        return "<a href='".\common\components\UserComponent::getNoImage()."' class='fancybox' data-fancybox-group='".$data->created_at."'><img src='".\common\components\UserComponent::getNoImage()."' class='img img-responsive' alt='no photo' width='100'></a>";
                    }

                },
            ],
            'guest_name',
            'age',
//            'guest_email:email',
//            'houseName',
//             'text_comment:ntext',
             'available_comment'
             => array(
                 'attribute' => 'available_comment',
                 'filter' => array(false=>'Нет',true=>'Да'),
                 'format' => 'raw',
                 'content'=>function($data){
                     return ($data['available_comment'] == true) ? 'Показано' : 'Спрятано';
                 },
             ),
             'created_at'=>[
                 'attribute' => 'created_at',
                 'format' => ['date', 'php:d.m.Y H:i:s'],
             ],
             'updated_at'=>[
                 'attribute' => 'updated_at',
                 'format' => ['date', 'php:d.m.Y H:i:s'],
             ],
            // 'deleted_at',
//             'entity_id',
            // 'entity_type',
//            'house.title',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
