<?php
use yii\bootstrap\Nav;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php if(is_file(\Yii::getAlias("@frontend/web/uploads/users/small_".\Yii::$app->user->id.".jpg")))
                { ?>
                    <img src="<?=\common\components\UserComponent::getUserImage(Yii::$app->user->id) ?>" class="img-circle" alt="User Image"/>
                <?php } else { ?>
                    <img src="<?=\common\components\UserComponent::getNoImageSqr() ?>" class="img-circle img-responsive" alt="User Image"/>
                <?php }?>

            </div>
            <div class="pull-left info">
                <p class="uppercase"><?=Yii::$app->user->identity->username ?></p>
            </div>
        </div>

        <?php echo Nav::widget(
            [
                'encodeLabels' => false,
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => '<i class="fa fa-home"></i><span><b>Главная</b></span>', 'url' => ['/']],
                    '<li class="header text-center">Дома</li>',

                    '<li class="treeview">
                        <a href="#"><i class="fa fa-info-circle"></i><span>Объявления о домах</span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">',
                            ['label' => '<i class="fa fa-info"></i> <span>Данные о домах</span>', 'url' => ['/house']],
//                            ['label' => '<i class="fa fa-photo"></i> <span>Фото Домов</span>', 'url' => ['/image']],
                            ['label' => '<i class="fa fa-cubes"></i> <span>Типы объявлений</span>', 'url' => ['/houses-type']],
                    '</ul>
                    </li>',
//                    ['label' => '<i class="fa fa-film"></i> <span>Слайдер</span>', 'url' => ['/slider']],
                    '<li class="header text-center">Отзывы</li>',
                    ['label' => '<i class="fa fa-commenting"></i> <span>Отзывы</span>', 'url' => ['/comment']],
                    '<li class="header text-center">Благотворительность</li>',

                    ['label' => '<i class="fa fa-gift"></i> <span>Благо Дари</span>', 'url' => ['/donate']],

//                    '<li class="treeview">
//                        <a href="#"><i class="fa fa-gift"></i><span>Благо Дари</span> <i class="fa fa-angle-left pull-right"></i></a>
//                        <ul class="treeview-menu">',
//                            ['label' => '<i class="fa fa-share-alt"></i> <span>Текст</span>', 'url' => ['/donate']],
//                            ['label' => '<i class="fa fa-file-photo-o"></i> <span>Фото</span>', 'url' => ['/donates-img']],
//                        '</ul>
//                    </li>',

                    '<li class="header text-center">Страницы</li>',
                    ['label' => '<i class="fa fa-pencil"></i> <span>О нас</span>', 'url' => ['/about-us']],
                    '<li class="treeview">
                        <a href="#"><i class="fa fa-envelope"></i><span>Контакты</span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">',
                            ['label' => '<i class="fa fa-phone"></i> <span>Контактные данные</span>', 'url' => ['/contactsinfo']],
                            ['label' => '<i class="fa fa-users"></i> <span>Наша команда</span>', 'url' => ['/our-team']],
                        '</ul>
                    </li>',
                    '<li class="header text-center">Настройки</li>',
                    ['label' => '<i class="fa fa-child"></i> <span>Администратор</span>', 'url' => ['/user']],
                    '<li class="treeview">',
                        '<a href="#"><i class="fa fa-gear"></i><span>Настройки профиля</span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">',
                            ['label' => '<i class="fa fa-user"></i> <span>Профиль</span>', 'url' => ['/settings']],
                            ['label' => '<i class="fa fa-edit"></i> <span>Изменить пароль</span>', 'url' => ['/change-password']],
                        '</ul>
                    </li>',
//                    ['label' => '<i class="fa fa-child"></i> <span>Пользователи</span>', 'url' => ['/user']],
//                    ['label' => '<i class="fa fa-user"></i> <span>Профиль</span>', 'url' => ['/settings']],
//                    ['label' => '<i class="fa fa-edit"></i> <span>Изменить пароль</span>', 'url' => ['/change-password']],
                    [
                        'label' => '<span class="glyphicon glyphicon-lock"></span> Sing in', //for basic
                        'url' => ['/site/login'],
                        'visible' =>Yii::$app->user->isGuest
                    ],
                ],
            ]
        );
        ?>

    </section>

</aside>
