<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?php echo Html::a('<span class="logo-lg">'.Yii::$app->name.'</span><span class="logo-mini">'.Yii::$app->name.'</span>', '//'.\Yii::$app->params['baseUrl'], ['class' => 'logo','target' => '_blank']) ?>
    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php if(is_file(\Yii::getAlias("@frontend/web/uploads/users/small_".\Yii::$app->user->id.".jpg")))
                        { ?>
                            <img src="<?=\common\components\UserComponent::getUserImage(Yii::$app->user->id) ?>" class="img-circle" alt="User Image"/>
                        <?php } else { ?>
                            <img src="<?=\common\components\UserComponent::getNoImageSqr() ?>" class="img-circle img-responsive" alt="User Image" style="float: left; margin-right: 10px"/>
                        <?php }?>
                        <span class="hidden-xs"><?=Yii::$app->user->identity->username ?></span>

                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">

                            <?php if(is_file(\Yii::getAlias("@frontend/web/uploads/users/small_".\Yii::$app->user->id.".jpg")))
                            { ?>
                                <img src="<?=\common\components\UserComponent::getUserImage(Yii::$app->user->id) ?>" class="img-circle" alt="User Image"/>
                            <?php } else { ?>
                                <img src="<?=\common\components\UserComponent::getNoImageSqr() ?>" class="img-circle img-responsive" alt="User Image"/>
                            <?php }?>

                            <p>
                                <?=Yii::$app->user->identity->username ?> - Администратор
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a(
                                    'Профиль',
                                    ['/default/settings'],
                                    ['class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Выход',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->

            </ul>
        </div>
    </nav>
</header>

