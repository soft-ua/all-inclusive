<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AboutUs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="about-us-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text_about')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $(function () {
        CKEDITOR.replace('aboutus-text_about');
    });
//    $('.cke_editable.cke_editable_themed.cke_contents_ltr.cke_show_borders').bind('blur keyup',function(){
//        console.log($('.cke_editable.cke_editable_themed.cke_contents_ltr.cke_show_borders p'));
//    }
</script>
