<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutUs */

$this->title = 'Обновить страницу "О нас"';
$this->params['breadcrumbs'][] = ['label' => 'О нас', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id_about, 'url' => ['view', 'id' => $model->id_about]];
$this->params['breadcrumbs'][] = ['label' => 'Обновить'];
//$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="about-us-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
