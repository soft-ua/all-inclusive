<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HousesType */

$this->title = 'Создать тип объявления';
$this->params['breadcrumbs'][] = ['label' => 'Тип объявления', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="houses-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
