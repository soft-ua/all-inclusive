<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\HousesTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тип объявления';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="houses-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    if(count($houseT)<2) {
    ?>
<!--        <p>-->
<!--            --><?php //echo Html::a('Создать тип объявления', ['create'], ['class' => 'btn btn-success']) ?>
<!--        </p>-->

            <?php $form = \yii\bootstrap\ActiveForm::begin([
                'method' => 'post',
                'action' => ['houses-type/create'],
            ]); ?>
            <div class="form-group">
            <?= Html::submitButton('Создать тип объявления', ['class' => 'btn btn-success']);
            ?>
            </div>

            <?php \yii\bootstrap\ActiveForm::end(); ?>
    <?php
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
//            'title',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
        ],
    ]); ?>
</div>
