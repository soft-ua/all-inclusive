var myMap, myPlacemark, coords, locationMap, coordsNew = [], firstGeoObject;

ymaps.ready(init);

function init () {

    coordHome = $('#house-location').val();
    console.log(coordHome);
    coordNew = coordHome.split(',');
    for (var i = 0; i < coordNew.length; i++) {
        coordNew[i] = Number(coordNew[i]);
        coordsNew.push(coordNew[i]);
    }

    coords = coordsNew;

    //Определяем начальные параметры карты
    myMap = new ymaps.Map('YMapsID', {
        center: coordsNew,
        zoom: 17
    });

    //Добавляем элементы управления на карту
    myMap.controls
        .add('zoomControl');

    locationMap = $('#house-address').val();
//            changeMap(locationMap);

    $('#house-address').bind('blur keyup',function(){
        locationMap = $('#house-address').val();
        changeMap(locationMap);
    })


    //Определяем метку и добавляем ее на карту
    firstGeoObject = new ymaps.Placemark(coordsNew,{
    }, {preset: "twirl#blueIcon", draggable: true});

    myMap.geoObjects.add(firstGeoObject);

    console.log(firstGeoObject);

    //Отслеживаем событие перемещения метки
    firstGeoObject.events.add("dragend", function (e) {
        coords = this.geometry.getCoordinates();
        savecoordinats();
    }, firstGeoObject);

    //Отслеживаем событие щелчка по карте
    myMap.events.add('click', function (e) {
        coords = e.get('coordPosition');
        savecoordinats();
    });

    function changeMap(locationMap){
        ymaps.geocode(locationMap,{
            results: 1
        }).then(function (res) {
            // Выбираем первый результат геокодирования.
            console.log(firstGeoObject != null);
            if(firstGeoObject != null){
                myMap.geoObjects.remove(firstGeoObject);
            }
            firstGeoObject = res.geoObjects.get(0),
                // Координаты геообъекта.
                coords = firstGeoObject.geometry.getCoordinates(),
                // Область видимости геообъекта.
                bounds = firstGeoObject.properties.get('boundedBy');
            console.log(coords);

            // Добавляем первый найденный геообъект на карту.
            myMap.geoObjects.add(firstGeoObject);

            // Масштабируем карту на область видимости геообъекта.
            myMap.setBounds(bounds, {
                // Проверяем наличие тайлов на данном масштабе.
                checkZoomRange: true
            });


        });
    }




    //Ослеживаем событие изменения области просмотра карты - масштаб и центр карты
    myMap.events.add('boundschange', function (event) {

        if (event.get('newZoom') != event.get('oldZoom')) {
            savecoordinats();
        }
        if (event.get('newCenter') != event.get('oldCenter')) {
            savecoordinats();
        }

    });

}

//Функция для передачи полученных значений в форму
function savecoordinats (){
    var new_coords = [coords[0].toFixed(4), coords[1].toFixed(4)];
    firstGeoObject.getOverlay().getData().geometry.setCoordinates(new_coords);
//		document.getElementById("latlongmet").value = new_coords;
    document.getElementById("house-location").value = new_coords;
//		document.getElementById("mapzoom").value = myMap.getZoom();
    var center = myMap.getCenter();
    var new_center = [center[0].toFixed(4), center[1].toFixed(4)];
//		document.getElementById("latlongcenter").value = new_coords;
}