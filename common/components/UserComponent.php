<?php

namespace common\components;

use yii\base\Component;

class UserComponent extends Component{

    public static function getUserImage($id, $original=false){

        $base = '//'.\Yii::$app->params['baseUrl'].'/uploads/users/';
        return $base.(($original) ? $id : 'small_'.$id).'.jpg';

    }

    public static function getCommentImage($time, $img, $original=false){
        $base = '//'.\Yii::$app->params['baseUrl'].'/uploads/comment/'.date('d.m.Y_H-i-s',$time).'/';

        return $base.(($original) ? $img : 'small_'.$img);
    }

    public static function getTeamImage($time, $img, $original=false){
        $base = '//'.\Yii::$app->params['baseUrl'].'/uploads/team/'.date('d.m.Y_H-i-s',$time).'/';
        return $base.(($original) ? $img : 'small_'.$img);
    }

    public static function getHouseImage($data, $img, $original=false){
        $base = '//'.\Yii::$app->params['baseUrl'].'/uploads/house/'.$data.'/';
        return $base.(($original) ? $img : 'small_'.$img);
    }

    public static function getDonateImg($data, $img, $original=false){
        $base = '//'.\Yii::$app->params['baseUrl'].'/uploads/donate/'.$data.'/';
        return $base.(($original) ? $img : 'small_'.$img);
    }

    public static function getDonateStrImgs($data, $img, $original=false){
        $base = '//'.\Yii::$app->params['baseUrl'].'/uploads/donate/'.$data.'/';
        return $base.(($original) ? $img : 'small_'.$img);
    }

    public static function getDonatesImage($data, $img, $original=false){
        $base = '//'.\Yii::$app->params['baseUrl'].'/uploads/donate/'.$data.'/';
        return $base.(($original) ? $img : 'small_'.$img);
    }

    public static function getNoImage(){
        $base = '//'.\Yii::$app->params['baseUrl'].'/uploads/nophoto.jpg';
        return $base;
    }

    public static function getNoImageSqr(){
        $base = '//'.\Yii::$app->params['baseUrl'].'/images/main/sqr_nophoto.jpg';
        return $base;
    }

    public static function getSrcJs(){
        $base = '/'.\Yii::$app->params['baseUrl'].'/';
        return $base;
    }
}