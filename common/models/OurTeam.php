<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "our_team".
 *
 * @property integer $id_team
 * @property string $name_team
 * @property string $img_team
 * @property string $office_team
 * @property integer $created_at
 * @property integer $updated_at
 */
class OurTeam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'our_team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_team', 'office_team'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name_team', 'office_team'], 'string', 'max' => 50],
//            [['img_team'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_team' => 'Id должности',
            'name_team' => 'Имя',
            'img_team' => 'Фото',
            'office_team' => 'Название должности',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios['create_team'] = ['name_team', 'img_team', 'office_team'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     * @return OurTeamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OurTeamQuery(get_called_class());
    }
}
