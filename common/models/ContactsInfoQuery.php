<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ContactsInfo]].
 *
 * @see ContactsInfo
 */
class ContactsInfoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ContactsInfo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ContactsInfo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}