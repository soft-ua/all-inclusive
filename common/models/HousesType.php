<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "houses_type".
 *
 * @property integer $id
 * @property string $name
 */
class HousesType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'houses_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер ID',
            'name' => 'Название'
        ];
    }

    /**
     * @inheritdoc
     * @return HousesTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HousesTypeQuery(get_called_class());
    }
}
