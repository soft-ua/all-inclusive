<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contacts_info".
 *
 * @property integer $idcontacts
 * @property string $email
 * @property string $skype
 * @property string $phone
 * @property string $address
 * @property string $yandexmaps
 */
class ContactsInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'phone', 'address'], 'required'],
            [['email', 'skype', 'phone'], 'string', 'max' => 50],
            [['address', 'yandexmaps','working_hours'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idcontacts' => 'Id',
            'email' => 'E-mail',
            'skype' => 'Skype',
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'working_hours' => 'Часы работы',
            'yandexmaps' => 'Координаты',
        ];
    }

    /**
     * @inheritdoc
     * @return ContactsInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContactsInfoQuery(get_called_class());
    }
}
