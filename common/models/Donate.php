<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "Donate".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $available_d
 * @property string $office_donate
 * @property string $str_imgs_donates
 * @property string $img_donates
 * @property string $user_id_donates
 * @property string $created_at
 * @property string $updated_at
 */
class Donate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'office_donate'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['available_d'], 'boolean'],
            [['description','str_imgs_donates'], 'string'],
            [['name'], 'string', 'max' => 45],
            [['office_donate','img_donates'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Автор',
            'description' => 'Описание',
            'available_d' => 'Доступно',
            'img_donates' => 'Главное фото',
            'str_imgs_donates' => 'Фотографии',
            'office_donate' => 'Должность и место работы',
            'user_id_donates' => 'ID пользователя',
            'created_at' => 'Создано',
            'updated_at' => 'Обновить',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(),['id' => 'user_id_donates']);
    }

    public function afterValidate(){
        $this->user_id_donates = \Yii::$app->user->identity->id;
    }

    public function afterSave(){
//        var_dump($this->id_donates);die;
        \Yii::$app->locator->cache->set('id',$this->id);
    }

    /**
     * @inheritdoc
     * @return DonateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DonateQuery(get_called_class());
    }
}
