<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Donate]].
 *
 * @see Donate
 */
class DonateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Donate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Donate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
