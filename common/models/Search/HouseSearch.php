<?php

namespace common\models\Search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\House;

/**
 * HouseSearch represents the model behind the search form about `common\models\House`.
 */
class HouseSearch extends House
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'type', 'available', 'user_id'], 'integer'],
            [['title', 'address', 'description', 'location', 'tour_link', 'str_imgs_house', 'img_house'], 'safe'],
//            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = House::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
//            'id' => $this->id,
            'price' => $this->price,
//            'bedroom' => $this->bedroom,
//            'livingroom' => $this->livingroom,
//            'kitchen' => $this->kitchen,
//            'garage' => $this->garage,
//            'room' => $this->room,
//            'floor' => $this->floor,
//            'hot' => $this->hot,
//            'new' => $this->new,
            'type' => $this->type,
            'available' => $this->available,
//            'recommend' => $this->recommend,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//            'deleted_at' => $this->deleted_at,
//            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
//            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'tour_link', $this->tour_link])
//            ->andFilterWhere(['like', 'str_imgs_house', $this->str_imgs_house])
//            ->andFilterWhere(['like', 'img_house', $this->img_house])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'description', $this->description])
//            ->andFilterWhere(['like', 'location', $this->location])
        ;

        return $dataProvider;
    }
}
