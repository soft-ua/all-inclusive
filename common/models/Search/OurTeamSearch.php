<?php

namespace common\models\Search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OurTeam;

/**
 * OurTeamSearch represents the model behind the search form about `common\models\OurTeam`.
 */
class OurTeamSearch extends OurTeam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_team'], 'integer'],
            [['name_team', 'img_team', 'office_team'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OurTeam::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_team' => $this->id_team,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name_team', $this->name_team])
            ->andFilterWhere(['like', 'img_team', $this->img_team])
            ->andFilterWhere(['like', 'office_team', $this->office_team]);

        return $dataProvider;
    }
}
