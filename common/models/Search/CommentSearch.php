<?php

namespace common\models\Search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Comment;

/**
 * CommentSearch represents the model behind the search form about `common\models\Comment`.
 */
class CommentSearch extends Comment
{
    public $houseName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['age', 'available_comment'], 'integer'],
            [['guest_name', 'comment_img'], 'safe'],
//            [['houseName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        $dataProvider->setSort([
            'attributes' => [
//                'houseName' => [
//                    'asc' => ['entity_id' => SORT_ASC],
//                    'desc' => ['entity_id' => SORT_DESC],
//                    'label' => 'Имя дома'
//                ],
//                'name_comment',
//                'guest_email',
                'comment_img',
                'age',
                'available_comment',
                'created_at',
                'updated_at',
                'guest_name',
            ]
        ]);

//        if (!($this->load($params) && $this->validate())) {
//            /**
//             * Жадная загрузка данных модели House
//             * для работы сортировки.
//             */
//            $query->joinWith(['house']);
//            return $dataProvider;
//        }

//        $query->joinWith(['house' => function ($q) {
//            $q->where('house.title LIKE "%' . $this->houseName . '%"');
//        }]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
//            'id_comment' => $this->id_comment,
//            'author_id' => $this->author_id,
//            'comment_img' => $this->comment_img,
            'age' => $this->age,
            'available_comment' => $this->available_comment,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//            'guest_name' => $this->guest_name,
//            'deleted_at' => $this->deleted_at,
//            'entity_id' => $this->entity_id,
//            'houseName' => $this->entity_id,
        ]);

        $query
//            ->andFilterWhere(['like', 'comment_img', $this->comment_img])
//            ->andFilterWhere(['like', 'age', $this->age])
            ->andFilterWhere(['like', 'guest_name', $this->guest_name])
//            ->andFilterWhere(['like', 'available_comment', $this->available_comment])
        ;

        return $dataProvider;
    }
}
