<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[HousesType]].
 *
 * @see HousesType
 */
class HousesTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return HousesType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return HousesType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
