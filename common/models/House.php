<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "house".
 *
 * @property integer $id
 * @property string $title
 * @property string $price
 * @property string $address
 * @property integer $bedroom
 * @property integer $livingroom
 * @property integer $kitchen
 * @property integer $garage
 * @property integer $room
 * @property integer $floor
 * @property string $description
 * @property string $location
 * @property integer $hot
 * @property integer $new
 * @property integer $tour_link
 * @property integer $type
 * @property integer $available
 * @property integer $recommend
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $user_id
 * @property integer $str_imgs_house
 * @property integer $img_house
 */
class House extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'house';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'address', 'location', 'type', 'available'], 'required'],
            [['type', 'price', 'created_at', 'updated_at', 'user_id',], 'integer'],
            [['available'], 'boolean'],
            [['description', 'str_imgs_house'], 'string'],
            [['title', 'address', 'tour_link', 'img_house'], 'string', 'max' => 255],
            [['location'], 'string', 'max' => 100],
//            [['str_imgs_house', 'img_house'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'price' => 'Цена, руб',
            'address' => 'Адрес',
//            'bedroom' => 'К-во спален',
//            'livingroom' => 'К-во жилых комнат',
//            'kitchen' => 'К-во кухонь',
//            'garage' => 'К-во гаражей',
//            'room' => 'Общее к-во комнат',
//            'floor' => 'К-во этажей',
            'description' => 'Описание',
            'img_house' => 'Главное фото',
            'str_imgs_house' => 'Фотографии',
            'tour_link' => 'Ссылка на тур',
            'location' => 'Координаты',
//            'hot' => 'Гарячее',
//            'new' => 'Новое',
            'type' => 'Тип',
            'available' => 'Отображать',
//            'recommend' => 'Рекомендовать',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
//            'deleted_at' => 'Удалено',
            'user_id' => 'ID пользователя',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios['projects'] = ['title', 'address', 'description', 'location', 'type', 'available'];

        return $scenarios;
    }

    public function getHousesType(){
        return $this->hasOne(HousesType::className(),['id' => 'type']);
    }

    public function getUser(){
        return $this->hasOne(User::className(),['id' => 'user_id']);
    }

    public function afterValidate(){
        $this->user_id = Yii::$app->user->identity->id;
    }

    public function afterSave(){
        Yii::$app->locator->cache->set('id',$this->id);
    }

    /**
     * @inheritdoc
     * @return HouseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HouseQuery(get_called_class());
    }
}
