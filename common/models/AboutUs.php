<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "about_us".
 *
 * @property integer $id_about
 * @property string $text_about
 */
class AboutUs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_us';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['text_about'], 'required'],
            [['text_about'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_about' => 'Id About',
            'text_about' => 'Текст о нас',
        ];
    }

    /**
     * @inheritdoc
     * @return AboutUsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AboutUsQuery(get_called_class());
    }
}
