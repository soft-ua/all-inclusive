<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id_comment
 * @property string $name_comment
 * @property integer $author_id
 * @property string $comment_img
 * @property string $age
 * @property string $guest_name
 * @property string $guest_email
 * @property string $text_comment
 * @property integer $available_comment
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $entity_id
 * @property string $entity_type
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['guest_name', 'available_comment'], 'required'],
            [['author_id', 'age', 'created_at', 'updated_at'], 'integer'],
            [['available_comment'], 'boolean'],
            [['text_comment'], 'string'],
            [['guest_email'], 'email'],
            [['comment_img'], 'string', 'max' => 255],
            [['guest_name'], 'string', 'max' => 50],
//            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
//            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => House::className(), 'targetAttribute' => ['entity_id' => 'id']],
//            [['entity_type'], 'exist', 'skipOnError' => true, 'targetClass' => HousesType::className(), 'targetAttribute' => ['entity_type' => 'id']],
        ];
    }

    public function getHouseName() {
        return $this->house['title'];
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios['front_comment'] = ['guest_name', 'guest_email', 'text_comment'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_comment' => 'Id Comment',
//            'name_comment' => 'Название комментария',
            'author_id' => 'ID пользователя',
            'guest_name' => 'Имя пользователя',
            'comment_img' => 'Фото',
            'age' => 'Возраст',
//            'guest_email' => 'Email пользователя',
            'text_comment' => 'Текст комментария',
            'available_comment' => 'Доступен',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
//            'deleted_at' => 'Удалено',
//            'entity_id' => 'ID объявления',
//            'entity_type' => 'Тип объявления',
//            'house.title' => 'Название объявления',
//            'houseName' => 'Название дома',
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(),['id' => 'author_id']);
    }

//    public function getHouse(){
//        return $this->hasOne(House::className(),['id' => 'entity_id']);
//    }

//    public function getHouseType(){
//        return $this->hasOne(HousesType::className(),['id' => 'entity_type']);
//    }

    public function afterValidate(){
        if(!Yii::$app->user->isGuest){
            $this->author_id = Yii::$app->user->identity->id;
            $this->guest_email = Yii::$app->user->identity->email;
        }
    }

    /**
     * @inheritdoc
     * @return CommentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommentQuery(get_called_class());
    }
}
