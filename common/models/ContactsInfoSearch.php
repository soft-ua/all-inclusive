<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ContactsInfo;

/**
 * ContactsInfoSearch represents the model behind the search form about `common\models\ContactsInfo`.
 */
class ContactsInfoSearch extends ContactsInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcontacts'], 'integer'],
            [['email', 'skype', 'phone', 'address', 'yandexmaps', 'working_hours'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactsInfo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idcontacts' => $this->idcontacts,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'skype', $this->skype])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'working_hours', $this->working_hours])
            ->andFilterWhere(['like', 'yandexmaps', $this->yandexmaps]);

        return $dataProvider;
    }
}
