<?php

namespace frontend\filters;

use common\models\House;
use yii\base\ActionFilter;
use yii\web\HttpException;

class FilterHouse extends ActionFilter{

    public function beforeAction($action){
        $id = \Yii::$app->request->get("id");
        $model = House::findOne($id);

        if($model == null){
            throw new  HttpException(404,'Неверно указан Id дома');
            return false;
        }

        return parent::beforeAction($action);

    }


    public function afterAction($action,$result){
       return parent::afterAction($action,$result);
    }


}