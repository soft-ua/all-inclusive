<?php
/**
 * Created by PhpStorm.
 * User: ласточка
 * Date: 08.02.2016
 * Time: 14:47
 */
namespace frontend\components;

use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\base\Component;
use yii\helpers\BaseFileHelper;
use yii\helpers\Url;

class Common extends Component{

    public function sendMail($subject,$text,$emailFrom='betzel@i.ua',$nameFrom='Receiver'){
        if(\Yii::$app->mail->compose()
            ->setFrom(['betzel@yandex.ru' => \Yii::$app->name])
            ->setTo([$emailFrom => $nameFrom])
            ->setSubject($subject)
            ->setHtmlBody($text)
            ->send()){
            return true;
        }
    }

    public static function getImageHouse($data, $original = false){

        $image = [];
        $base = '\/\/'.\Yii::$app->params['baseUrl'];

//        var_dump($base);die;

        if($original){
            if($data['name'] == null || $data['name'] == ''){
                $image[] = $base.'/uploads/nophoto.jpg';
            }else {
                $image[] = $base . '/uploads/house/' . $data['id_image'] . '/' . $data['name'];
            }
        }
        else{
            if($data['name'] == null || $data['name'] == ''){
                $image[] = $base.'/uploads/nophoto.jpg';
            }else {
                $image[] = $base . '/uploads/house/' . $data['id_image'] . '/small_' . $data['name'];
            }
        }

        return $image;
    }

    public static function getImageComment($data, $original = false){

        $image = [];
        $base = '\/\/'.\Yii::$app->params['baseUrl'];

        if($original){
            if($data['comment_img'] == null || $data['comment_img'] == ''){
                $image[] = $base.'/uploads/nophoto.jpg';
            }else{
                $image[] = $base.'/uploads/comment/'.date('d.m.Y_H-i-s',$data['created_at']).'/'.$data['comment_img'];
            }
        }else{
            if($data['comment_img'] == null || $data['comment_img'] == ''){
                $image[] = $base.'/uploads/nophoto.jpg';
            }else {
                $image[] = $base . '/uploads/comment/' . date('d.m.Y_H-i-s', $data['created_at']) . '/small_' . $data['comment_img'];
            }
        }

        return $image;
    }

    public static function getImageTeam($data, $original = false){

        $image = [];
        $base = '\/\/'.\Yii::$app->params['baseUrl'];

        if($original){
            if($data['img_team'] == null || $data['img_team'] == ''){
                $image[] = $base.'/uploads/nophoto.jpg';
            }else {
                $image[] = $base . '/uploads/team/' . date('d.m.Y_H-i-s', $data['created_at']) . '/' . $data['img_team'];
            }
        }else{
            if($data['img_team'] == null || $data['img_team'] == ''){
                $image[] = $base.'/uploads/nophoto.jpg';
            }else {
                $image[] = $base . '/uploads/team/' . date('d.m.Y_H-i-s', $data['created_at']) . '/small_' . $data['img_team'];
            }
        }

        return $image;
    }

    public static function substr($text,$start=0,$end=50){

        return mb_substr($text,$start,$end);
    }

    public function getUrlHouse($row){

        return Url::to(['/main/main/view-house', 'id' => $row['id']]);
    }

    public function getUrlProject($row){

        return Url::to(['/main/main/view-project', 'id' => $row['id']]);
    }

    public static function getMainwatermark($row){

        $id = $row['id'];
        $path_dir = \Yii::getAlias("@frontend/web/images/main/watermark/");
        $allink_01 = $path_dir . "allink_01.png";
        $path = \Yii::getAlias("@frontend/web/uploads/house/".$id);
        $images_arr = [];

        try {
            if(is_dir($path)) {


                $file = $row['img_house'];

                if (is_file($path.'/'.basename($file)) && !is_file($path.'/wtmarked_'.basename($file))) {
                    $images_arr[] = basename($file);
                    $image = ''.basename($file);
                    $pos = mb_strripos($file, '.');
                    $new_str = mb_substr($file, $pos);

                    $size = getimagesize($path.'/'.$file);
                    $width = $size[0];
                    $height = $size[1];

                    $sizeWMark = getimagesize($allink_01);
                    $widthWMark = $sizeWMark[0];
                    $heightWMark = $sizeWMark[1];

                    \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);





                if($width <= 5000 && $height <= 5000){
                        $new_name = $path. '/' .time()."_allink.png";

                        //открываем исходное изображение
                        $src = imagecreatefrompng($allink_01);

//создаем дескриптор для измененного изображения
//                        $dst = imagecreatetruecolor($width, $height);
                        if($new_str == '.jpg' || $new_str == '.jpeg'){
                            $dst = imagecreatefromjpeg($path.'/'.$image);
                        }elseif($new_str == '.png'){
                            $dst = imagecreatefrompng($path.'/'.$image);
                        }elseif($new_str == '.gif'){
                            $dst = imagecreatefromgif($path.'/'.$image);
                        }

//устанавливаем прозрачность
                        \frontend\components\Common::setTransparency($dst, $src);

//изменяем размер
                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $height);



//сохраняем уменьшенное изображение в файл
                        ImagePNG($dst, $new_name);

                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

//закрываем дескрипторы исходного и уменьшенного изображений
                        ImageDestroy($src);
                        ImageDestroy($dst);

                        unlink($new_name);

                }elseif($width > 5000 && $height <= 5000){
                        $new_name = $path. '/' .time()."_allink.png";

                        //открываем исходное изображение
                        $src = imagecreatefrompng($allink_01);

                        if($new_str == '.jpg' || $new_str == '.jpeg'){
                            $dst = imagecreatefromjpeg($path.'/'.$image);
                        }elseif($new_str == '.png'){
                            $dst = imagecreatefrompng($path.'/'.$image);
                        }elseif($new_str == '.gif'){
                            $dst = imagecreatefromgif($path.'/'.$image);
                        }

                        \frontend\components\Common::setTransparency($dst, $src);

                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $height);



                        ImagePNG($dst, $new_name);

                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

                        ImageDestroy($src);
                        ImageDestroy($dst);

                        unlink($new_name);

                }elseif($width <= 5000 && $height > 5000){
                    $new_name = $path. '/' .time()."_allink.png";

                    $src = imagecreatefrompng($allink_01);

                    if($new_str == '.jpg' || $new_str == '.jpeg'){
                        $dst = imagecreatefromjpeg($path.'/'.$image);
                    }elseif($new_str == '.png'){
                        $dst = imagecreatefrompng($path.'/'.$image);
                    }elseif($new_str == '.gif'){
                        $dst = imagecreatefromgif($path.'/'.$image);
                    }

                    \frontend\components\Common::setTransparency($dst, $src);

                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $heightWMark);



                    ImagePNG($dst, $new_name);

                    $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

                    ImageDestroy($src);
                    ImageDestroy($dst);

                    unlink($new_name);

                }else{

                    $new_name = $path. '/' .time()."_allink.png";

                    $src = imagecreatefrompng($allink_01);

                    if($new_str == '.jpg' || $new_str == '.jpeg'){
                        $dst = imagecreatefromjpeg($path.'/'.$image);
                    }elseif($new_str == '.png'){
                        $dst = imagecreatefrompng($path.'/'.$image);
                    }elseif($new_str == '.gif'){
                        $dst = imagecreatefromgif($path.'/'.$image);
                    }

                    \frontend\components\Common::setTransparency($dst, $src);

                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $heightWMark);


                    ImagePNG($dst, $new_name);

                    $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

                    ImageDestroy($src);
                    ImageDestroy($dst);

                    unlink($new_name);

                }


                }
            }
        }
        catch(\yii\base\Exception $e){}

    }

    public static function setTransparency($new_image, $image_source)
    {
        $transparencyIndex = imagecolortransparent($image_source);
        $transparencyColor = array('red' => 255, 'green' => 255, 'blue' => 255);


        if ($transparencyIndex >= 0)
            $transparencyColor = imagecolorsforindex($image_source, $transparencyIndex);

        $transparencyIndex = imagecolorallocatealpha($new_image, $transparencyColor['red'], $transparencyColor['green'], $transparencyColor['blue'], 0);
        imagefill($new_image, 0, 0, $transparencyIndex);
        imagecolortransparent($new_image, $transparencyIndex);
    }

}