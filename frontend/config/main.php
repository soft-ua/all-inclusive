<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'language' => 'ru-RU',
//    'language' => 'ru',
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'main',
    'modules' => [
        'main' => [
            'class' => 'app\modules\main\Module',
        ],
//        'cabinet' => [
//            'class' => 'app\modules\cabinet\Module',
//        ],
    ],
    'components' => [
        'mail' => [
            'class'            => 'zyx\phpmailer\Mailer',
            'viewPath'         => '@common/mail',
            'useFileTransport' => false,
            'config'           => [
                'mailer'     => 'smtp',
                'host'       => 'smtp.yandex.ru',
                'port'       => '465',
                'smtpsecure' => 'ssl',
                'smtpauth'   => true,
                'username'   => 'betzel@yandex.ru',
                'password'   => '223988',
            ],
        ],
        'main' => [
            'class' => 'app\modules\main\Module',
        ],
        'common' => [
            'class' => 'frontend\components\Common',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/main/main/login',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'main/',
                '/list-projects' => 'main/main/list-projects',
                '/list-houses' => 'main/main/list-houses',
                '/contact' => 'main/main/contact',
                '/give-good' => 'main/main/dari-dobro',
                '/testimonials' => 'main/main/testimonials',
                '/list' => 'main/main/',
                '/about' => 'main/main/about',
//                '/settings' => 'default/settings',
//                '/change-password' => 'default/change-password',
//                'pages/<view:[a-zA-Z0-9-]+>' => 'main/main/page',
                'view-house/<id:\d+>' => 'main/main/view-house',
                'view-project/<id:\d+>' => 'main/main/view-project',
//                'cabinet/<action_cabinet:(settings|change-password)>' => 'cabinet/default/<action_cabinet>'
            ],
        ],
    ],
    'params' => $params,
];
