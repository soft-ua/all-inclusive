<?php
namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class MainAsset extends  AssetBundle{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'resource/css/style.css',
        'resource/css/reset.css',
        'resource/css/screen.css',
        'resource/css/animate.css',
        'resource/css/font-awesome.css',
        'resource/css/jquery.fancybox.css',
    ];

    public $js = [
//        'resource/js/jquery.tubular.1.0.js',
        'resource/js/animate-css.js',
        'resource/js/waypoints.min.js',
        'resource/js/jquery.fancybox.pack.js',
//        'resource/js/yandex.api2.js',
        'resource/js/script.js',
        '//api-maps.yandex.ru/2.0/?load=package.full&amp;lang=ru-RU',
        'resource/js/ya.map.front.js',
//        'resource/js/jssor.js',
//        'resource/js/jssor.slider.js',
//        'resource/js/jssor.custom.js',
    ];

    public $depends = [
        'yii\web\YiiAsset', // yii.js, jquery.js
        'yii\bootstrap\BootstrapAsset', // bootstrap.css
        'yii\bootstrap\BootstrapPluginAsset' // bootstrap.js
    ];

    public $jsOptions = [
//        'position' =>  View::POS_HEAD,
    ];


}

