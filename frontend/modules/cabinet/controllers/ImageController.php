<?php

namespace app\modules\cabinet\controllers;

use common\controllers\AuthController;
use common\models\Slider;
use Yii;
use common\models\Image;
use common\models\Search\ImageSearch;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;
use Imagine\Image\Point;
use Imagine\Image\Box;
use yii\helpers\BaseFileHelper;
use \kartik\depdrop\Depdrop;

/**
 * ImageController implements the CRUD actions for Image model.
 */
class ImageController extends AuthController
{
    public $layout = "inclusivemain";

    /**
     * Lists all Image models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Image model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Image model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Image();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['step2', 'id' => $model->id_image]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Image model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $image = [];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['step2', 'id' => $model->id_image]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Image model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionStep2(){
        $id = Yii::$app->locator->cache->get('id');
        $model = Image::findOne($id);
        $image = [];
        if($name = $model->name){
            $image[] =  '<img src="/uploads/house/' . $model->id_image . '/' . $name . '" width=250>';
        }

        if(Yii::$app->request->isPost){
            $this->redirect(Url::to(['image/']));
        }

        $path = Yii::getAlias("@frontend/web/uploads/house/".$model->id_image);
        $images_add = [];

        try {
            if(is_dir($path)) {
                $files = \yii\helpers\FileHelper::findFiles($path);

                foreach ($files as $file) {
                    if (strstr($file, "small_") && !strstr($file, "general")) {
                        $images_add[] = '<img src="/uploads/house/' . $model->id_image . '/' . basename($file) . '" width=250>';
                    }
                }
            }
        }
        catch(\yii\base\Exception $e){}


        return $this->render("step2",['model' => $model,'image' => $image, 'images_add' => $images_add]);
    }



    public function actionFileUploadGeneral(){

        if(Yii::$app->request->isPost){
            $id = Yii::$app->request->post("id");
            $path = Yii::getAlias("@frontend/web/uploads/house/".$id);
            BaseFileHelper::createDirectory($path);
            $model = Image::findOne($id);
//            $model->scenario = 'step2';

            $file = UploadedFile::getInstance($model,'name');
            $name = $file->name;
            $file->saveAs($path .DIRECTORY_SEPARATOR .$name);

            $image  = $path .DIRECTORY_SEPARATOR .$name;
            $new_name = $path .DIRECTORY_SEPARATOR."small_".$name;

            $model->name = $name;
            $model->save();

            $size = getimagesize($image);
            $width = $size[0];
            $height = $size[1];

            \yii\imagine\Image::frame($image, 0, '666', 0)
                ->crop(new Point(0, 0), new Box($width, $height))
                ->resize(new Box(241,241))
                ->save($new_name, ['quality' => 100]);

            return true;

        }
    }


//    public function actionFileUploadImages(){
//        if(Yii::$app->request->isPost){
//            $id = Yii::$app->request->post("id");
//            $path = Yii::getAlias("@frontend/web/uploads/house/".$id);
//            BaseFileHelper::createDirectory($path);
//            $file = UploadedFile::getInstanceByName('images');
//            $name = $file->name;
//            $file->saveAs($path .DIRECTORY_SEPARATOR .$name);
//
//            $image = $path .DIRECTORY_SEPARATOR .$name;
//            $new_name = $path .DIRECTORY_SEPARATOR."small_".$name;
//
//            $size = getimagesize($image);
//            $width = $size[0];
//            $height = $size[1];
//
//            \yii\imagine\Image::frame($image, 0, '666', 0)
//                ->crop(new Point(0, 0), new Box($width, $height))
//                ->resize(new Box(1000,644))
//                ->save($new_name, ['quality' => 100]);
//
//            sleep(1);
//            return true;
//
//        }
//    }

// start kartik

// THE CONTROLLER
    public function actionSubcat1() {

        $house = \common\models\House::find()->all();

        $house1 = \common\models\House::find()->where('type = 1')->all();
        $houses1 = \yii\helpers\ArrayHelper::map($house,'id','title');

        $house2 = \common\models\House::find()->where('type = 2')->all();
        $houses2 = \yii\helpers\ArrayHelper::map($house,'id','title');

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $param1 = null;
                $param2 = null;
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];
                    $param1 = $params[0]; // get the value of input-type-1
                    $param2 = $params[1]; // get the value of input-type-2
                }

                $out = self::getSubCatList1($cat_id, $param1, $param2);
                // the getSubCatList1 function will query the database based on the
//                 cat_id, param1, param2 and return an array like below:
                 [
                    'group1'=>[
                        ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                        ['id'=>'<sub-cat-id-2>', 'name'=>'<sub-cat-name2>']
                    ],
                    'group2'=>[
                        ['id'=>'<sub-cat-id-3>', 'name'=>'<sub-cat-name3>'],
                        ['id'=>'<sub-cat-id-4>', 'name'=>'<sub-cat-name4>']
                    ]
                 ];


                $selected = self::getDefaultSubCat($cat_id);
                // the getDefaultSubCat function will query the database
                // and return the default sub cat for the cat_id

                echo Json::encode(['output'=>$out, 'selected'=>$selected]);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

//    end kartik


    /**
     * Finds the Image model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Image the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Image::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }
}
