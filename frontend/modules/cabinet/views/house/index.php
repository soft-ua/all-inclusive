<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\HouseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Дома';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="house-index container inner">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать объявление о доме', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title',
            'price',
            'address',
            //'bedroom',
            // 'livingroom',
            // 'kitchen',
            // 'garage',
            'room',
            'floor',
            // 'description:ntext',
            // 'location',
            // 'hot',
            // 'new',
             'type',
            // 'available',
            // 'recommend',
            // 'created_at',
            // 'updated_at',
            // 'deleted_at',
            // 'user_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
