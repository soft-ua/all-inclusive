<section class="inner">
    <div class="container">
        <?php $form = \yii\bootstrap\ActiveForm::begin(); ?>

        <div class="row">
            <?php
            echo $form->field($model, 'general_image')->widget(\kartik\file\FileInput::classname(),[
                'options' => [
                    'accept' => 'image/*',
                ],
                'pluginOptions' => [
                    'uploadUrl' => \yii\helpers\Url::to(['file-upload-general']),
                    'uploadExtraData' => [
                        'id' => $model->id,
//                        'name' => $model
                    ],
                    'allowedFileExtensions' =>  ['jpg', 'png','gif'],
                    'initialPreview' => $image,
                    'showUpload' => true,
                    'showRemove' => true,
                    'dropZoneEnabled' => true
                ]
            ]);
            ?>

        </div>

        <div class="row">
            <?php
            echo \yii\helpers\Html::label('Фото');

            echo \kartik\file\FileInput::widget([
                'name' => 'images',
                'options' => [
                    'accept' => 'image/*',
                    'multiple'=>false,
                ],
                'pluginOptions' => [
                    'uploadUrl' => \yii\helpers\Url::to(['file-upload-images']),
                    'uploadExtraData' => [
                        'id' => $model->id,
                    ],
                    'overwriteInitial' => false,
                    'allowedFileExtensions' =>  ['jpg', 'png','gif'],
                    'initialPreview' => $images_add,
                    'showUpload' => true,
                    'showRemove' => true,
                    'dropZoneEnabled' => true
                ]
            ]);
            ?>

        </div>

        <div class="form-group">
            <?= \yii\helpers\Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php \yii\bootstrap\ActiveForm::end(); ?>
    </div>
</section>