<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\House */

$this->title = 'Создать объявление о доме';
$this->params['breadcrumbs'][] = ['label' => 'Дома', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="house-create container inner">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
