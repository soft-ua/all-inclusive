<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\House */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="house-form container inner">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <div id="map_canvas" style="width:80%; height:380px;margin: auto;"></div><br/>

    <?php echo $form->field($model, 'bedroom')->textInput() ?>

    <?php echo $form->field($model, 'livingroom')->textInput() ?>

    <?php echo $form->field($model, 'kitchen')->textInput() ?>

    <?php echo $form->field($model, 'garage')->textInput() ?>

    <?php echo $form->field($model, 'room')->textInput() ?>

    <?php echo $form->field($model, 'floor')->textInput() ?>

    <?php echo $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'location')->textInput()->hiddenInput()->label(false) ?>

    <?php echo $form->field($model, 'hot')->radioList(['Нет', 'Да']) ?>

    <?php echo $form->field($model, 'new')->radioList(['Нет', 'Да']) ?>

    <?php echo $form->field($model, 'type')->dropDownList(['Готовые Дома', 'Реализованные проекты']) ?>

    <?php echo $form->field($model, 'available')->radioList(['Нет', 'Да']) ?>

    <?php echo $form->field($model, 'recommend')->radioList(['Нет', 'Да']) ?>

    <?php echo $form->field($model, 'created_at')->textInput() ?>

    <?php echo $form->field($model, 'updated_at')->textInput() ?>

    <?php echo $form->field($model, 'deleted_at')->textInput() ?>

    <?php echo $form->field($model, 'user_id')->textInput() ?>

    <?php
    $this->registerJs("
       var geocoder;
        var map;
        var marker;
         var markers = [];

        function initialize(){

              var latlng = new google.maps.LatLng(59.9342802,30.335098600000038);

            var options = {
//                zoom: 12,
                center: latlng,
            };
            map = new google.maps.Map(document.getElementById('map_canvas'), options);
            geocoder = new google.maps.Geocoder();

        }

          function DeleteMarkers() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
                }
                markers = [];
            }

        function findLocation(val){

            geocoder.geocode( {'address': val}, function(results, status) {

            var location = results[0].geometry.location
            map.setCenter(location)
            map.setZoom(15)
            DeleteMarkers()

            $('#house-location').val(location)

             marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: location
            });

          google.maps.event.addListener(marker, 'dragend', function()
        {
                    $('#house-location').val(marker.getPosition())
        });

        markers.push(marker);

        })
        }

        $(document).ready(function() {

            initialize();

            if( $('#house-address').val()){
             _location = $('#house-address').val()
               findLocation(_location)
               }

            $('#house-address').bind('blur keyup',function(){
               _location = $('#house-address').val()
               findLocation(_location)
            })


        });"

    );
    ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
