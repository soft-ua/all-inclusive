<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\donate */

$this->title = 'Создать благотворительный отзыв';
$this->params['breadcrumbs'][] = ['label' => 'Благотворительный отзыв', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donate-create container inner">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
