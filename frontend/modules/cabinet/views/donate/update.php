<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\donate */

$this->title = 'Обновить благотворительный отзыв: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Благотворительные отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="donate-update container inner">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
