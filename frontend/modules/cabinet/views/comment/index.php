<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Comment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_comment',
            'name_comment',
            'author_id',
            'guest_name',
            'guest_email:email',
            // 'text_comment:ntext',
            // 'available_comment',
            // 'created_at',
            // 'updated_at',
            // 'deleted_at',
            // 'entity_id',
            // 'entity_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
