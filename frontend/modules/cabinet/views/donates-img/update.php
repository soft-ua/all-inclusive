<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DonatesImg */

$this->title = 'Update Donates Img: ' . $model->id_donates;
$this->params['breadcrumbs'][] = ['label' => 'Donates Imgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_donates, 'url' => ['view', 'id' => $model->id_donates]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="donates-img-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
