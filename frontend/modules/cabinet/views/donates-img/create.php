<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DonatesImg */

$this->title = 'Create Donates Img';
$this->params['breadcrumbs'][] = ['label' => 'Donates Imgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donates-img-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
