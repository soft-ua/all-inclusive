<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DonatesImg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="donates-img-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_donates')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_donates')->textInput() ?>

    <?= $form->field($model, 'available_donates')->textInput() ?>

    <?= $form->field($model, 'donate_text')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
