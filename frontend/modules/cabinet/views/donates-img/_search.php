<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Search\DonatesImgSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="donates-img-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_donates') ?>

    <?= $form->field($model, 'name_donates') ?>

    <?= $form->field($model, 'main_donates') ?>

    <?= $form->field($model, 'available_donates') ?>

    <?= $form->field($model, 'donate_text') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
