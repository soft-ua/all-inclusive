<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OurTeam */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="our-team-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_team')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'img_team')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'office_team')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
