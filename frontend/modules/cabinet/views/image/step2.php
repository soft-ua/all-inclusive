<div class="container inner">
<?php $form = \yii\bootstrap\ActiveForm::begin(); ?>

    <div class="row">
        <?php
        echo $form->field($model, 'name')->widget(\kartik\file\FileInput::classname(),[
            'options' => [
                'accept' => 'image/*',
            ],
            'pluginOptions' => [
                'uploadUrl' => \yii\helpers\Url::to(['file-upload-general']),
                'uploadExtraData' => [
                    'id_image' => $model->id_image,
                ],
                'allowedFileExtensions' =>  ['jpg', 'png','gif'],
                'initialPreview' => $image,
                'showUpload' => true,
                'showRemove' => false,
                'dropZoneEnabled' => false
            ]
        ]);
        ?>

    </div>

<!--    <div class="row">-->
<!--        --><?php
//
//        echo $form->field($model, 'name')->widget(\kartik\file\FileInput::classname(),[
//            'name' => 'images',
//            'options' => [
//                'accept' => 'image/*',
//                'multiple'=>true
//            ],
//            'pluginOptions' => [
//                'uploadUrl' => \yii\helpers\Url::to(['file-upload-images']),
//                'uploadExtraData' => [
//                    'id' => $model->id,
//                ],
//                'overwriteInitial' => false,
//                'allowedFileExtensions' =>  ['jpg', 'png','gif'],
//                'initialPreview' => $images_add,
//                'showUpload' => true,
//                'showRemove' => false,
//                'dropZoneEnabled' => false
//            ]
//        ]);
//        ?>
<!---->
<!--    </div>-->

<!--    --><?php //echo $form->field($model, 'main')->radioList(['Нет', 'Да']) ?>
<!---->
<!--    --><?php //echo $form->field($model, 'available')->radioList(['Нет', 'Да']) ?>


    <div class="form-group">
        <?= \yii\helpers\Html::submitButton($model->isNewRecord ? 'Создать Фото' : 'Обновить Фото', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php \yii\bootstrap\ActiveForm::end(); ?>
</div>