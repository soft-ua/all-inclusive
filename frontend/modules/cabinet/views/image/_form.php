<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\depdrop\Depdrop;

/* @var $this yii\web\View */
/* @var $model common\models\Image */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$slider = \common\models\Slider::find()->all();
$sliders = \yii\helpers\ArrayHelper::map($slider,'id','name');

$house = \common\models\House::find()->all();
$houses = \yii\helpers\ArrayHelper::map($house,'id','title');
$housesTypes = \yii\helpers\ArrayHelper::map($house,'id','type');

$houseDTypes = \common\models\House::find()->select('type')->all();
$houseDistinctTypes = \yii\helpers\ArrayHelper::map($houseDTypes,'id','type');


$house1 = \common\models\House::find()->where('type = 0')->all();
$houses1 = \yii\helpers\ArrayHelper::map($house,'id','title');

$house2 = \common\models\House::find()->where('type = 1')->all();
$houses2 = \yii\helpers\ArrayHelper::map($house,'id','title');
?>

<div class="image-form container inner">

    <?php $form = ActiveForm::begin([
//        'enableClientValidation' => true,
//        'enableAjaxValidation' => true,
    ]); ?>

<!--  kartik  -->

    <?php

//    // Parent
//    echo $form->field($model, 'entity_type')->dropDownList([0=>'Готовые Дома', 1=>'Реализованные проекты'], ['id'=>'cat-id']);
//
//    // Additional input fields passed as params to the child dropdown's pluginOptions
//    echo Html::hiddenInput('input-type-1', 'Additional value 1', ['id'=>'input-type-1']);
//    echo Html::hiddenInput('input-type-2', 'Additional value 2', ['id'=>'input-type-2']);
//
//    // Child # 1
//    echo $form->field($model, 'entity_id')->widget(DepDrop::classname(), [
//        'type'=>DepDrop::TYPE_SELECT2,
//        'data'=>[2 => 'Tablets'],
//        'options'=>['id'=>'subcat1-id', 'placeholder'=>'Выберите ...'],
//        'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
//        'pluginOptions'=>[
//            'depends'=>['cat-id'],
//            'url'=>\yii\helpers\Url::to(['/main/main/subcat1']),
//            'params'=>['input-type-1', 'input-type-2']
//        ]
//    ]);

?>
<!--  end kartik  -->


    <?php //echo $form->field($model, 'name')->textInput() ?>


<!--    <div class="row">-->
<!--        --><?php
//        echo $form->field($model, 'name')->widget(\kartik\file\FileInput::classname(),[
//            'options' => [
//                'accept' => 'image/*',
//            ],
//            'pluginOptions' => [
//                'uploadUrl' => \yii\helpers\Url::to(['file-upload-general']),
//                'uploadExtraData' => [
//                    'id' => $model->id,
//                ],
//                'allowedFileExtensions' =>  ['jpg', 'png','gif'],
//                'initialPreview' => $image,
//                'showUpload' => true,
//                'showRemove' => false,
//                'dropZoneEnabled' => false
//            ]
//        ]);
//        ?>
<!---->
<!--    </div>-->

    <?php echo $form->field($model, 'main')->radioList(['Нет', 'Да']) ?>

    <?php echo $form->field($model, 'available')->radioList(['Нет', 'Да']) ?>



    <?= $form->field($model, 'slider_id')->dropDownList($sliders, ['prompt'=> 'Укажите название слайдера']) ?>

    <?php echo $form->field($model, 'entity_type')->dropDownList([1=>'Готовые Дома', 2=>'Реализованные проекты'],['prompt'=> 'Укажите тип']) ?>

    <?php echo $form->field($model, 'entity_id')->dropDownList($houses, ['prompt'=> 'Укажите название объявления']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
