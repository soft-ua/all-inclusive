<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фотографии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-index container inner">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить фотографию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id_image',
//            'name',
            [
                'label' => 'Фотография',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(\frontend\components\Common::getImageHouse($data),[
                        'alt'=>"$data->name - gridview",
                        'style' => 'width:70px;'
                    ]);
                },
            ],
            'main' => array(
                'attribute' => 'main',
                'filter' => array(0=>'Нет',1=>'Да'),
            ),
            'available'
            => array(
                'attribute' => 'available',
                'filter' => array(0=>'Нет',1=>'Да'),
                'format' => 'raw',
            ),
//            'slider_id',
//            'slider.name',
            'sliderName',
//             'entity_id',
//            'house.name',
            'houseName',
            'entity_type' => array(
                'attribute' => 'entity_type',
                'filter' => array(1=>'Дома на продажу',2=>'Реализованные проекты'),
            ),
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
