<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Search\ImageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-search container inner">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_image') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'main') ?>

    <?= $form->field($model, 'available') ?>

    <?= $form->field($model, 'slider_id') ?>

    <?php // echo $form->field($model, 'entity_id') ?>

    <?php // echo $form->field($model, 'entity_type') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сброс', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
