<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Image */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-view container inner">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_image' => $model->id_image], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_image' => $model->id_image], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что желаете удалить эту фотографию?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_image',
            'name',
            'main',
            'available',
            'slider_id',
            'entity_id',
            'entity_type',
        ],
    ]) ?>

</div>
