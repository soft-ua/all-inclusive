<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HousesType */

$this->title = 'Create Houses Type';
$this->params['breadcrumbs'][] = ['label' => 'Houses Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="houses-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
