<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\HousesTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Houses Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="houses-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Houses Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'house_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
