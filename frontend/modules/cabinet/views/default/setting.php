<div class="default-form container">

    <div class="col-sm-3">
        <?php $form = \yii\bootstrap\ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

        <?php echo \yii\helpers\Html::img(\common\components\UserComponent::getUserImage(Yii::$app->user->id), ['width' => 200]) ?>
    </div>

    <div class="col-sm-9"><?php echo $form->field($model,'username') ?>
        <?php echo $form->field($model,'email') ?>
        <?php echo \yii\helpers\Html::label('Avatar') ?>
        <?php echo \yii\helpers\Html::fileInput('avatar') ?>
        <br>
        <br>


        <?php echo  \yii\helpers\Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success btn-right' : 'btn btn-primary']) ?>

        <?php \yii\bootstrap\ActiveForm::end() ?>
    </div>

</div>