<div class="default-form container">

    <?php $form = \yii\bootstrap\ActiveForm::begin(); ?>

    <?php echo $form->field($model,'password')->passwordInput() ?>
    <?php echo $form->field($model,'repassword')->passwordInput() ?>


    <?php echo \yii\helpers\Html::submitButton('Сменить пароль', ['class' => 'btn btn-primary']) ?>

    <?php \yii\bootstrap\ActiveForm::end() ?>


</div>