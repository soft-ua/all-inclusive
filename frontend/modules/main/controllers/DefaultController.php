<?php

namespace app\modules\main\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public $layout = "inclusivemain";
    public function actionIndex()
    {
        return $this->render('index');
    }
}
