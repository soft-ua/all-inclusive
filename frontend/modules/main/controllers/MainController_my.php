<?php
namespace app\modules\main\controllers;

use common\models\Comment;
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\helpers\BaseFileHelper;
use common\models\Donate;
use common\models\House;
use common\models\HousesType;
use common\models\OurTeam;
use common\models\User;
use common\models\LoginForm;
use frontend\filters\FilterHouse;
use frontend\models\ContactForm;
use frontend\models\Image;
use frontend\models\SignupForm;
use yii\base\DynamicModel;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\Response;
use yii\widgets\ActiveForm;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\Marker;
use Imagine\Image\ImageInterface;

class MainController extends \yii\web\Controller
{
    public $layout = "inclusivemain";

    public function init(){
//        \Yii::$app->view->registerJsFile('/resource/js/yandex.api2.js',['position' => \yii\web\View::POS_HEAD]);
//        \Yii::$app->view->registerJsFile('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU',['position' => \yii\web\View::POS_END]);
//        \Yii::$app->view->registerJsFile('/resource/js/ya.map.front.js',['position' => \yii\web\View::POS_END]);
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
            'test' => [
                'class' => 'frontend\actions\TestAction',
            ],
            'page' => [
                'class' => 'yii\web\ViewAction',
                'layout' => 'inclusivemain',
            ]
        ];
    }

    public function behaviors()
    {
        return [
            [
                'only' => ['view-house'],
                'class' => FilterHouse::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $query = new Query();

        $all_result = $query->from('house')
            ->where('house.type = 1')
            ->andWhere('house.available = true')
            ->orderBy('house.id desc')
            ->limit(18)
            ->all();

        $result_general = $query->from('house')
            ->where('house.type = 1')
            ->andWhere('house.available = true')
            ->orderBy('house.id desc')
            ->limit(6)
            ->all();

        $result_second = $query->from('house')
            ->where('house.type = 1')
            ->andWhere('house.available = true')
            ->orderBy('house.id desc')
            ->limit(12)
            ->offset(6)
            ->all();

        $queryHouseType = new Query();
        $nameHouseType = $queryHouseType->from('houses_type')
            ->andWhere('id = 1')
            ->one();

        return $this->render('index',[
            'result_general' => $result_general,
            'result_second' => $result_second,
            'nameHouseType' => $nameHouseType,
            'all_result' => $all_result,
        ]);
    }

//    public function actionRegistration(){
//
//        $model = new SignupForm();
//
//        if(\Yii::$app->request->isAjax && \Yii::$app->request->isPost){
//            if($model->load(\Yii::$app->request->post())){
//                \Yii::$app->response->format = Response::FORMAT_JSON;
//                return ActiveForm::validate($model);
//            }
//        }
//
//        if($model->load(\Yii::$app->request->post()) && $model->signup()){
//            \Yii::$app->session->setFlash('success','Регистрация прошла успешно!');
//        }
////        if($model->load(\Yii::$app->request->post())){
////            \Yii::$app->session->setFlash('error','Во время отправки произошла ошибка, пожайлуста, попробуйте повторите попытку позже!');
////        }
//
//        return $this->render("registration",['model' => $model]);
//    }

//    public function actionLogin()
//    {
//        $model = new LoginForm;
//
//        if($model->load(\Yii::$app->request->post()) && $model->login()){
//            $this->goBack();
//
//            \Yii::$app->session->setFlash('success','Вы успешно вошли!');
//        }
//
//        return $this->render("login", ["model" => $model]);
//    }

//    public function actionLogout()
//    {
//        \Yii::$app->user->logout();
//        return $this->goHome();
//    }

    public function actionContact(){

//        \Yii::$app->view->registerJsFile('//api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU',['position' => \yii\web\View::POS_HEAD]);
//        \Yii::$app->view->registerJsFile('//api-maps.yandex.ru/2.1/?lang=ru_RU',['position' => \yii\web\View::POS_HEAD]);
//        \Yii::$app->view->registerJsFile('/resource/js/ya.map.front.js',['position' => \yii\web\View::POS_END]);

        $model = new ContactForm();
        if($model->load(\Yii::$app->request->post()) && $model->validate()){
            $body = " <div>Имя отправителя: <b> ".$model->name." </b></div>";
            $body .= " <div>Email отправителя: <b> ".$model->email." </b></div>";
            $body .= " <div>Сообщение: <b> ".$model->body." </b></div>";

            \Yii::$app->common->sendMail($model->subject,$body);

            \Yii::$app->session->setFlash('success','Ваше письмо было отправлено. Мы внимательно ознакомимся с ним!');
        }
        $query = new Query();
        $result_contacts = $query->from('contacts_info')
//            ->where("idcontacts = 1")
            ->all();

        $our_team = new Query();
        $our_team = $our_team->from('our_team')
            ->limit(3)
            ->all();



        $our_team_item = new Query();
        $our_team_item = $our_team_item->from('our_team')
//            ->offset(3)
            ->all();

        return $this->render("contact", ['model' => $model, 'result_contacts' => $result_contacts, 'our_team' => $our_team, 'our_team_item' => $our_team_item]);
    }

    public function actionViewHouse($id){
//        \Yii::$app->view->registerJsFile('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU',['position' => \yii\web\View::POS_END]);
//        \Yii::$app->view->registerJsFile('/resource/js/ya.map.front.js',['position' => \yii\web\View::POS_END]);
//        \Yii::$app->view->registerJsFile('/resource/js/jquery.fancybox.pack.js',['position' => \yii\web\View::POS_END]);





        $query = new Query();
        $modelImage = $query->from('house')
//            ->innerJoin('image', 'house.id = image.entity_id')
//            ->select('image.str_imgs')
            ->where("id = :id",[':id'=>$id])
//            ->andWhere('image.main = false')
            ->andWhere('type = 1')
            ->all();
//        var_dump($id);die;

        $modelImageMain = $query->from('house')
//            ->innerJoin('image', 'house.id = image.entity_id')
            ->where("id = :id",[':id'=>$id])
//            ->andWhere('image.main = true')
            ->andWhere('type = 1')
            ->all();

        $queryAll = new Query();
        $modelAllImage = $queryAll->from('house')
//            ->innerJoin('house', 'house.id = image.entity_id')
//            ->select('image.str_imgs')
            ->where("id = :id",[':id'=>$id])
            ->andWhere('type = 1')
//            ->andWhere('image.available = true')
            ->all();

        $modelAllImageActive = $queryAll->from('house')
//            ->innerJoin('house', 'house.id = image.entity_id')
            ->limit(6)
            ->all();

        $modelAllImageItem = $queryAll->from('house')
//            ->innerJoin('house', 'house.id = image.entity_id')
            ->offset(6)
            ->all();

        $queryHouses = new Query();
        $allHousesActive = $queryHouses->from('house')
//            ->innerJoin('image', 'house.id = image.entity_id')
            ->where('house.type = 1')
//            ->andWhere('image.slider_id = 1')
            ->andWhere('house.available = true')
//            ->andWhere('image.available = true')
//            ->andWhere('image.main = true')
            ->orderBy('house.id desc')
            ->limit(6)
            ->all();

        $allHousesItem = $queryHouses->from('house')
            ->offset(6)
            ->all();




        $model_comment = new Comment();

        $model_comment->scenario = 'front_comment';

        if($model_comment->load(\Yii::$app->request->post()) && $model_comment->validate()){
            $model_comment->entity_id = $id;
            $model_comment->created_at = time();
            $model_comment->save();
        }



        if(!$model_comment->load(\Yii::$app->request->post())){
            \Yii::$app->session->setFlash('error','Во время отправки произошла ошибка, пожалуйста, попробуйте повторите попытку позже!');
        }
        if($model_comment->load(\Yii::$app->request->post()) && $model_comment->validate()){
            \Yii::$app->session->setFlash('success','Ваш комментарий успешно отправлен! Ожидайте подтверждения администратора');
        }




        $model = House::findOne($id);


        $coords = str_replace(['(',')'],'',$model->location);
        $coords = explode(',',$coords);

        $coord = new LatLng(['lat' => $coords[0], 'lng' => $coords[1]]);
        $map = new Map([
            'center' => $coord,
            'zoom' => 12,
        ]);

        $marker = new Marker([
            'position' => $coord,
        ]);

        $map->addOverlay($marker);


        $user = $model->user;

        $current_user = ['email' => '', 'username' => ''];

        if(!\Yii::$app->user->isGuest){

            $current_user['email'] = \Yii::$app->user->identity->email;
            $current_user['username'] = \Yii::$app->user->identity->username;

        }

        return $this->render('view_house',[
            'model' => $model,
            'model_comment' => $model_comment,
            'user' => $user,
            'current_user' => $current_user,
            'map' => $map,
            'modelImage' => $modelImage,
            'modelAllImage' => $modelAllImage,
            'modelAllImageActive' => $modelAllImageActive,
            'modelAllImageItem' => $modelAllImageItem,
            'allHousesActive' => $allHousesActive,
            'allHousesItem' => $allHousesItem,
            'modelImageMain' => $modelImageMain,
        ]);

    }

    public function actionViewProject($id){
//        \Yii::$app->view->registerJsFile('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU',['position' => \yii\web\View::POS_END]);
//        \Yii::$app->view->registerJsFile('/resource/js/ya.map.front.js',['position' => \yii\web\View::POS_END]);
//        \Yii::$app->view->registerJsFile('/resource/js/jquery.fancybox.pack.js',['position' => \yii\web\View::POS_END]);

//        $path_dir = \Yii::getAlias("@frontend/web/images/main/watermark/");
//        $path_mark_bk_lg = $path_dir . "logo_black_300.png";
//        $path_mark_bk = $path_dir . "logo_black.png";
//        $path_mark_bk_md = $path_dir . "logo_black_md.png";
//        $path_mark_bk_sm = $path_dir . "logo_black_sm.png";
//        $path_mark_bl_lg = $path_dir . "logo_blue_300.png";
//        $path_mark_bl = $path_dir . "logo_blue.png";
//        $path_mark_bl_md = $path_dir . "logo_blue_md.png";
//        $path_mark_bl_sm = $path_dir . "logo_blue_sm.png";
//        $path_mark_rd_lg = $path_dir . "logo_red_300.png";
//        $path_mark_rd = $path_dir . "logo_red.png";
//        $path_mark_rd_md = $path_dir . "logo_red_md.png";
//        $path_mark_rd_sm = $path_dir . "logo_red_sm.png";
//        $path = \Yii::getAlias("@frontend/web/uploads/house/".$id);
//        $images_arr = [];
//        $original = \Yii::getAlias("@frontend/web/uploads/house/".$id.'/original');
//
////        BaseFileHelper::createDirectory($original);
//        try {
//            if(is_dir($path)) {
//                $files = \yii\helpers\FileHelper::findFiles($path);
////                $files_original = \yii\helpers\FileHelper::findFiles($original);
//
//                foreach ($files as $file) {
//
//                    if (!strstr(basename($file), "wtmarked_") && !strstr(basename($file), "small_")) {
//                        $images_arr[] = basename($file);
//                        $image = basename($file);
//
//
//                        $size = getimagesize($file);
//                        $width = $size[0];
//                        $height = $size[1];
//
//                        $sizeWMark = getimagesize($path_mark_bk_lg);
//                        $widthWMark = $sizeWMark[0];
//                        $heightWMark = $sizeWMark[1];
//
//                        if($height > 2071 && $width > 1144){
//
////                            \yii\imagine\Image::frame($file,0,'666',0)->save($original.'/'.$image);
//                            $image_mark = \yii\imagine\Image::watermark($file, $path_mark_bk_lg, [($width-$widthWMark), ($height-$heightWMark)])->save($path.'/wtmarked_'.$image);
//
//                        }elseif( $height >= 500 && $height && $width >= 275){
//
//                            $sizeWMark = getimagesize($path_mark_bk);
//                            $widthWMark = $sizeWMark[0];
//                            $heightWMark = $sizeWMark[1];
//
////                            \yii\imagine\Image::frame($file,0,'666',0)->save($original.'/'.$image);
//                            $image_mark = \yii\imagine\Image::watermark($file, $path_mark_bk, [($width-$widthWMark), ($height-$heightWMark)])->save($path.'/wtmarked_'.$image);
//
//                        }elseif( $height >= 300 && $height && $width >= 166){
//
//                            $sizeWMark = getimagesize($path_mark_bk_md);
//                            $widthWMark = $sizeWMark[0];
//                            $heightWMark = $sizeWMark[1];
//
////                            \yii\imagine\Image::frame($file,0,'666',0)->save($original.'/'.$image);
//                            $image_mark = \yii\imagine\Image::watermark($file, $path_mark_bk_md, [($width-$widthWMark), ($height-$heightWMark)])->save($path.'/wtmarked_'.$image);
//
//                        }else{
//
//                            $sizeWMark = getimagesize($path_mark_bk_sm);
//                            $widthWMark = $sizeWMark[0];
//                            $heightWMark = $sizeWMark[1];
//
////                            \yii\imagine\Image::frame($file,0,'666',0)->save($original.'/'.$image);
//                            $image_mark = \yii\imagine\Image::watermark($file, $path_mark_bk_sm, [($width-$widthWMark), ($height-$heightWMark)])->save($path.'/wtmarked_'.$image);
//
//                        }
//                    }
//                }
//            }
//        }
//        catch(\yii\base\Exception $e){}

        $query = new Query();
        $modelImage = $query
            ->from('house')
//            ->innerJoin('image', 'house.id = image.entity_id')
            ->where("id = :id",[':id'=>$id])
//            ->andWhere('image.main = false')
            ->andWhere('house.type = 2')
            ->all();


        $modelImageMain = $query
//            ->from('house')
//            ->innerJoin('image', 'house.id = image.entity_id')
            ->where("id = :id",[':id'=>$id])
//            ->andWhere('image.main = true')
            ->andWhere('house.type = 2')
            ->all();

        $queryAll = new Query();
        $modelAllImage = $queryAll
            ->from('house')
//            ->innerJoin('house', 'house.id = image.entity_id')
            ->where("id = :id",[':id'=>$id])
//            ->andWhere('image.main = 1')
            ->andWhere('house.type = 2')
//            ->andWhere('image.available = true')
            ->all();

        $modelAllImageActive = $queryAll
//            ->from('house')
//            ->innerJoin('house', 'house.id = image.entity_id')
            ->limit(6)
            ->all();

        $modelAllImageItem = $queryAll->from('house')
//            ->innerJoin('house', 'house.id = image.entity_id')
            ->offset(6)
            ->all();

        $queryHouses = new Query();
        $allHousesActive = $queryHouses->from('house')
//            ->innerJoin('image', 'house.id = image.entity_id')
            ->where('house.type = 2')
//            ->andWhere('image.slider_id = 1')
            ->andWhere('house.available = true')
//            ->andWhere('image.available = true')
//            ->andWhere('image.main = true')
            ->orderBy('house.id desc')
            ->limit(6)
            ->all();

        $allHousesItem = $queryHouses->from('house')
            ->offset(6)
            ->all();




        $model_comment = new Comment();

        $model_comment->scenario = 'front_comment';

        if($model_comment->load(\Yii::$app->request->post()) && $model_comment->validate()){
            $model_comment->entity_id = $id;
            $model_comment->created_at = time();
            $model_comment->save();
        }



        if(!$model_comment->load(\Yii::$app->request->post())){
            \Yii::$app->session->setFlash('error','Во время отправки произошла ошибка, пожалуйста, попробуйте повторите попытку позже!');
        }
        if($model_comment->load(\Yii::$app->request->post()) && $model_comment->validate()){
            \Yii::$app->session->setFlash('success','Ваш комментарий успешно отправлен! Ожидайте подтверждения администратора');
        }




        $model = House::findOne($id);


        $coords = str_replace(['(',')'],'',$model->location);
        $coords = explode(',',$coords);

        $coord = new LatLng(['lat' => $coords[0], 'lng' => $coords[1]]);
        $map = new Map([
            'center' => $coord,
            'zoom' => 12,
        ]);

        $marker = new Marker([
            'position' => $coord,
        ]);

        $map->addOverlay($marker);


        $user = $model->user;

        $current_user = ['email' => '', 'username' => ''];

        if(!\Yii::$app->user->isGuest){

            $current_user['email'] = \Yii::$app->user->identity->email;
            $current_user['username'] = \Yii::$app->user->identity->username;

        }

        return $this->render('view_project',[
            'model' => $model,
            'model_comment' => $model_comment,
            'user' => $user,
            'current_user' => $current_user,
            'map' => $map,
            'modelImage' => $modelImage,
            'modelAllImage' => $modelAllImage,
            'modelAllImageActive' => $modelAllImageActive,
            'modelAllImageItem' => $modelAllImageItem,
            'allHousesActive' => $allHousesActive,
            'allHousesItem' => $allHousesItem,
            'modelImageMain' => $modelImageMain,
        ]);

    }

    public function actionListHouses(){

        $query = new Query();
        $query->from('house')
//            ->innerJoin('image', 'house.id = image.entity_id')
            ->where('house.type = 1')
//            ->andWhere('image.slider_id = 1')
            ->andWhere('house.available = true');
//            ->andWhere('image.main = true')
//            ->andWhere('image.available = true');

        $countQuery = clone $query;
        $countQuerySmall = clone $query;

        $query->orderBy('house.id desc')
            ->all();



        $pages = new Pagination((['totalCount' => $countQuery->count()]));
        $pages->setPageSize(9);

        $small_pages = new Pagination((['totalCount' => $countQuerySmall->count()]));
        $small_pages->setPageSize(1);

        $model = $query->offset($pages->offset)->limit($pages->limit)->all();

        $small_model = $query->offset($small_pages->offset)->limit($small_pages->limit)->all();

        return $this->render("list_houses", [
            'model'=> $model,
            'small_model'=> $small_model,
            'modelListHouses' => $query,
            'pages' => $pages,
            'small_pages' => $small_pages,
        ]);
    }

    public function actionListProjects(){

        $query = new Query();
        $query->from('house')
//            ->innerJoin('image', 'house.id = image.entity_id')
            ->where('house.type = 2')
            ->andWhere('house.available = true');
//            ->andWhere('image.slider_id = 1')
//            ->andWhere('image.main = true')
//            ->andWhere('image.available = true');

        $countQuery = clone $query;
        $countQuerySmall = clone $query;

        $query->orderBy('house.id desc')
            ->all();
        $pages = new Pagination((['totalCount' => $countQuery->count()]));
        $pages->setPageSize(9);

        $small_pages = new Pagination((['totalCount' => $countQuerySmall->count()]));
        $small_pages->setPageSize(1);

        $model = $query->offset($pages->offset)->limit($pages->limit)->all();

        $small_model = $query->offset($small_pages->offset)->limit($small_pages->limit)->all();

        $queryHouseType = new Query();
        $nameHouseType = $queryHouseType->from('houses_type')
            ->andWhere('id = 2')
            ->one();

        return $this->render("list_projects", [
            'model'=> $model,
            'small_model'=> $small_model,
            'modelListHouses' => $query,
            'pages' => $pages,
            'small_pages' => $small_pages,
            'nameHouseType' => $nameHouseType,
        ]);
    }

    public function actionTestimonials(){
//        \Yii::$app->view->registerJsFile('/resource/js/jquery.fancybox.pack.js',['position' => \yii\web\View::POS_END]);

        $query = new Query();

        $modelTestimonials = $query->from('comment')
            ->where('available_comment = true')
            ->all();

        $countQuery = clone $query;
        $pages = new Pagination((['totalCount' => $countQuery->count()]));
        $pages->setPageSize(10);

        $model = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render("testimonials", ['model'=> $model, 'modelTestimonials' => $modelTestimonials, 'pages' => $pages]);
    }

    public function actionDariDobro(){
//        \Yii::$app->view->registerJsFile('/resource/js/jquery-1.9.1.min.js',['position' => \yii\web\View::POS_BEGIN]);
//        \Yii::$app->view->registerJsFile('/resource/js/jssor.js',['position' => \yii\web\View::POS_HEAD]);
//        \Yii::$app->view->registerJsFile('/resource/js/jssor.slider.min.js',['position' => \yii\web\View::POS_BEGIN]);
//        \Yii::$app->view->registerJsFile('/resource/js/jssor.custom.js',['position' => \yii\web\View::POS_BEGIN]);

//        $modelDonate = Donate::find()->all();
//        var_dump($modelDonate);die;

        $query = new Query();
        $query->from('donate');

//        $modelDonate = $query->all();

//        $query->from('donate')
//            ->innerJoin('donates_img','donate.id = donates_img.donate_text')
//            ->where('donate.available_d = true');
//            ->andWhere('donates_img.available_donates = true');


        $allImgs = $query
//            ->andWhere('donates_img.main_donates = false')
            ->all();

//        $query->where('donates_img.main_donates = true');
        $countQuery = clone $query;

        $query->orderBy('donate.id DESC')
            ->where('donate.available_d = true')
//            ->andWhere('donates_img.available_donates = true')
            ->all();
        $pages = new Pagination((['totalCount' => $countQuery->count()]));
        $pages->setPageSize(1);

        $model = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render("dari_dobro", [
            'modelDonate'=> $model,
//            'modelDonate'=> $modelDonate,
//            'mainImg' => $mainImg,
            'allImgs' => $allImgs,
            'pages' => $pages
        ]);

    }

    public function actionAbout(){
        $query = new Query();

        $model = $query->from('about_us')
            ->all();

        return $this->render("about", ['model'=>$model]);
    }

    public function actionName(){
        // Array with names
        $a[] = "Anna";
        $a[] = "Brittany";
        $a[] = "Cinderella";
        $a[] = "Diana";
        $a[] = "Eva";
        $a[] = "Fiona";
        $a[] = "Gunda";
        $a[] = "Hege";
        $a[] = "Inga";
        $a[] = "Johanna";
        $a[] = "Kitty";
        $a[] = "Linda";
        $a[] = "Nina";
        $a[] = "Ophelia";
        $a[] = "Petunia";
        $a[] = "Amanda";
        $a[] = "Raquel";
        $a[] = "Cindy";
        $a[] = "Doris";
        $a[] = "Eve";
        $a[] = "Evita";
        $a[] = "Sunniva";
        $a[] = "Tove";
        $a[] = "Unni";
        $a[] = "Violet";
        $a[] = "Liza";
        $a[] = "Elizabeth";
        $a[] = "Ellen";
        $a[] = "Wenche";
        $a[] = "Vicky";

// get the q parameter from URL
        $q = $_REQUEST["q"];

        $hint = "";

// lookup all hints from array if $q is different from ""
        if ($q !== "") {
            $q = strtolower($q);
            $len=strlen($q);
            foreach($a as $name) {
                if (stristr($q, substr($name, 0, $len))) {
                    if ($hint === "") {
                        $hint = $name;
                    } else {
                        $hint .= ", $name";
                    }
                }
            }
        }

// Output "no suggestion" if no hint was found or output correct values
        echo $hint === "" ? "no suggestion" : $hint;

    }

    public function actionMainwatermark(){

        $file = $_GET["n"];
        $id = $_GET["id"];
        $wsc = $_GET["wsc"];
        $hsc = $_GET["hsc"];

        $path_dir = \Yii::getAlias("@frontend/web/images/main/watermark/");
        $allink_01 = $path_dir . "allink_01.png";
        $path = \Yii::getAlias("@frontend/web/uploads/house/".$id);

        try {
            if(is_dir($path)) {

                $size = getimagesize($path.'/'.$file);
                $width = $size[0];
                $height = $size[1];

                $ratio =  $width/$height;

                if (is_file($path.'/'.$file) && !is_file($path.'/wtmarked_'.$wsc.'x'.$hsc.'_'. $file)) {
                    $image = $file;
                    $pos = mb_strripos($file, '.');
                    $new_str = mb_substr($file, $pos);

                    $sizeWMark = getimagesize($allink_01);
                    $widthWMark = $sizeWMark[0];
                    $heightWMark = $sizeWMark[1];

//                    if($width <= 15000 && $height <= 15000) {

                        if ($width > $wsc) {
                            if ($wsc > 480) {
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($wsc, $wsc / $ratio))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc, $wsc / $ratio, $wsc, $wsc / $ratio);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$wsc/$ratio.'_'.$file.'" />';
                            }elseif($wsc <= 480){
                                $wsc_size = 480;
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($wsc_size, $wsc_size / $ratio))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc_size, $wsc_size / $ratio, $wsc_size, $wsc_size / $ratio);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$wsc/$ratio.'_'.$file.'" />';
                            }
                        }elseif($width <= $wsc){
                            if ($wsc > 480) {
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($width, $width / $ratio))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $width / $ratio, $width, $width / $ratio);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$wsc/$ratio.'_'.$file.'" />';
                            }elseif($wsc <= 480){
                                if($width >= 480){
                                    $wsc_size = 480;
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($wsc_size, $wsc_size / $ratio))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc_size, $wsc_size / $ratio, $wsc_size, $wsc_size / $ratio);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$wsc/$ratio.'_'.$file.'" />';
                                }elseif($width < 480){
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($width, $width / $ratio))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $width / $ratio, $width, $width / $ratio);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$wsc/$ratio.'_'.$file.'" />';
                                }
                            }
                        }

//                    }

                }elseif(is_file($path.'/'.$file) && is_file($path.'/wtmarked_'.$wsc.'x'.$hsc.'_'. $file)){
                    echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$wsc/$ratio.'_'.$file.'" />';
                }else{
                    echo '<img class="img img-responsive text-center" style="margin: auto" src="/images/main/nophoto.jpg"  alt="no photo" />';
                }

            }
        }
        catch(\yii\base\Exception $e){}

    }

    public function actionManywatermark(){

        $file = $_GET["n"];
        $id = $_GET["id"];
        $wsc = $_GET["wsc"];
        $hsc = $_GET["hsc"];

        $path_dir = \Yii::getAlias("@frontend/web/images/main/watermark/");
        $allink_01 = $path_dir . "allink_01.png";
        $path = \Yii::getAlias("@frontend/web/uploads/house/".$id);

        try {
            if(is_dir($path) && file_exists($path.'/'.$file)) {

                $size = getimagesize($path.'/'.$file);
                $width = $size[0];
                $height = $size[1];

                $ratio =  $width/$height;

                if (is_file($path.'/'.$file) && !is_file($path.'/wtmarked_'.$wsc.'x'.$hsc.'_'. $file)) {
                    $image = $file;
                    $pos = mb_strripos($file, '.');
                    $new_str = mb_substr($file, $pos);

                    $sizeWMark = getimagesize($allink_01);
                    $widthWMark = $sizeWMark[0];
                    $heightWMark = $sizeWMark[1];

                    if($ratio > 1) {

                        if ($width > $wsc) {
                            if ($wsc > 480) {
                                if ($height > $hsc) {

                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($hsc * $ratio, $hsc))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc * $ratio, $hsc, $hsc * $ratio, $hsc);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/house/' . $id . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $file;
                                }else{
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($wsc, $wsc / $ratio))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc, $wsc / $ratio, $wsc, $wsc / $ratio);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/house/' . $id . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $file;
                                }
                            }elseif($wsc <= 480){
                                $wsc_size = 480;
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($wsc_size, $wsc_size / $ratio))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc_size, $wsc_size / $ratio, $wsc_size, $wsc_size / $ratio);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                            }
                        }elseif($width <= $wsc){
                            if ($wsc > 480) {
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($width, $width / $ratio))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $width / $ratio, $width, $width / $ratio);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                            }elseif($wsc <= 480){
                                if($width >= 480){
                                    $wsc_size = 480;
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($wsc_size, $wsc_size / $ratio))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc_size, $wsc_size / $ratio, $wsc_size, $wsc_size / $ratio);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                                }elseif($width < 480){
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($width, $width / $ratio))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $width / $ratio, $width, $width / $ratio);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                                }
                            }
                        }

                    }else{

                        if ($height > $hsc) {
                            if ($hsc > 640) {

                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($hsc*$ratio, $hsc))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc*$ratio, $hsc, $hsc*$ratio, $hsc);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                            }elseif($hsc <= 640){
                                $hsc_size = 640;
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($hsc_size*$ratio, $hsc_size))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc_size*$ratio, $hsc_size, $hsc_size*$ratio, $hsc_size);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                            }
                        }elseif($height <= $hsc){
                            if ($hsc > 640) {
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($height*$ratio, $height))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $height*$ratio, $height, $height*$ratio, $height);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                            }elseif($hsc <= 640){
                                if($height >= 640){
                                    $hsc_size = 640;
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($hsc_size * $ratio, $hsc_size))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc_size * $ratio, $hsc_size, $hsc_size * $ratio, $hsc_size);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                                }elseif($height < 640){
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($height * $ratio, $height))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $height * $ratio, $height, $height * $ratio, $height);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                                }
                            }
                        }

                    }

                }elseif(is_file($path.'/'.$file) && is_file($path.'/wtmarked_'.$wsc.'x'.$hsc.'_'. $file)){
                    echo '/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                }else{
                    echo '/images/main/nophoto.jpg';
                }
            }
        }
        catch(\yii\base\Exception $e){}
    }

    public function actionManywatermarkdonate2(){

        $file = $_REQUEST["n"];
        $id = $_REQUEST["id"];
//
        $path_dir = \Yii::getAlias("@frontend/web/images/main/watermark/");
        $allink_01 = $path_dir . "allink_01.png";
        $path = \Yii::getAlias("@frontend/web/uploads/donate/".$id);
        $images_arr = [];

        try {
            if(is_dir($path)) {

                if (is_file($path.'/'.$file) && !is_file($path.'/wtmarked_'.($file))) {
                    $images_arr[] = ($file);
                    $image = ''.($file);
                    $pos = mb_strripos($file, '.');
                    $new_str = mb_substr($file, $pos);

                    $size = getimagesize($path.'/'.$file);
                    $width = $size[0];
                    $height = $size[1];

                    $sizeWMark = getimagesize($allink_01);
                    $widthWMark = $sizeWMark[0];
                    $heightWMark = $sizeWMark[1];



                    if($width <= 5000 && $height <= 5000){

                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);

                        $new_name = $path. '/' .time()."_allink.png";


                        //открываем исходное изображение
                        $src = imagecreatefrompng($allink_01);

                        //создаем дескриптор для измененного изображения
                        //                        $dst = imagecreatetruecolor($width, $height);
                        if($new_str == '.jpg' || $new_str == '.jpeg'){
                            $dst = imagecreatefromjpeg($path.'/'.$image);
                        }elseif($new_str == '.png'){
                            $dst = imagecreatefrompng($path.'/'.$image);
                        }elseif($new_str == '.gif'){
                            $dst = imagecreatefromgif($path.'/'.$image);
                        }

                        //устанавливаем прозрачность
                        \frontend\components\Common::setTransparency($dst, $src);

                        //изменяем размер
                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $height);

                        //сохраняем уменьшенное изображение в файл
                        ImagePNG($dst, $new_name);

                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

                        //закрываем дескрипторы исходного и уменьшенного изображений
                        ImageDestroy($src);
                        ImageDestroy($dst);

                        unlink($new_name);

                    }elseif($width > 5000 && $height <= 5000){

                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);

                        $new_name = $path. '/' .time()."_allink.png";


                        $src = imagecreatefrompng($allink_01);

                        if($new_str == '.jpg' || $new_str == '.jpeg'){
                            $dst = imagecreatefromjpeg($path.'/'.$image);
                        }elseif($new_str == '.png'){
                            $dst = imagecreatefrompng($path.'/'.$image);
                        }elseif($new_str == '.gif'){
                            $dst = imagecreatefromgif($path.'/'.$image);
                        }

                        \frontend\components\Common::setTransparency($dst, $src);

                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $height);

                        ImagePNG($dst, $new_name);

                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

                        ImageDestroy($src);
                        ImageDestroy($dst);

                        unlink($new_name);

                    }elseif($width <= 5000 && $height > 5000){

                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);

                        $new_name = $path. '/' .time()."_allink.png";


                        $src = imagecreatefrompng($allink_01);

                        if($new_str == '.jpg' || $new_str == '.jpeg'){
                            $dst = imagecreatefromjpeg($path.'/'.$image);
                        }elseif($new_str == '.png'){
                            $dst = imagecreatefrompng($path.'/'.$image);
                        }elseif($new_str == '.gif'){
                            $dst = imagecreatefromgif($path.'/'.$image);
                        }

                        \frontend\components\Common::setTransparency($dst, $src);

                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $heightWMark);

                        ImagePNG($dst, $new_name);

                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

                        ImageDestroy($src);
                        ImageDestroy($dst);

                        unlink($new_name);

                    }else{

                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);

                        $new_name = $path. '/' .time()."_allink.png";


                        $src = imagecreatefrompng($allink_01);

                        if($new_str == '.jpg' || $new_str == '.jpeg'){
                            $dst = imagecreatefromjpeg($path.'/'.$image);
                        }elseif($new_str == '.png'){
                            $dst = imagecreatefrompng($path.'/'.$image);
                        }elseif($new_str == '.gif'){
                            $dst = imagecreatefromgif($path.'/'.$image);
                        }

                        \frontend\components\Common::setTransparency($dst, $src);

                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $heightWMark);

                        ImagePNG($dst, $new_name);

                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

                        ImageDestroy($src);
                        ImageDestroy($dst);

                        unlink($new_name);

                    }
                }
            }
        }
        catch(\yii\base\Exception $e){}

        return true;
    }

    public function actionManywatermarkdonate(){

        $file = $_GET["n"];
        $id = $_GET["id"];
        $wsc = $_GET["wsc"];
        $hsc = $_GET["hsc"];

        $path_dir = \Yii::getAlias("@frontend/web/images/main/watermark/");
        $allink_01 = $path_dir . "allink_01.png";
        $path = \Yii::getAlias("@frontend/web/uploads/donate/".$id);

        try {
            if(is_dir($path) && file_exists($path.'/'.$file)) {

                $size = getimagesize($path.'/'.$file);
                $width = $size[0];
                $height = $size[1];

                $ratio =  $width/$height;

                if (is_file($path.'/'.$file) && !is_file($path.'/wtmarked_'.$wsc.'x'.$hsc.'_'. $file)) {
                    $image = $file;
                    $pos = mb_strripos($file, '.');
                    $new_str = mb_substr($file, $pos);

                    $sizeWMark = getimagesize($allink_01);
                    $widthWMark = $sizeWMark[0];
                    $heightWMark = $sizeWMark[1];

                    if($ratio > 1) {

                        if ($width > $wsc) {
                            if ($wsc > 480) {
                                if ($height > $hsc) {
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($hsc * $ratio, $hsc))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc * $ratio, $hsc, $hsc * $ratio, $hsc);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/donate/' . $id . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $file;
                                }else{
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($wsc, $wsc / $ratio))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc, $wsc / $ratio, $wsc, $wsc / $ratio);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/donate/' . $id . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $file;
                                }
                            }elseif($wsc <= 480){
                                $wsc_size = 480;
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($wsc_size, $wsc_size / $ratio))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc_size, $wsc_size / $ratio, $wsc_size, $wsc_size / $ratio);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '/uploads/donate/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                            }
                        }elseif($width <= $wsc){
                            if ($wsc > 480) {
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($width, $width / $ratio))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $width / $ratio, $width, $width / $ratio);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '/uploads/donate/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                            }elseif($wsc <= 480){
                                if($width >= 480){
                                    $wsc_size = 480;
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($wsc_size, $wsc_size / $ratio))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc_size, $wsc_size / $ratio, $wsc_size, $wsc_size / $ratio);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/donate/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                                }elseif($width < 480){
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($width, $width / $ratio))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $width / $ratio, $width, $width / $ratio);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/donate/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                                }
                            }
                        }

                    }else{

                        if ($height > $hsc) {
                            if ($hsc > 640) {
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($hsc*$ratio, $hsc))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc*$ratio, $hsc, $hsc*$ratio, $hsc);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '/uploads/donate/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                            }elseif($hsc <= 640){
                                $hsc_size = 640;
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($hsc_size*$ratio, $hsc_size))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc_size*$ratio, $hsc_size, $hsc_size*$ratio, $hsc_size);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '/uploads/donate/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                            }
                        }elseif($height <= $hsc){
                            if ($hsc > 640) {
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($height*$ratio, $height))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $height*$ratio, $height, $height*$ratio, $height);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '/uploads/donate/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                            }elseif($hsc <= 640){
                                if($height >= 640){
                                    $hsc_size = 640;
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($hsc_size * $ratio, $hsc_size))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc_size * $ratio, $hsc_size, $hsc_size * $ratio, $hsc_size);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/donate/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                                }elseif($height < 640){
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($height * $ratio, $height))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $height * $ratio, $height, $height * $ratio, $height);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '/uploads/donate/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                                }
                            }
                        }

                    }

                }elseif(is_file($path.'/'.$file) && is_file($path.'/wtmarked_'.$wsc.'x'.$hsc.'_'. $file)){
                    echo '/uploads/donate/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file;
                }else{
                    echo '/images/main/nophoto.jpg';
                }
            }
        }
        catch(\yii\base\Exception $e){}
    }

    public function actionManywatermarktestim2(){

        $file = $_REQUEST["n"];
        $id = $_REQUEST["id"];
//
        $path_dir = \Yii::getAlias("@frontend/web/images/main/watermark/");
        $allink_01 = $path_dir . "allink_01.png";
        $path = \Yii::getAlias("@frontend/web/uploads/comment/".$id);
        $images_arr = [];

        try {
            if(is_dir($path)) {

                if (is_file($path.'/'.$file) && !is_file($path.'/wtmarked_'.($file))) {
                    $images_arr[] = ($file);
                    $image = ''.($file);
                    $pos = mb_strripos($file, '.');
                    $new_str = mb_substr($file, $pos);

                    $size = getimagesize($path.'/'.$file);
                    $width = $size[0];
                    $height = $size[1];

                    $sizeWMark = getimagesize($allink_01);
                    $widthWMark = $sizeWMark[0];
                    $heightWMark = $sizeWMark[1];



                    if($width <= 5000 && $height <= 5000){

                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);

                        $new_name = $path. '/' .time()."_allink.png";


                        //открываем исходное изображение
                        $src = imagecreatefrompng($allink_01);

                        //создаем дескриптор для измененного изображения
                        //                        $dst = imagecreatetruecolor($width, $height);
                        if($new_str == '.jpg' || $new_str == '.jpeg'){
                            $dst = imagecreatefromjpeg($path.'/'.$image);
                        }elseif($new_str == '.png'){
                            $dst = imagecreatefrompng($path.'/'.$image);
                        }elseif($new_str == '.gif'){
                            $dst = imagecreatefromgif($path.'/'.$image);
                        }

                        //устанавливаем прозрачность
                        \frontend\components\Common::setTransparency($dst, $src);

                        //изменяем размер
                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $height);

                        //сохраняем уменьшенное изображение в файл
                        ImagePNG($dst, $new_name);

                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

                        //закрываем дескрипторы исходного и уменьшенного изображений
                        ImageDestroy($src);
                        ImageDestroy($dst);

                        unlink($new_name);

                    }elseif($width > 5000 && $height <= 5000){

                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);

                        $new_name = $path. '/' .time()."_allink.png";


                        $src = imagecreatefrompng($allink_01);

                        if($new_str == '.jpg' || $new_str == '.jpeg'){
                            $dst = imagecreatefromjpeg($path.'/'.$image);
                        }elseif($new_str == '.png'){
                            $dst = imagecreatefrompng($path.'/'.$image);
                        }elseif($new_str == '.gif'){
                            $dst = imagecreatefromgif($path.'/'.$image);
                        }

                        \frontend\components\Common::setTransparency($dst, $src);

                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $height);

                        ImagePNG($dst, $new_name);

                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

                        ImageDestroy($src);
                        ImageDestroy($dst);

                        unlink($new_name);

                    }elseif($width <= 5000 && $height > 5000){

                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);

                        $new_name = $path. '/' .time()."_allink.png";


                        $src = imagecreatefrompng($allink_01);

                        if($new_str == '.jpg' || $new_str == '.jpeg'){
                            $dst = imagecreatefromjpeg($path.'/'.$image);
                        }elseif($new_str == '.png'){
                            $dst = imagecreatefrompng($path.'/'.$image);
                        }elseif($new_str == '.gif'){
                            $dst = imagecreatefromgif($path.'/'.$image);
                        }

                        \frontend\components\Common::setTransparency($dst, $src);

                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $heightWMark);

                        ImagePNG($dst, $new_name);

                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

                        ImageDestroy($src);
                        ImageDestroy($dst);

                        unlink($new_name);

                    }else{

                        \yii\imagine\Image::frame($path.'/'.$image,0,'666',0)->save($path.'/wtmarked_'.$image);

                        $new_name = $path. '/' .time()."_allink.png";


                        $src = imagecreatefrompng($allink_01);

                        if($new_str == '.jpg' || $new_str == '.jpeg'){
                            $dst = imagecreatefromjpeg($path.'/'.$image);
                        }elseif($new_str == '.png'){
                            $dst = imagecreatefrompng($path.'/'.$image);
                        }elseif($new_str == '.gif'){
                            $dst = imagecreatefromgif($path.'/'.$image);
                        }

                        \frontend\components\Common::setTransparency($dst, $src);

                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $height, $widthWMark, $heightWMark);

                        ImagePNG($dst, $new_name);

                        $image_mark = \yii\imagine\Image::watermark($path.'/wtmarked_'.$image, $new_name, [0,0])->save($path.'/wtmarked_'.$image);

                        ImageDestroy($src);
                        ImageDestroy($dst);

                        unlink($new_name);

                    }
                }
            }
        }
        catch(\yii\base\Exception $e){}

        return true;
    }

    public function actionManywatermarktestim(){

        $file = $_GET["n"];
        $id = $_GET["id"];
        $wsc = $_GET["wsc"];
        $hsc = $_GET["hsc"];

        $path_dir = \Yii::getAlias("@frontend/web/images/main/watermark/");
        $allink_01 = $path_dir . "allink_01.png";
        $path = \Yii::getAlias("@frontend/web/uploads/comment/".$id);

        try {
            if(is_dir($path)) {

                $size = getimagesize($path.'/'.$file);
                $width = $size[0];
                $height = $size[1];

                $ratio =  $width/$height;

                if (is_file($path.'/'.$file) && !is_file($path.'/wtmarked_'.$wsc.'x'.$hsc.'_'. $file)) {
                    $image = $file;
                    $pos = mb_strripos($file, '.');
                    $new_str = mb_substr($file, $pos);

                    $sizeWMark = getimagesize($allink_01);
                    $widthWMark = $sizeWMark[0];
                    $heightWMark = $sizeWMark[1];

                    if($ratio > 1) {

                        if ($width > $wsc) {
                            if ($wsc > 480) {
                                if ($height > $hsc) {
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($hsc*$ratio, $hsc))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc*$ratio, $hsc, $hsc*$ratio, $hsc);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/comment/' . $id . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $file . '"  alt="wtmarked_' . $wsc . 'x' . $hsc . '_' . $file . '" />';
                                }elseif($height <= $hsc){
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($wsc, $wsc/$ratio))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc, $wsc/$ratio, $wsc, $wsc/$ratio);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/comment/' . $id . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $file . '"  alt="wtmarked_' . $wsc . 'x' . $hsc . '_' . $file . '" />';
                                }
                            }elseif($wsc <= 480){
                                $wsc_size = 480;
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($wsc_size, $wsc_size / $ratio))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc_size, $wsc_size / $ratio, $wsc_size, $wsc_size / $ratio);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/house/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'" />';
                            }
                        }elseif($width <= $wsc){
                            if ($wsc > 480) {
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($width, $width / $ratio))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $width / $ratio, $width, $width / $ratio);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/comment/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'" />';
                            }elseif($wsc <= 480){
                                if($width >= 480){
                                    $wsc_size = 480;
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($wsc_size, $wsc_size / $ratio))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $wsc_size, $wsc_size / $ratio, $wsc_size, $wsc_size / $ratio);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/comment/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'" />';
                                }elseif($width < 480){
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($width, $width / $ratio))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $width, $width / $ratio, $width, $width / $ratio);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/comment/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'" />';
                                }
                            }
                        }

                    }else{

                        if ($height > $hsc) {
                            if ($hsc > 640) {
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($hsc*$ratio, $hsc))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc*$ratio, $hsc, $hsc*$ratio, $hsc);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/comment/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'" />';
                            }elseif($hsc <= 640){
                                $hsc_size = 640;
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($hsc_size*$ratio, $hsc_size))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc_size*$ratio, $hsc_size, $hsc_size*$ratio, $hsc_size);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/comment/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'" />';
                            }
                        }elseif($height <= $hsc){
                            if ($hsc > 640) {
                                \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                    ->resize(new Box($height*$ratio, $height))
                                    ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                $src = imagecreatefrompng($allink_01);

                                if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                    $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.png') {
                                    $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                } elseif ($new_str == '.gif') {
                                    $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                }

                                ImageCopyResampled($dst, $src, 0, 0, 0, 0, $height*$ratio, $height, $height*$ratio, $height);

                                ImagePNG($dst, $new_name);

                                ImageDestroy($src);
                                ImageDestroy($dst);

                                echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/comment/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'" />';
                            }elseif($hsc <= 640){
                                if($height >= 640){
                                    $hsc_size = 640;
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($hsc_size*$ratio, $hsc_size))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $hsc_size*$ratio, $hsc_size, $hsc_size*$ratio, $hsc_size);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/comment/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'" />';
                                }elseif($height < 640){
                                    \yii\imagine\Image::frame($path . '/' . $image, 0, '666', 0)
                                        ->resize(new Box($height*$ratio, $height))
                                        ->save($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);

                                    $new_name = $path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image;

                                    $src = imagecreatefrompng($allink_01);

                                    if ($new_str == '.jpg' || $new_str == '.jpeg') {
                                        $dst = imagecreatefromjpeg($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.png') {
                                        $dst = imagecreatefrompng($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    } elseif ($new_str == '.gif') {
                                        $dst = imagecreatefromgif($path . '/wtmarked_' . $wsc . 'x' . $hsc . '_' . $image);
                                    }

                                    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $height*$ratio, $height, $height*$ratio, $height);

                                    ImagePNG($dst, $new_name);

                                    ImageDestroy($src);
                                    ImageDestroy($dst);

                                    echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/comment/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'" />';
                                }
                            }
                        }

                    }

                }elseif(is_file($path.'/'.$file) && is_file($path.'/wtmarked_'.$wsc.'x'.$hsc.'_'. $file)){
                    echo '<img class="img img-responsive text-center" style="margin: auto;" src="/uploads/comment/'.$id.'/wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'"  alt="wtmarked_'.$wsc.'x'.$hsc.'_'.$file.'" />';
                }else{
                    echo '<img class="img img-responsive text-center" style="margin: auto" src="/images/main/nophoto.jpg"  alt="no photo" />';
                }

            }
        }
        catch(\yii\base\Exception $e){}

    }

}
