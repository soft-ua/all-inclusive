<?php $this->title = "Форма регистрации";?>
<section class="registration">
    <div class="container">
        <h1 class="text-center"><?php echo $this->title;?></h1>
        <div class="col-lg-6 col-lg-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 ">

            <?php
            $form = \yii\bootstrap\ActiveForm::begin([
                'enableClientValidation' => true,
                'enableAjaxValidation' => true,
            ]);
            ?>

            <?php
            echo $form->field($model, 'username');
            ?>
            <?php
            echo $form->field($model, 'email');
            ?>
            <?php
            echo $form->field($model, 'password')->passwordInput();
            ?>
            <?php
            echo $form->field($model, 'repassword')->passwordInput();
            ?>

            <?php
            echo \yii\helpers\Html::submitButton('Регистрация',['class' => 'btn btn-success text-right']);
            ?>

            <?php
            \yii\bootstrap\ActiveForm::end();
            ?>

        </div>
    </div><!--container-->
</section><!--registration-->