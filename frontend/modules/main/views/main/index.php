<?php $this->title = "Презентация домов";?>
<section class="sky inner">
    <div class="wrap_menu">
        <img class="img img-responsive margin_auto padding_top logo_img2" src="/images/main/all_inclusive.png">
        <?php

        echo \frontend\widgets\Menu::widget();

        ?>
    </div><!-- wrap_menu -->
    <div class="container">
        <p class="text_upper text_bold text-center font_18 text_title"><?php //echo($nameHouseType['title'])?>Готовые дома к продаже</p>
        <div class="row">
            <div class="col-sm-12">
                <div class="row margin_none more_767">

                    <div id="carousel-logo" class="carousel slide list" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">




                        <?php if($all_result != null){?>

                            <?php if(count($all_result) <= 6){?>

                                <div class="item active">
                                        <div class="row">
                                            <?php for($i=0; $i<count($all_result); $i++){?>

                                                <div class="col-sm-4">
                                                    <a href="<?php echo \frontend\components\Common::getUrlHouse($all_result[$i]) ?>" class="jello_block">

                                                        <?php if($all_result[$i]['img_house'] == '' || $all_result[$i]['img_house'] == null || is_file('/uploads/house/'.$all_result[$i]['id'].'/'.$all_result[$i]['img_house'])){?>
                                                            <img class="img img-responsive img-circle img_house" src="/images/main/sqr_nophoto.jpg" alt="nophoto" style="margin: auto;"/>
                                                        <?php }else{?>
                                                            <img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $all_result[$i]['id']?>/small_<?php echo $all_result[$i]['img_house']?>" alt="<?php echo $all_result[$i]['img_house']?>">
                                                        <?php }?>
                                                        <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                        <p class="price text-right"><?php echo ($all_result[$i]['price'] != null && $all_result[$i]['price'] != 0 && $all_result[$i]['price'] != '0' && $all_result[$i]['price'] != '') ? $all_result[$i]['price'].' <span>PУБ</span>' : ''?></p>
                                                    </a>
                                                </div>

                                            <?php }?>

                                        </div>
                                </div>

                            <?php }?>


                            <?php if(count($all_result)>6){?>

                                <div class="item active">
                                    <div class="row">
                                        <?php for($i=0; $i<6; $i++){?>

                                            <div class="col-sm-4">
                                                <a href="<?php echo \frontend\components\Common::getUrlHouse($all_result[$i]) ?>" class="jello_block">

                                                    <?php if($all_result[$i]['img_house'] == '' || $all_result[$i]['img_house'] == null || is_file('/uploads/house/'.$all_result[$i]['id'].'/'.$all_result[$i]['img_house'])){?>
                                                        <img class="img img-responsive img-circle img_house" src="/images/main/sqr_nophoto.jpg" alt="nophoto" style="margin: auto;"/>
                                                    <?php }else{?>
                                                        <img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $all_result[$i]['id']?>/small_<?php echo $all_result[$i]['img_house']?>" alt="<?php echo $all_result[$i]['img_house']?>">
                                                    <?php }?>
                                                    <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                    <p class="price text-right"><?php echo ($all_result[$i]['price'] != null && $all_result[$i]['price'] != 0 && $all_result[$i]['price'] != '0' && $all_result[$i]['price'] != '') ? $all_result[$i]['price'].' <span>PУБ</span>' : ''?></p>
                                                </a>
                                            </div>

                                        <?php }?>

                                    </div>
                                </div>


                                <?php for($m=0; $m<((count($all_result)-6)/6); $m++){?>


                                    <div class="item">
                                        <div class="row">

                                            <?php if(count($all_result)/($m*6+12)>1){?>

                                                <?php
                                                for($i=$m*6+6; $i<$m*6+12; $i++){
                                                    ?>

                                                    <div class="col-sm-4">
                                                        <a href="<?php echo \frontend\components\Common::getUrlHouse($all_result[$i]) ?>" class="jello_block">

                                                            <?php if($all_result[$i]['img_house'] == '' || $all_result[$i]['img_house'] == null || is_file('/uploads/house/'.$all_result[$i]['id'].'/'.$all_result[$i]['img_house'])){?>
                                                                <img class="img img-responsive img-circle img_house" src="/images/main/sqr_nophoto.jpg" alt="nophoto" style="margin: auto;"/>
                                                            <?php }else{?>
                                                                <img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $all_result[$i]['id']?>/small_<?php echo $all_result[$i]['img_house']?>" alt="<?php echo $all_result[$i]['img_house']?>">
                                                            <?php }?>
                                                            <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                            <p class="price text-right"><?php echo ($all_result[$i]['price'] != null && $all_result[$i]['price'] != 0 && $all_result[$i]['price'] != '0' && $all_result[$i]['price'] != '') ? $all_result[$i]['price'].' <span>PУБ</span>' : ''?></p>
                                                        </a>
                                                    </div>

                                                <?php }?>
                                            <?php } else {?>
                                                <?php
                                                for($i=$m*6+6; $i<$m*6+6+(count($all_result)%($m*6+6)); $i++){
                                                    ?>

                                                    <div class="col-sm-4">
                                                        <a href="<?php echo \frontend\components\Common::getUrlHouse($all_result[$i]) ?>" class="jello_block">

                                                            <?php if($all_result[$i]['img_house'] == '' || $all_result[$i]['img_house'] == null || is_file('/uploads/house/'.$all_result[$i]['id'].'/'.$all_result[$i]['img_house'])){?>
                                                                <img class="img img-responsive img-circle img_house" src="/images/main/sqr_nophoto.jpg" alt="nophoto" style="margin: auto;"/>
                                                            <?php }else{?>
                                                                <img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $all_result[$i]['id']?>/small_<?php echo $all_result[$i]['img_house']?>" alt="<?php echo $all_result[$i]['img_house']?>">
                                                            <?php }?>
                                                            <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                            <p class="price text-right"><?php echo ($all_result[$i]['price'] != null && $all_result[$i]['price'] != 0 && $all_result[$i]['price'] != '0' && $all_result[$i]['price'] != '')? $all_result[$i]['price'].' <span>PУБ</span>' : ''?></p>
                                                        </a>
                                                    </div>

                                                <?php }?>
                                            <?php }?>
                                        </div>
                                    </div>

                                <?php }?>
                            <?php } ?>





                        <?php }?>


                    </div>

                    <?php
                    if(count($all_result) > 6){?>
                        <a class="left carousel-control" href="#carousel-logo" role="button" data-slide="prev">
                            <span class="arrow"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-logo" role="button" data-slide="next">
                            <span class="arrow"></span>
                        </a>
                    <?php }?>
                </div>

                </div>
                <div class="row margin_none less_767">

                    <div id="small_carousel-logo" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">

                            <?php
                                if(($all_result != null)){
                            ?>

                                <?php if((count($all_result)> 1)){?>
                                    <?php for($j=0; $j<1; $j++){?>

                                    <div class="item active">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <a href="<?php echo \frontend\components\Common::getUrlHouse($all_result[$j]) ?>">

                                                        <?php if($all_result[$j]['img_house'] == '' || $all_result[$j]['img_house'] == null || is_file('/uploads/house/'.$all_result[$j]['id'].'/'.$all_result[$j]['img_house'])){?>
                                                            <img class="img img-responsive img-circle img_house" src="/images/main/sqr_nophoto.jpg" alt="nophoto" style="margin: auto;"/>
                                                        <?php }else{?>
                                                            <img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $all_result[$j]['id']?>/small_<?php echo $all_result[$j]['img_house']?>" alt="houses<?php echo $all_result[$j]['id']?>">
                                                        <?php }?>

<!--                                                        <img class="img img-responsive img-circle img_house" src="/uploads/house/--><?php //echo $all_result[$j]['id']?><!--/small_--><?php //echo $all_result[$j]['img_house']?><!--" alt="houses--><?php //echo $all_result[$j]['id']?><!--">-->
                                                        <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                        <p class="price text-right"><?php echo ($all_result[$j]['price'] != null && $all_result[$j]['price'] != '' && $all_result[$j]['price'] != '0' && $all_result[$j]['price'] != 0) ? $all_result[$j]['price'].' <span>PУБ</span>' : ''?></p>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    }
                                    ?>

                                        <?php for($m=1; $m<count($all_result); $m++){?>

                                            <div class="item">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <a href="<?php echo \frontend\components\Common::getUrlHouse($all_result[$m]) ?>">

                                                                <?php if($all_result[$m]['img_house'] == '' || $all_result[$m]['img_house'] == null || is_file('/uploads/house/'.$all_result[$m]['id'].'/'.$all_result[$m]['img_house'])){?>
                                                                    <img class="img img-responsive img-circle img_house" src="/images/main/sqr_nophoto.jpg" alt="nophoto" style="margin: auto;transform: scale(1.1)"/>
                                                                <?php }else{?>
                                                                    <img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $all_result[$m]['id']?>/small_<?php echo $all_result[$m]['img_house']?>" alt="houses<?php echo $all_result[$m]['id']?>">
                                                                <?php }?>

<!--                                                                <img class="img img-responsive img-circle img_house" src="/uploads/house/--><?php //echo $all_result[$m]['id']?><!--/small_--><?php //echo $all_result[$m]['img_house']?><!--" alt="houses--><?php //echo $all_result[$m]['id']?><!--">-->
                                                                <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                <p class="price text-right"><?php echo ($all_result[$m]['price'] != null && $all_result[$m]['price'] != '0' && $all_result[$m]['price'] != '' && $all_result[$m]['price'] != 0)? $all_result[$m]['price'].' <span>PУБ</span>' : ''?></p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                        }
                                        ?>

                                <?php } else { ?>
                                        <?php for($j=0; $j<1; $j++){?>

                                            <div class="item active">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <a href="<?php echo \frontend\components\Common::getUrlHouse($all_result[$j]) ?>">

                                                                <?php if($all_result[$j]['img_house'] == '' || $all_result[$j]['img_house'] == null || is_file('/uploads/house/'.$all_result[$j]['id'].'/'.$all_result[$j]['img_house'])){?>
                                                                    <img class="img img-responsive img-circle img_house" src="/images/main/sqr_nophoto.jpg" alt="nophoto" style="margin: auto;transform: scale(1.1)"/>
                                                                <?php }else{?>
                                                                    <img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $all_result[$j]['id']?>/small_<?php echo $all_result[$j]['img_house']?>" alt="houses<?php echo $all_result[$j]['id']?>">
                                                                <?php }?>

<!--                                                                <img class="img img-responsive img-circle img_house" src="/uploads/house/--><?php //echo $all_result[$j]['id']?><!--/small_--><?php //echo $all_result[$j]['img_house']?><!--" alt="houses--><?php //echo $all_result[$j]['id']?><!--">-->
                                                                <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                <p class="price text-right"><?php echo ($all_result[$j]['price'] != null && $all_result[$j]['price'] != '0' && $all_result[$j]['price'] != '' && $all_result[$j]['price'] != 0)? $all_result[$j]['price'].' <span>PУБ</span>' : ''?></p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                        }
                                        ?>
                                <?php } ?>

                            <?php }?>

                        </div>

                        <?php
                        if($result_second != null || count($all_result)>1){?>
                            <a class="left carousel-control" href="#small_carousel-logo" role="button" data-slide="prev">
                                <span class="arrow"></span>
                            </a>
                            <a class="right carousel-control" href="#small_carousel-logo" role="button" data-slide="next">
                                <span class="arrow"></span>
                            </a>
                        <?php }?>
                    </div>

                </div>
            </div><!-- col-sm-12 -->
            <div class="col-sm-12 text-center">
                <a href="/list-houses" class="btn btn-default btn-lg look_all">Смотреть все</a>
            </div><!-- col-sm-12 -->
            <div class="col-sm-12 our_projects padding_none text-center">

                <p class="text_upper text_bold text-center font_18">Наши реализованные проекты</p>

                <div class="col-sm-4 test_link">
                    <a class="test_text" href="/testimonials"><img class="img img-responsive" src="/images/main/testimonials.jpg" alt="testimonials">Отзывы</a>
                </div>
                <div class="col-sm-4 project_link">
                    <a href="/list-projects" class="block_img">
                        <img class="img img-responsive img-circle img_house link" src="/images/main/projects.png" alt="our_projects">
                        <img class="img img-responsive img-circle only_circle link_projects" src="/images/inner/blue_circle.png" alt="blue_circle">
                    </a>
                </div>
                <div class="col-sm-4 good_link">
                    <a class="give_good" href="/give-good"><img class="img img-responsive" src="/images/main/donate.jpg" alt="donate">Благотворительность</a>
                </div>
            </div>
        </div><!-- row -->
    </div><!-- container -->
</section><!-- sky -->

<section class="we_the_best">
    <p class="text_upper text_bold text-center font_18">Преимущества вашего будущего дома</p>
    <div class="wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/nice_place.png" alt="nice place">
                            <p><a href="#">Живописное место</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/keys.png" alt="kyes">
                            <p><a href="#">Дом под ключ</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/house.png" alt="house">
                            <p><a href="#">Индивидуальный дизайнерский проект</a></p>
                        </div>
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/armchair.png" alt="armchair">
                            <p><a href="#">Меблированный дом</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/tv.png" alt="tv">
                            <p><a href="#">Бытовая техника</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/road.png" alt="road">
                            <p><a href="#">Асфальтированный подъезд</a></p>
                        </div>
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/bus.png" alt="bus">
                            <p><a href="#">Общественный транспорт</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/passport.png" alt="passport">
                            <p><a href="#">Прописка</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/cran.png" alt="cran">
                            <p><a href="#">Водоснабжение</a></p>
                        </div>
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/k.png" alt="k">
                            <p><a href="#">Канализация</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/gas.png" alt="gas">
                            <p><a href="#">Магистральный газ</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/radiator.png" alt="radiator">
                            <p><a href="#">Отопление</a></p>
                        </div>
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/warm_floor.png" alt="warm_floor">
                            <p><a href="#">Теплый пол</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/dog.png" alt="dog">
                            <p><a href="#">Охрана</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/camera.png" alt="camera">
                            <p><a href="#">Видеонаблюдение</a></p>
                        </div>
                    </div><!-- row -->
                    <div class="row">
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/plant.png" alt="plant">
                            <p><a href="#">Благоустроенный участок</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/neighbors.png" alt="neighbors">
                            <p><a href="#">Достойные соседи</a></p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img img-responsive img-circle" src="/images/inner/advantages/parking.png" alt="parking">
                            <p><a href="#">Индивидуальная парковка</a></p>
                        </div>
                    </div><!-- row -->
                </div><!-- col-sm-12 -->
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- wrap -->
</section>