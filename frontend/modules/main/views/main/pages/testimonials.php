<section class="sky inner">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <img class="img img-responsive margin_auto padding_top logo_img2" src="/images/main/all_inclusive.png">
                <?php

                echo \frontend\widgets\Menu::widget();

                ?>
                <p class="text_upper text_bold text-center font_18 text_title">Отзывы</p>
                <div class="col-sm-12 our_projects testimonials text-center">
                    <div class="col-sm-4">
                        <a href="/main/main/our_projects" class="block_img">
                            <img class="img img-responsive img-circle img_house" src="/images/main/projects.png" alt="our_projects">
                            <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                        </a>
                    </div>
                    <div class="col-sm-8 text-left">
                        <p>111</p>
                    </div>
                </div>

            </div><!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- sky -->