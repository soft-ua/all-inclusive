<?php $this->title = "Список реализованных проектов";?>
<section class="sky inner">
    <div class="wrap_menu">
        <img class="img img-responsive margin_auto padding_top logo_img2" src="/images/main/all_inclusive.png">
        <?php

        echo \frontend\widgets\Menu::widget();

        ?>
    </div><!-- wrap_menu -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p class="text_upper text_bold text-center font_18 text_title"><?php //echo $nameHouseType['title']?><?php echo $this->title;?></p>
                <div class="list_houses">
                    <div class="row">
                    <?php
                    if(isset($model)){foreach($model as $row):
                    ?>
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="padding-top:10px;">
                            <a href="<?php echo \frontend\components\Common::getUrlProject($row) ?>" class="jello_block">
<!--                                --><?php //var_dump($model);die;?>
                                <?php if($row['img_house'] == '' || $row['img_house'] == null || is_file('/uploads/house/'.$row['id'].'/'.$row['img_house'])){?>
                                    <img class="img img-responsive img-circle img_house" src="/images/main/sqr_nophoto.jpg" alt="nophoto" style="margin: auto;"/>
                                <?php }else{?>
                                    <img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $row['id']?>/small_<?php echo $row['img_house']?>" alt="<?php echo $row['img_house']?>">
                                <?php }?>
                                <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
<!--                                <p class="price text-right">--><?php //echo $row['price']?><!-- <span>PУБ</span></p>-->
                            </a>
                            </div>
                        </div>

                    <?php
                    endforeach;}
                    ?>
                    </div>
                </div>
                <div class="text-center">
                    <?php echo \yii\widgets\LinkPager::widget([
                        'pagination' => $pages,
                        'maxButtonCount' => 5
                    ]) ?>
                </div>

<!--                <div class="text-center less_767">-->
<!--                    --><?php //echo \yii\widgets\LinkPager::widget([
//                        'pagination' => $small_pages
//                    ]) ?>
<!--                </div>-->


            </div><!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- container -->
</section>