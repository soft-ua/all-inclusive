<?php $this->title = 'Контакты';?>
    <section class="sky inner pages contact">
    <div class="wrap_menu">
        <img class="img img-responsive margin_auto padding_top logo_img2" src="/images/main/all_inclusive.png">
        <?php

        echo \frontend\widgets\Menu::widget();

        ?>
    </div><!-- wrap_menu -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p class="text_upper text_bold text-center font_18 text_title"><?php echo $this->title;?></p>

                <div class="col-lg-12 col-sm-12 text-center contact_info">
                    <?php
                    foreach($result_contacts as $row):
                    ?>

                        <p>Адрес: <span><?php echo $row['address'];?></span></p>
                        <p>Наш многоканальный телефон: <a href="tel:<?php echo \frontend\widgets\ContactsAPhone::widget();?>"><?php echo \frontend\widgets\ContactsPhone::widget();?></a></p>
                        <p>График работы: <span><?php echo $row['working_hours'];?></span></p>
                        <p>E-mail: <a href="mailto:<?php echo \frontend\widgets\ContactsEmail::widget();?>"><?php echo \frontend\widgets\ContactsEmail::widget();?></a></p>

                        <?php
                    endforeach;
                    ?>

                </div>


            </div>
            </div>


        </div>
    </div>

    <?php if($result_contacts != null){?>
        <div><p class="yandexmaps"><?php foreach($result_contacts as $row){echo($row['yandexmaps']);}?></p><div id="YMapsID"></div></div>
    <?php }?>

    <?php if($our_team_item != null){?>
        <div class="container our_team">
            <h2 class="text-center">Наша команда</h2>
            <div class="row more_767">

                    <div id="carousel-logo" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">

                                <?php if((count($our_team_item)>=3)){?>
                                    <?php for($j=0; $j<1; $j++){?>
                                        <div class="item active">
                                            <div class="">
                                                <div class="row">
                                                    <?php for($i=($j*3); $i<($j*3)+3; $i++){?>
                                                        <div class="col-sm-4">
                                                            <div class="col-sm-12" style="padding-top:10px;">
                                                                <a href="#" class="jello_block">
                                                                    <?php if(($our_team_item[$i]['img_team'] == null) || ($our_team_item[$i]['img_team'] == '')){
                                                                        ?>
                                                                        <img class="img img-responsive img-circle" src="/uploads/nophoto.jpg"  alt="nophoto" style="margin: auto;"/>
                                                                    <?php }else{?>
                                                                        <img class="img img-responsive img-circle img_house" src="/uploads/team/<?php echo(date('d.m.Y_H-i-s',$our_team_item[$i]['created_at']));?>/small_<?php echo $our_team_item[$i]['img_team']?>" alt="Наша команда - <?php echo $our_team_item[$i]['office_team']?>">
                                                                        <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                    <?php }?>
                                                                    <p class="text-center fio"><?php echo $our_team_item[$i]['name_team']?></p>
                                                                    <p class="text-center office"><?php echo $our_team_item[$i]['office_team']?></p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php for($m=0; $m<(count($our_team_item)-3)/3; $m++){?>
                                        <div class="item">
                                            <div class="">
                                                <div class="row">
                                                    <?php if(count($our_team_item)-$m*3-3>=3){?>
                                                        <?php for($i=($m*3)+3; $i<($m*3)+6; $i++){?>
                                                            <?php if(isset($our_team_item[$i])){?>
                                                                <div class="col-sm-4">
                                                                    <div class="col-sm-12" style="padding-top:10px;">
                                                                        <a href="#" class="jello_block">
                                                                            <?php if(($our_team_item[$i]['img_team'] == null) || ($our_team_item[$i]['img_team'] == '')){
                                                                                ?>
                                                                                <img class="img img-responsive img-circle" src="/uploads/nophoto.jpg"  alt="nophoto" style="margin: auto;"/>
                                                                            <?php }else{?>
                                                                                <img class="img img-responsive img-circle img_house" src="/uploads/team/<?php echo(date('d.m.Y_H-i-s',$our_team_item[$i]['created_at']));?>/small_<?php echo $our_team_item[$i]['img_team']?>" alt="Наша команда - <?php echo $our_team_item[$i]['office_team']?>">
                                                                                <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                            <?php }?>
                                                                            <p class="text-center fio"><?php echo $our_team_item[$i]['name_team']?></p>
                                                                            <p class="text-center office"><?php echo $our_team_item[$i]['office_team']?></p>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                        <?php }?>
                                                    <?php } elseif(count($our_team_item)-$m*3-3 == 2){ ?>
                                                        <?php for($i=($m*3)+3; $i<($m*3)+6; $i++){?>
                                                            <?php if(isset($our_team_item[$i])){?>
                                                                <div class="col-sm-6">
                                                                    <div class="col-sm-12" style="padding-top:10px;padding-bottom:10px;">
                                                                        <a href="#" class="jello_block">
                                                                            <?php if(($our_team_item[$i]['img_team'] == null) || ($our_team_item[$i]['img_team'] == '')){
                                                                                ?>
                                                                                <img class="img img-responsive img-circle" src="/uploads/nophoto.jpg"  alt="nophoto" style="margin: auto;"/>
                                                                            <?php }else{?>
                                                                                <img class="img img-responsive img-circle img_house" src="/uploads/team/<?php echo(date('d.m.Y_H-i-s',$our_team_item[$i]['created_at']));?>/small_<?php echo $our_team_item[$i]['img_team']?>" alt="Наша команда - <?php echo $our_team_item[$i]['office_team']?>">
                                                                                <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                            <?php }?>
                                                                            <p class="text-center fio"><?php echo $our_team_item[$i]['name_team']?></p>
                                                                            <p class="text-center office"><?php echo $our_team_item[$i]['office_team']?></p>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                        <?php }?>
                                                    <?php } else {?>
                                                        <?php for($i=($m*3)+3; $i<($m*3)+6; $i++){?>
                                                            <?php if(isset($our_team_item[$i])){?>
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-12" style="padding-top:10px;padding-bottom:10px;">
                                                                        <a href="#" class="jello_block">
                                                                            <?php if(($our_team_item[$i]['img_team'] == null) || ($our_team_item[$i]['img_team'] == '')){
                                                                                ?>
                                                                                <img class="img img-responsive img-circle" src="/uploads/nophoto.jpg"  alt="nophoto" style="margin: auto;"/>
                                                                            <?php }else{?>
                                                                                <img class="img img-responsive img-circle img_house" src="/uploads/team/<?php echo(date('d.m.Y_H-i-s',$our_team_item[$i]['created_at']));?>/small_<?php echo $our_team_item[$i]['img_team']?>" alt="Наша команда - <?php echo $our_team_item[$i]['office_team']?>">
                                                                                <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                            <?php }?>
                                                                            <p class="text-center fio"><?php echo $our_team_item[$i]['name_team']?></p>
                                                                            <p class="text-center office"><?php echo $our_team_item[$i]['office_team']?></p>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                        <?php }?>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                <?php }
                                elseif((count($our_team_item)==2)){
                                    ?>
                                    <?php for($j=0; $j<1; $j++){?>
                                        <div class="item active">
                                            <div class="">
                                                <div class="row">
                                                    <?php for($i=($j*2); $i<($j*2)+2; $i++){?>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-12" style="padding-top:10px;">
                                                                <a href="#" class="jello_block">
                                                                    <!--                                                                                                        --><?php //var_dump(date('d.m.Y_H-i-s',$our_team_item[$i]['created_at']));die;?>
                                                                    <?php if(($our_team_item[$i]['img_team'] == null) || ($our_team_item[$i]['img_team'] == '')){
                                                                        ?>
                                                                        <img class="img img-responsive img-circle" src="/uploads/nophoto.jpg"  alt="nophoto" style="margin: auto;"/>
                                                                    <?php }else{?>
                                                                        <img class="img img-responsive img-circle img_house" src="/uploads/team/<?php echo(date('d.m.Y_H-i-s',$our_team_item[$i]['created_at']));?>/small_<?php echo $our_team_item[$i]['img_team']?>" alt="Наша команда - <?php echo $our_team_item[$i]['office_team']?>">
                                                                        <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                    <?php }?>
                                                                    <p class="text-center fio"><?php echo $our_team_item[$i]['name_team']?></p>
                                                                    <p class="text-center office"><?php echo $our_team_item[$i]['office_team']?></p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                            </div>

                                        </div>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <?php for($j=0; $j<2; $j++){?>
                                        <div class="item active">
                                            <div class="">
                                                <div class="row">
                                                    <?php for($i=($j); $i<1; $i++){?>
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-12" style="padding-top:10px;">
                                                                <a href="#" class="jello_block">
                                                                    <?php if(($our_team_item[$i]['img_team'] == null) || ($our_team_item[$i]['img_team'] == '')){
                                                                        ?>
                                                                        <img class="img img-responsive img-circle" src="/uploads/nophoto.jpg"  alt="nophoto" style="margin: auto;"/>
                                                                    <?php }else{?>
                                                                        <img class="img img-responsive img-circle img_house" src="/uploads/team/<?php echo(date('d.m.Y_H-i-s',$our_team_item[$i]['created_at']));?>/small_<?php echo $our_team_item[$i]['img_team']?>" alt="Наша команда - <?php echo $our_team_item[$i]['office_team']?>">
                                                                        <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                    <?php }?>
                                                                    <p class="text-center fio"><?php echo $our_team_item[$i]['name_team']?></p>
                                                                    <p class="text-center office"><?php echo $our_team_item[$i]['office_team']?></p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                            </div>

                                        </div>
                                    <?php } ?>
                                <?php } ?>
                        <?php if((count($our_team_item)>3)){?>
                            <a class="left carousel-control" href="#carousel-logo" role="button" data-slide="prev">
                                <span class="arrow"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-logo" role="button" data-slide="next">
                                <span class="arrow"></span>
                            </a>
                        <?php }?>
                        </div>
                    </div>
            </div>


            <div class="row less_767">

                <div id="carousel-logo_small" class="carousel slide view_h" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">

                        <?php if((count($our_team_item)>=2)){?>
                            <?php for($j=0; $j<1; $j++){?>
                                <div class="item active">
                                    <div class="">
                                        <div class="row">
                                            <?php for($i=($j); $i<($j)+1; $i++){?>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12" style="padding-top:10px;">
                                                        <a href="#" class="jello_block">
                                                            <?php if(($our_team_item[$i]['img_team'] == null) || ($our_team_item[$i]['img_team'] == '')){
                                                                ?>
                                                                <img class="img img-responsive img-circle" src="/uploads/nophoto.jpg"  alt="nophoto" style="margin: auto;"/>
                                                            <?php }else{?>
                                                                <img class="img img-responsive img-circle img_house" src="/uploads/team/<?php echo(date('d.m.Y_H-i-s',$our_team_item[$i]['created_at']));?>/small_<?php echo $our_team_item[$i]['img_team']?>" alt="Наша команда - <?php echo $our_team_item[$i]['office_team']?>">
                                                                <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                            <?php }?>
                                                            <p class="text-center fio"><?php echo $our_team_item[$i]['name_team']?></p>
                                                            <p class="text-center office"><?php echo $our_team_item[$i]['office_team']?></p>
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php for($m=1; $m<(count($our_team_item)); $m++){?>
                                <div class="item">
                                    <div class="">
                                        <div class="row">
                                                    <?php if(isset($our_team_item[$i])){?>
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-12" style="padding-top:10px;">
                                                                <a href="#" class="jello_block">
                                                                    <?php if(($our_team_item[$m]['img_team'] == null) || ($our_team_item[$m]['img_team'] == '')){
                                                                        ?>
                                                                        <img class="img img-responsive img-circle" src="/uploads/nophoto.jpg"  alt="nophoto" style="margin: auto;"/>
                                                                    <?php }else{?>
                                                                        <img class="img img-responsive img-circle img_house" src="/uploads/team/<?php echo(date('d.m.Y_H-i-s',$our_team_item[$m]['created_at']));?>/small_<?php echo $our_team_item[$m]['img_team']?>" alt="Наша команда - <?php echo $our_team_item[$m]['office_team']?>">
                                                                        <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                    <?php }?>
                                                                    <p class="text-center fio"><?php echo $our_team_item[$m]['name_team']?></p>
                                                                    <p class="text-center office"><?php echo $our_team_item[$m]['office_team']?></p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        <?php }else{ ?>
                            <?php for($j=0; $j<1; $j++){?>
                                <div class="item active">
                                    <div class="">
                                        <div class="row">
                                            <?php for($i=($j); $i<($j)+1; $i++){?>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12" style="padding-top:10px;">
                                                        <a href="#" class="jello_block">
                                                            <?php if(($our_team_item[$i]['img_team'] == null) || ($our_team_item[$i]['img_team'] == '')){
                                                                ?>
                                                                <img class="img img-responsive img-circle" src="/uploads/nophoto.jpg"  alt="nophoto" style="margin: auto;"/>
                                                            <?php }else{?>
                                                                <img class="img img-responsive img-circle img_house" src="/uploads/team/<?php echo(date('d.m.Y_H-i-s',$our_team_item[$i]['created_at']));?>/small_<?php echo $our_team_item[$i]['img_team']?>" alt="Наша команда - <?php echo $our_team_item[$i]['office_team']?>">
                                                                <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                            <?php }?>
                                                            <p class="text-center fio"><?php echo $our_team_item[$i]['name_team']?></p>
                                                            <p class="text-center office"><?php echo $our_team_item[$i]['office_team']?></p>
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php }?>

                        <?php if((count($our_team_item)>1)){?>
                            <a class="left carousel-control" href="#carousel-logo_small" role="button" data-slide="prev">
                                <span class="arrow"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-logo_small" role="button" data-slide="next">
                                <span class="arrow"></span>
                            </a>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
</section>


<?php
foreach($result_contacts as $row):
?>
<script>

    var addr = '<?php echo $row['address'];?>';

</script>
<?php
    endforeach;
?>