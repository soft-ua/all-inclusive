<?php $this->title = "Просмотр дома";?>
<?php
$simbol = '<div style="page-break-after: always"><span style="display:none">&nbsp;</span></div>';

function findSpace($str){

    $simbol = '<div style="page-break-after: always"><span style="display:none">&nbsp;</span></div>';

//    if(mb_strstr($str, $simbol, true)){
    if(mb_strpos($str, $simbol, true) != false){

        $new_str = mb_strstr($str, $simbol, true);

        return $new_str;
    }else{
        return $str;
    }
}

function echoStr($str){

    $simbol = '<div style="page-break-after: always"><span style="display:none">&nbsp;</span></div>';

    $last_str = mb_strstr($str, $simbol, false);

    return $last_str;
}

$this->registerJs("
//    $(document).ready(function() {
//        $('.fancybox-thumb').fancybox({
//            prevEffect	: 'none',
//            nextEffect	: 'none',
//            helpers	: {
//                title	: {
//                    type: 'outside'
//                },
//                thumbs	: {
//                    width	: 50,
//                    height	: 50
//                }
//            }
//        });
//    });
");
?>

<script>
    var screenWidthJs = window.innerWidth;
    var screenHeightJs = window.innerHeight;
</script>
<?php
$screenWidth = '<script>document.write(screenWidthJs);</script>';
$screenHeight = '<script>document.write(screenHeightJs);</script>';

$str_link = $screenWidth.'x'.$screenHeight;

?>
<section class="sky inner pages view_house">
    <div class="wrap_menu">
        <img class="img img-responsive margin_auto padding_top logo_img2" src="/images/main/all_inclusive.png">
        <?php

        echo \frontend\widgets\Menu::widget();

        ?>
    </div><!-- wrap_menu -->
    <div class="property-images text-center more_767">


        <?php if($model->tour_link != null || $model->tour_link != ''){?>
            <!-- vtour IFRAME -->
<!--            <h2 class="text-center vtour_title">Он-лайн экскурсия по дому</h2>-->
            <p><a class="btn btn-default text-center margin_bottom_30" href="<?php echo '/vtours/'.$model->tour_link.'/tour.html'?>" target="_blank">Виртуальная прогулка по дому</a></p>
            <div class="vtour">
                <iframe src="<?php echo '/vtours/'.$model->tour_link.'/tour.html'?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen>
                </iframe>
            </div>
            <!-- End vtour IFRAME -->
        <?php } else {?>
            <?php if($modelImageMain[0]['img_house'] != null){?>
            <div id="main_photo"></div>
            <script>var mph_name = '<?php echo $modelImageMain[0]['img_house']?>';var mph_id = '<?php echo $modelImageMain[0]['id']?>';</script>
            <?php } else { ?>
                <?php var_dump(is_file('/uploads/house/'.$modelImageMain[0]['id'].'/'.$modelImageMain[0]['img_house']));die;?>
                <img class="img img-responsive text-center" style="margin: auto;" src="/images/main/nophoto.jpg"  alt="no photo" />
            <?php } ?>
        <?php }?>



    </div>
    <div class="property-images text-center less_767">



            <?php if($modelImageMain != null){?>

                <?php if($modelImageMain[0]['img_house'] != null){?>
                <div id="main_photo_small"></div>
                <script>var mph_name = '<?php echo $modelImageMain[0]['img_house']?>';var mph_id = '<?php echo $modelImageMain[0]['id']?>';</script>
                <?php } else { ?>
                    <?php var_dump(is_file('/uploads/house/'.$modelImageMain[0]['id'].'/'.$modelImageMain[0]['img_house']));die;?>
                    <img class="img img-responsive text-center" style="margin: auto;" src="/images/main/nophoto.jpg"  alt="no photo" />
                <?php } ?>

                <?php if($model->tour_link != null || $model->tour_link != ''){?>
                    <p><a href="<?php echo '/'.$model->tour_link.'/tour.html'?>" target="_blank">Открыть виртуальный тур в отдельном окне</a></p>
                <?php }?>
            <?php }?>



    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-12">

                <?php
                if($model->description != null || $model->description != ''){
                    ?>

                    <div class="desc">
                        <h2 class="text-center">Описание дома</h2>

                        <div class="start"><?php echo findSpace($model->description) ?></div>

                        <div id="collapseOne<?php echo $model->id?>" class="panel-collapse collapse">
                            <?php echo echoStr($model->description)?>
                        </div>
                        <div class="col-sm-12 text-center btn_block">

                            <?php if(mb_strstr($model->description, $simbol, true)){?>
                                <a class="btn btn-default btn-lg more_detailed" data-toggle="collapse" href="#collapseOne<?php echo $model->id?>">Подробнее</a>
                            <?php }?>
                        </div><!-- col-sm-12 -->

                    </div>

                <?php } ?>

            </div><!-- col-md-12 -->
<!--            <div class="col-sm-12 text-center btn_block">-->
<!--                <a href="#" class="btn btn-default btn-lg">Подробнее</a>-->
<!--            </div>-->
        </div><!-- row -->
    </div>
</section>

<section class="view_house_slider">
    <div class="container">
        <h2 class="text-center">Фотографии домов</h2>
        <div class="row more_767">

            <div id="carousel-logo" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php if($modelAllImage != null){?>
                        <?php
                        $allImages = [];
                        foreach($modelAllImage as $image){
                            $allImg = str_replace("<img src=\"", "", $image['str_imgs_house']);
                            $allImg = str_replace("\" width=200>", "", $allImg);

                            $allImages = explode(',',$allImg);
//                                var_dump($allImages);die;
                            for($i=0; $i<count($allImages); $i++){
                                $strAll[$i] = strrpos($allImages[$i], '/', -1);
                                $str = substr($allImages[$i], $strAll[$i]+1);
                                $all[$i] = $str;
                            }
                            $allImages = $all;
//                                var_dump($allImages);die;
                            ?>
                            <?php if($allImages[0] == null || $allImages[0] == '' || $allImages[0] === false){?>
                                <h2 class="text-center" style="text-transform: lowercase;">Еще не загрузили фото данного дома</h2>
                            <?php } else {?>
                                <?php if((count($allImages)>3)){?>
                                    <?php for($j=0; $j<1; $j++){?>
                                        <div class="item active">
                                            <div class="">
                                                <div class="row">
                                                    <?php for($i=($j*3); $i<($j*3)+3; $i++){?>
                                                        <div class="col-sm-4">
                                                            <div class="col-sm-12">
                                                                <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="<?php echo $modelAllImage[$j]['id']?>" href="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/wtmarked_<?php echo $allImages[$i]?>">
                                                                    <div class="wrap_img"><img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/small_<?php echo $allImages[$i]?>" alt="houses<?php echo $modelAllImage[$j]['id']?>"></div>
                                                                    <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php for($m=0; $m<((count($allImages)-3)/3); $m++){?>
                                        <!--                                        --><?php //var_dump(ceil((count($allImages)-3)/3));die;?>
                                        <div class="item">
                                            <div class="">
                                                <div class="row">
                                                    <?php if(count($allImages)-$m*3-3>=3){?>
                                                        <?php for($i=($m*3)+3; $i<($m*3)+6; $i++){?>
                                                            <?php if(isset($allImages[$i]) && $allImages[$i] != null){?>
                                                                <div class="col-sm-4">
                                                                    <div class="col-sm-12">
                                                                        <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="<?php echo $modelAllImage[0]['id']?>" href="/uploads/house/<?php echo $modelAllImage[0]['id']?>/wtmarked_<?php echo $allImages[$i]?>">
                                                                            <div class="wrap_img"><img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $modelAllImage[0]['id']?>/small_<?php echo $allImages[$i]?>" alt="houses<?php echo $modelAllImage[0]['id']?>"></div>
                                                                            <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                        <?php }?>
                                                    <?php } elseif(count($allImages)-$m*3-3 == 2){ ?>
                                                        <?php for($i=($m*3)+3; $i<($m*3)+6; $i++){?>
                                                            <?php if(isset($allImages[$i]) && $allImages[$i] != null){?>
                                                                <div class="col-sm-6">
                                                                    <div class="col-sm-12">
                                                                        <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="<?php echo $modelAllImage[0]['id']?>" href="/uploads/house/<?php echo $modelAllImage[0]['id']?>/wtmarked_<?php echo $allImages[$i]?>">
                                                                            <div class="wrap_img"><img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $modelAllImage[0]['id']?>/small_<?php echo $allImages[$i]?>" alt="houses<?php echo $modelAllImage[0]['id']?>"></div>
                                                                            <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                        <?php }?>
                                                    <?php } elseif(count($allImages)-$m*3-3 == 1) {?>
                                                        <?php for($i=($m*3)+3; $i<($m*3)+6; $i++){?>
                                                            <?php if(isset($allImages[$i]) && $allImages[$i] != null){?>
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-12">
                                                                        <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="<?php echo $modelAllImage[0]['id']?>" href="/uploads/house/<?php echo $modelAllImage[0]['id']?>/wtmarked_<?php echo $allImages[$i]?>">
                                                                            <div class="wrap_img"><img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $modelAllImage[0]['id']?>/small_<?php echo $allImages[$i]?>" alt="houses<?php echo $modelAllImage[0]['id']?>"></div>
                                                                            <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php }?>
                                                        <?php }?>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>


                                <?php }elseif((count($allImages)==3)){ ?>
                                    <?php for($j=0; $j<1; $j++){?>
                                        <div class="item active">
                                            <div class="">
                                                <div class="row">
                                                    <?php for($i=($j*3); $i<($j*3)+3; $i++){?>
                                                        <div class="col-sm-4">
                                                            <div class="col-sm-12">
                                                                <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="<?php echo $modelAllImage[$j]['id']?>" href="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/wtmarked_<?php echo $allImages[$i]?>">
                                                                    <div class="wrap_img"><img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/small_<?php echo $allImages[$i]?>" alt="houses<?php echo $modelAllImage[$j]['id']?>"></div>
                                                                    <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                            </div>

                                        </div>
                                    <?php } ?>
                                <?php }elseif((count($allImages)==2)){ ?>
                                    <?php for($j=0; $j<1; $j++){?>
                                        <div class="item active">
                                            <div class="">
                                                <div class="row">
                                                    <?php for($i=($j*2); $i<($j*2)+2; $i++){?>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-12">
                                                                <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="<?php echo $modelAllImage[$j]['id']?>" href="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/wtmarked_<?php echo $allImages[$i]?>">
                                                                    <div class="wrap_img"><img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/small_<?php echo $allImages[$i]?>" alt="houses<?php echo $modelAllImage[$j]['id']?>"></div>
                                                                    <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                            </div>

                                        </div>
                                    <?php } ?>
                                <?php }elseif((count($modelAllImage)==1)){ ?>
                                    <?php for($j=0; $j<2; $j++){?>
                                        <div class="item active">
                                            <div class="">
                                                <div class="row">
                                                    <?php for($i=($j); $i<1; $i++){?>
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-12">
                                                                <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="<?php echo $modelAllImage[$j]['id']?>" href="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/wtmarked_<?php echo $allImages[$i]?>">
                                                                    <div class="wrap_img"><img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/small_<?php echo $allImages[$i]?>" alt="houses<?php echo $modelAllImage[$j]['id']?>"></div>
                                                                    <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <h2 class="text-center">Еще не загрузили фото данного дома</h2>
                                <?php }?>
                            <?php }?>
                        <?php }?>
                    <?php } ?>

                </div>
                <?php if(count($allImages) > 3){?>
                    <a class="left carousel-control" href="#carousel-logo" role="button" data-slide="prev">
                        <span class="arrow"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-logo" role="button" data-slide="next">
                        <span class="arrow"></span>
                    </a>
                <?php } ?>
            </div>

        </div>

        <div class="row less_767">

            <div id="carousel-logo_small" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">

                    <?php if($allImages[0] == null || $allImages[0] == '' || $allImages[0] === false){?>
                        <h2 class="text-center" style="text-transform: lowercase;">Еще не загрузили фото данного дома</h2>
                    <?php } else {?>

                        <?php if((count($allImages)>=2)){?>
                            <?php for($j=0; $j<1; $j++){?>
                                <div class="item active">
                                    <div class="">
                                        <div class="row">
                                            <?php for($i=($j); $i<($j)+1; $i++){?>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12">
                                                        <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="small_<?php echo $modelAllImage[$j]['id']?>" href="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/wtmarked_<?php echo $allImages[$i]?>">
                                                            <div class="wrap_img"><img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/small_<?php echo $allImages[$i]?>" alt="houses<?php echo $modelAllImage[$j]['id']?>"></div>
                                                            <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php for($m=1; $m<(count($allImages)); $m++){?>
                                <div class="item">
                                    <div class="">
                                        <div class="row">
                                            <?php if(isset($allImages[$m]) && $allImages[$m] != null){?>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12">
                                                        <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="small_<?php echo $modelAllImage[0]['id']?>" href="/uploads/house/<?php echo $modelAllImage[0]['id']?>/wtmarked_<?php echo $allImages[$m]?>">
                                                            <div class="wrap_img"><img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $modelAllImage[0]['id']?>/small_<?php echo $allImages[$m]?>" alt="houses<?php echo $modelAllImage[0]['id']?>"></div>
                                                            <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        <?php } else { ?>

                            <?php for($j=0; $j<1; $j++){?>
                                <div class="item active">
                                    <div class="">
                                        <div class="row">
                                            <?php for($i=($j); $i<($j)+1; $i++){?>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12">
                                                        <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="small_<?php echo $modelAllImage[$j]['id']?>" href="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/wtmarked_<?php echo $allImages[$i]?>">
                                                            <div class="wrap_img"><img class="img img-responsive img-circle img_house" src="/uploads/house/<?php echo $modelAllImage[$j]['id']?>/small_<?php echo $allImages[$i]?>" alt="houses<?php echo $modelAllImage[$j]['id']?>"></div>
                                                            <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        <?php }?>

                        <?php if((count($allImages)>1)){?>
                        <a class="left carousel-control" href="#carousel-logo_small" role="button" data-slide="prev">
                            <span class="arrow"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-logo_small" role="button" data-slide="next">
                            <span class="arrow"></span>
                        </a>
                    <?php }?>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="wrap_yamap view"><p class="yandexmaps"><?php echo($model->location);?></p><div id="YMapsID"></div></div>

<script>

    var addr = '<?php echo $model->address;?>';

</script>

<script type="text/javascript" src="http://yandex.st/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        (function (){
            var baseUrl = '<?php echo \Yii::$app->params['baseUrl']; ?>';
            $('#main_photo').html('<h2 class="text-center loading">Фотография загружается</h2><p class="text-center"><img src="/images/fancy/fancybox_loading.gif" alt="loading"></p>');
            $('#main_photo_small').html('<h2 class="text-center loading">Фотография загружается</h2><p class="text-center"><img src="/images/fancy/fancybox_loading.gif" alt="loading"></p>');

            screenWidthJs = String(screenWidthJs);
            screenHeightJs = String(screenHeightJs);

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if(screenWidthJs > 767){
                        $('#main_photo').html(xmlhttp.responseText);
                    }else{
                        $('#main_photo_small').html(xmlhttp.responseText);
                    }
                    setTimeout(function(){
                        if(!$('.pluso-wrap').hasClass('opac')){
                            $('.pluso-vkontakte').after($('.more_350 .pluso-instagram'));
                            $('.pluso .pluso-instagram').css('vertical-align','top');
                            $('.pluso-wrap').addClass('opac');
                        }
                    },2000);
                }
            };
            xmlhttp.open("GET", '//'+baseUrl+"/main/main/mainwatermark/?n=" + mph_name+"&id="+mph_id+"&wsc="+screenWidthJs+"&hsc="+screenHeightJs, true);
            xmlhttp.send();
        })();


        var arrALinks = $('a.fancybox-thumb');
        for(var t = 0; t < arrALinks.length; t++){
            var nameLink = arrALinks[t].href.split('/wtmarked_',-1);
            var idLink = arrALinks[t].href.split('/',-1);
            var str_nameLink = '';
            var str_idLink = '';

            for (var s = nameLink.length-1; s < nameLink.length; s++) {
                str_nameLink = nameLink[s];
            }

            for (var f = idLink.length-2; f < idLink.length-1; f++) {
                str_idLink = idLink[f];
            }

            arrALinks[t].href = "/main/main/manywatermark/?id="+str_idLink+"&wsc="+screenWidthJs+"&hsc="+screenHeightJs+"&n=" + str_nameLink;
        }

        $('.fancybox-thumb').fancybox({
            padding : 0,
        });
        setTimeout(function(){
            if(!$('.pluso-wrap').hasClass('opac')){
                $('.pluso-vkontakte').after($('.more_350 .pluso-instagram'));
                $('.pluso .pluso-instagram').css('vertical-align','top');
                $('.pluso-wrap').addClass('opac');
            }
        },2000);

    });
</script>