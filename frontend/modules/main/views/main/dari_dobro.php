<?php $this->title = "Дари добро"?>
<script>
    var screenWidthJs = window.innerWidth;
    var screenHeightJs = window.innerHeight;
</script>
<section class="sky inner">
    <div class="wrap_menu">
        <img class="img img-responsive margin_auto padding_top logo_img2" src="/images/main/all_inclusive.png">
        <?php

        echo \frontend\widgets\Menu::widget();

        ?>
    </div><!-- wrap_menu -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p class="text_upper text_bold text-center font_18 text_title"><?php echo $this->title;?></p>


                <?php
                if(($modelDonate != null)){
                    ?>
                    <div class="row give_good_block">

                        <div class="col-sm-4">
                            <div class="row">

                                <?php
                                foreach($modelDonate as $row):
                                    ?>
                                    <?php if(is_file(Yii::getAlias("@frontend/web/uploads/donate/".$row['id'].'/'.$row['img_donates'])) && !$row['img_donates'] == null){?>
                                        <div>
                                            <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="main_<?php echo $row['id']?>" href="/uploads/donate/<?php echo $row['id']?>/wtmarked_<?php echo $row['img_donates']?>">
                                                <img class="img img-responsive img-thumbnail" src="/uploads/donate/<?php echo $row['id']?>/<?php echo $row['img_donates']?>" alt="<?php echo $row['img_donates'] ?>"/>
                                            </a>
                                        </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="row thumbs_block">
                                <?php
                                    $allImages = [];

                                    $allImg = str_replace("<img src=\"", "", $row['str_imgs_donates']);
                                    $allImg = str_replace("\" width=200>", "", $allImg);

                                    $allImages = explode(',',$allImg);
                                    for($i=0; $i<count($allImages); $i++){
                                        $strAll[$i] = strrpos($allImages[$i], '/', -1);
                                        $str = substr($allImages[$i], $strAll[$i]+1);
                                        $all[$i] = $str;
                                    }
                                    $allImages = $all;
                                        foreach($allImages as $image){
                                    ?>
                                    <?php
                                    if(($row['str_imgs_donates'] != null && is_file(Yii::getAlias("@frontend/web/uploads/donate/".$row['id'].'/'.$image)))){
                                        ?>

                                            <div class="col-sm-6">
                                                <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="<?php echo $row['id']?>" href="/uploads/donate/<?php echo $row['id'] ?>/wtmarked_<?php echo $image ?>">
                                                    <img class="img img-responsive img-thumbnail" src="/uploads/donate/<?php echo $row['id'] ?>/small_<?php echo $image ?>" alt="<?php echo $image ?>"/>
                                                </a>
                                            </div>
                                        <?php }?>

                                    <?php
                                    }
                                    ?>

                                    <?php
                                endforeach;
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    if(($modelDonate != null)) {
                        foreach ($modelDonate as $row):
                            if ($row['description'] != null) {
                                ?>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="blue">
                                            <?php echo $row['description'] ?>
                                            <p><?php echo $row['name'] ?>, <?php echo $row['office_donate'] ?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        endforeach;}
                    ?>


                    <div class="text-center">
                        <?php echo \yii\widgets\LinkPager::widget([
                            'pagination' => $pages,
                            'maxButtonCount' => 5
                        ]) ?>
                    </div>
                <?php }?>
            </div><!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- sky -->

<script type="text/javascript" src="http://yandex.st/jquery/1.7.1/jquery.min.js"></script>

<script type="text/javascript">

    $(document).ready(function() {
        var arrALinks = $('a.fancybox-thumb');
        for(var t = 0; t < arrALinks.length; t++){
            var nameLink = arrALinks[t].href.split('/wtmarked_',-1);
            var idLink = arrALinks[t].href.split('/',-1);
            var str_nameLink = '';
            var str_idLink = '';

            for (var s = nameLink.length-1; s < nameLink.length; s++) {
                str_nameLink = nameLink[s];
            }

            for (var f = idLink.length-2; f < idLink.length-1; f++) {
                str_idLink = idLink[f];
            }

            arrALinks[t].href = "/main/main/manywatermarkdonate/?id="+str_idLink+"&wsc="+screenWidthJs+"&hsc="+screenHeightJs+"&n=" + str_nameLink;
        }

        $('.fancybox-thumb')
            .attr('rel', 'gallery')
            .fancybox({
                padding : 0
            });

    });
</script>