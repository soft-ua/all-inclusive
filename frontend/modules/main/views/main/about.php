<?php $this->title = 'О нас';?>
<section class="sky inner">
    <div class="wrap_menu">
        <img class="img img-responsive margin_auto padding_top logo_img2" src="/images/main/all_inclusive.png">
        <?php

        echo \frontend\widgets\Menu::widget();

        ?>
    </div><!-- wrap_menu -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p class="text_upper text_bold text-center font_18 text_title"><?php echo $this->title;?></p>


                <?php
                foreach($model as $row):
                ?>
                <?php echo $row['text_about']; ?>
                <?php endforeach; ?>

            </div><!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- sky -->