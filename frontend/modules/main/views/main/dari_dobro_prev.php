<?php $this->title = "Дари добро"?>
<?php
$this->registerJs("
    $(document).ready(function() {
        $('.fancybox').fancybox();
        $('.fancybox-thumb').fancybox({
            prevEffect	: 'none',
            nextEffect	: 'none',
            helpers	: {
                title	: {
                    type: 'outside'
                },
                thumbs	: {
                    width	: 50,
                    height	: 50
                }
            }
        });
    });
");
?>
<section class="sky inner">
    <div class="wrap_menu">
        <img class="img img-responsive margin_auto padding_top logo_img2" src="/images/main/all_inclusive.png">
        <?php

        echo \frontend\widgets\Menu::widget();

        ?>
    </div><!-- wrap_menu -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p class="text_upper text_bold text-center font_18 text_title"><?php echo $this->title;?></p>

                <script>
                    jQuery(document).ready(function ($) {
                        var _SlideshowTransitions = [

                        ];

                        var options = {
                            $AutoPlay: false,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                            $AutoPlayInterval: 3000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                            $PauseOnHover: 1,                                //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                            $DragOrientation: 0,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                            $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                            $SlideDuration: 3000,                                //Specifies default duration (swipe) for slide in milliseconds

                            $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                                $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                                $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
                                $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                                $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                            },

                            $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
                                $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                                $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                                $AutoCenter: 2,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                                $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                            },

                            $ThumbnailNavigatorOptions: {                       //[Optional] Options to specify and enable thumbnail navigator or not
                                $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                                $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                                $ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                                $Lanes: 2,                                      //[Optional] Specify lanes to arrange thumbnails, default value is 1
                                $SpacingX: 14,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                                $SpacingY: 12,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                                $DisplayPieces: 2,                             //[Optional] Number of pieces to display, default value is 1
                                $ParkingPosition: 4,                          //[Optional] The offset position to park thumbnail
                                $Orientation: 2                                //[Optional] Orientation to arrange thumbnails, 1 horizental, 2 vertical, default value is 1
                            }
                        };

                        var jssor_slider1 = new $JssorSlider$("slider1_container", options);
                        //responsive code begin
                        //you can remove responsive code if you don't want the slider scales while window resizes
                        function ScaleSlider() {
                            var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                            if (parentWidth) {
                                jssor_slider1.$ScaleWidth(Math.max(Math.min(parentWidth, $('#slider1_container').parent().width() + 15), 300));

                                if($(window).width()>570) {
                                    $('#slider1_container').css('height', (($('#slider1_container').parent().width() + 35) * 0.33));
                                    $('#slider1_container div').css('height', ($('#slider1_container').parent().width() * 0.33));
                                    $('#slider1_container').css('width', ($('#slider1_container').parent().width()));


                                    $('.child_slider1_container').css('height', ($('#slider1_container').parent().width() * 0.33));
                                    $('.child_slider1_container').css('width', ($('#slider1_container').parent().width() * 0.5));
                                    $('.child_slider1_container > div').css('height', ($('#slider1_container').parent().width() * 0.33));
                                    $('.child_slider1_container > div').css('width', ($('#slider1_container').parent().width() * 0.5));
                                    $('.child_slider1_container img').css('width', '100%');
                                    $('.child_slider1_container img').css('height', '100%');

                                    $('.jssora05l').css('top', (($('#slider1_container').parent().width() * 0.33) - 30) / 2);
                                    $('.jssora05r').css('top', (($('#slider1_container').parent().width() * 0.33) - 30) / 2);


                                    $('.p').css('height', (($('#slider1_container').parent().width() * 0.33) - 30) / 2);
                                    $('.w').css('height', (($('#slider1_container').parent().width() * 0.33) - 30) / 2);
                                    $('.c').css('height', (($('#slider1_container').parent().width() * 0.33) - 30) / 2);
                                    $('.jssort02').css('height', (($('#slider1_container').parent().width() * 0.33) - 30) / 2);

                                    $('.child_slider1_container').css('height', ($('#slider1_container').parent().width() * 0.33));
                                    $('.jssort02').css('height', ($('#slider1_container').parent().width() * 0.33));

                                }else{
                                    $('#slider1_container').css('height', (($('#slider1_container').parent().width()) * 0.6));
                                    $('#slider1_container div').css('height', ($('#slider1_container').parent().width() * 0.6));
                                    $('#slider1_container').css('width', ($('#slider1_container').parent().width()));


                                    $('.child_slider1_container').css('width', ($('#slider1_container').parent().width() *0.95));
                                    $('.child_slider1_container').css('height', ($('#slider1_container').parent().width() * 0.54));
                                    $('.child_slider1_container').css('left', '0%');
                                    $('.child_slider1_container > div').css('height', (($('#slider1_container').parent().width()-30) * 0.6));
                                    $('.child_slider1_container > div').css('width', (($('#slider1_container').parent().width()-5) * 1));
                                    $('.child_slider1_container img').css('width', '100%');
                                    $('.child_slider1_container img').css('height', '100%');

                                    $('.jssort02').css('display', 'none');
                                    $('.jssort02 > div > div').css('height', $('.jssort02 > div > div').height()-65);

                                    $('.jssort02>div>div').css('top',$('#slider1_container').width()*0.33/2);
                                }
                            }else{
                                window.setTimeout(ScaleSlider, 30);
                            }
                        }
                        ScaleSlider();

                        $(window).bind("load", ScaleSlider);
                        $(window).bind("resize", ScaleSlider);
                        $(window).bind("orientationchange", ScaleSlider);
                        //responsive code end
                    });


                </script>

                <?php $heightSlider1 = $this->registerJs("(($('#slider1_container').parent().width()+15)*0.33+'px')");
                ?>

                <!-- Jssor Slider Begin -->
                <!-- To move inline styles to css file/block, please specify a class name for each element. -->
                <?php
                if(($modelDonate != null)){
                    ?>
                    <div id="slider1_container" style="position: relative; top: 0px; left: 0px; overflow: hidden; height:  <?php echo $this->registerJs("$('#slider1_container').css('height',$('#slider1_container').width()*0.33)");?>px;">

                        <!-- Loading Screen -->
                        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block; background-color: #000000; top: 0px; left: 0px; width: 100%;height:100%;">
                            </div>
                            <div style="position: absolute; display: block; background: url(/images/main/img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
                            </div>
                        </div>

                        <!-- Slides Container -->
                        <div u="slides" class="child_slider1_container" style="cursor: pointer; position: absolute; right: 50%; top: 0px; width: 570px; height: <?php echo $this->registerJs("$('.child_slider1_container').css('height',$('#slider1_container').width()*0.33)");?>px; overflow: hidden; border-radius: 20px">

                            <?php
                            foreach($modelDonate as $row):
                                ?>




                                <?php if(is_file(Yii::getAlias("@frontend/web/uploads/donate/".$row['id'].'/'.$row['img_donates'])) && !$row['img_donates'] == null){?>
                                    <div>
                                        <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="<?php echo $row['id']?>" href="/uploads/donate/<?php echo $row['id']?>/<?php echo $row['img_donates']?>">
                                            <img u="image" src="/uploads/donate/<?php echo $row['id']?>/small_<?php echo $row['img_donates']?>" alt="<?php echo $row['img_donates'] ?>"/>
                                        </a>
                                        <img u="thumb" src="/uploads/donate/<?php echo $row['id']?>/small_<?php echo $row['img_donates']?>" alt="small_<?php echo $row['img_donates'] ?>"/>
                                    </div>
                                <?php } else {?>
                                <!--                                    <div>-->
                                <!--                                        <img u="image" src="--><?php //echo \common\components\UserComponent::getNoImage();?><!--" alt="no photo"/>-->
                                <!--                                        <img u="thumb" src="--><?php //echo \common\components\UserComponent::getNoImage();?><!--" alt="small no photo"/>-->
                                <!--                                    </div>-->
                                <?php }?>



                                <?php
                                $allImages = [];

                                $allImg = str_replace("<img src=\"", "", $row['str_imgs_donates']);
                                $allImg = str_replace("\" width=200>", "", $allImg);

                                $allImages = explode(',',$allImg);
                                for($i=0; $i<count($allImages); $i++){
                                    $strAll[$i] = strrpos($allImages[$i], '/', -1);
                                    $str = substr($allImages[$i], $strAll[$i]+1);
                                    $all[$i] = $str;
                                }
                                $allImages = $all;
                                    foreach($allImages as $image){
                                ?>
                                <?php
                                if(($row['str_imgs_donates'] != null && is_file(Yii::getAlias("@frontend/web/uploads/donate/".$row['id'].'/'.$image)))){
                                    ?>

                                        <div>
                                            <a class="fancybox-thumb" rel="fancybox-thumb" data-fancybox-group="<?php echo $row['id']?>" href="/uploads/donate/<?php echo $row['id'] ?>/<?php echo $image ?>">
                                                <img u="image" src="/uploads/donate/<?php echo $row['id'] ?>/small_<?php echo $image ?>" alt="<?php echo $image ?>"/>
                                            </a>
                                            <img u="thumb" src="/uploads/donate/<?php echo $row['id'] ?>/small_<?php echo $image ?>" alt="small_<?php echo $image ?>"/>
                                        </div>
                                    <?php }?>

                                <?php
                                }
                                ?>

                                <?php
                            endforeach;
                            ?>

                        </div>

                        <!--#region Arrow Navigator Skin Begin -->
                        <style>
                            /* jssor slider arrow navigator skin 05 css */
                            /*
                            .jssora05l                  (normal)
                            .jssora05r                  (normal)
                            .jssora05l:hover            (normal mouseover)
                            .jssora05r:hover            (normal mouseover)
                            .jssora05l.jssora05ldn      (mousedown)
                            .jssora05r.jssora05rdn      (mousedown)
                            */
                            .jssora05l, .jssora05r {
                                display: block;
                                position: absolute;
                                /* size of arrow element */
                                width: 40px;
                                height: 40px;
                                cursor: pointer;
                                background: url('/images/inner/a17.png') no-repeat;
                                overflow: hidden;
                            }
                            .jssora05l { background-position: -10px -40px; }
                            .jssora05r { background-position: -70px -40px; }
                            .jssora05l:hover { background-position: -130px -40px; }
                            .jssora05r:hover { background-position: -190px -40px; }
                            .jssora05l.jssora05ldn { background-position: -250px -40px; }
                            .jssora05r.jssora05rdn { background-position: -310px -40px; }
                        </style>
                        <!-- Arrow Left -->
                    <span u="arrowleft" class="jssora05l" style="top: 158px; left: 1%">
                    </span>
                        <!-- Arrow Right -->
                    <span u="arrowright" class="jssora05r" style="top: 158px; left: 45%;">
                    </span>
                        <!--#endregion Arrow Navigator Skin End -->
                        <!--#region Thumbnail Navigator Skin Begin -->
                        <!-- Help: http://www.jssor.com/development/slider-with-thumbnail-navigator-jquery.html -->
                        <style>
                            /* jssor slider thumbnail navigator skin 02 css */
                            /*
                            .jssort02 .p            (normal)
                            .jssort02 .p:hover      (normal mouseover)
                            .jssort02 .p.pav        (active)
                            .jssort02 .p.pdn        (mousedown)
                            */

                            .jssort02 {
                                position: absolute;
                                /* size of thumbnail navigator container */
                                width: 50%;
                                height:  370px;
                            }


                            .jssort02 > div > div{
                                /*width: 46%;*/
                                height: 50% !important;
                                left: 1% !important;
                                display: inline-block;
                            }

                            .jssort02 .p {
                                position: absolute;
                                top: 4px !important;
                                left: 0;
                                width: 46%;
                                height: 90.5%;
                                cursor: move;
                            }

                            .jssort02 .t {
                                position: absolute;
                                top: 0;
                                left: 0;
                                width: 100%;
                                height: 100%;
                                border: none;
                                -webkit-border-radius: 20px;
                                -moz-border-radius: 20px;
                                border-radius: 20px;
                            }

                            .jssort02 .w {
                                position: absolute;
                                top: 2px;
                                left: 2px;
                                width: 100%;
                                height: 100%;
                            }

                            .jssort02 .c {
                                position: absolute;
                                top: 0px;
                                left: 0px;
                                width: 100%;
                                height: 100%;
                                /*border: #000 2px solid;*/
                                box-sizing: content-box;
                                background: url(/images/main/img/t01.png) -800px -800px no-repeat;
                                _background: none;
                            }

                            .jssort02 .pav .c {
                                top: 2px;
                                _top: 0px;
                                left: 2px;
                                _left: 0px;
                                width: 100%;
                                height: 100%;
                                /*border: #000 0px solid;*/
                                _border: #fff 2px solid;
                                background-position: 50% 50%;
                            }

                            .jssort02 .p:hover .c {
                                top: 2px;
                                left: 2px;
                                width: 100%;
                                height: 100%;
                                /*border: #fff 2px solid;*/
                                background-position: 50% 50%;
                            }

                            .jssort02 .p.pdn .c {
                                background-position: 50% 50%;
                                width: 100%;
                                height: 100%;
                                /*border: #000 2px solid;*/
                            }

                            * html .jssort02 .c, * html .jssort02 .pdn .c, * html .jssort02 .pav .c {
                                /* ie quirks mode adjust */
                                width /**/: 100%;
                                height /**/: 50%;
                            }
                        </style>

                        <!-- thumbnail navigator container -->
                        <div u="thumbnavigator" class="jssort02" style="right: 0px; bottom: 0px;">
                            <!-- Thumbnail Item Skin Begin -->
                            <div u="slides" style="cursor: default;">
                                <div u="prototype" class="p">
                                    <div class=w><div u="thumbnailtemplate" class="t"></div></div>
                                    <div class=c></div>
                                </div>
                            </div>
                            <!-- Thumbnail Item Skin End -->
                        </div>
                    </div>
                    <!-- Jssor Slider End -->

                    <?php
                    if(($modelDonate != null)) {
                        foreach ($modelDonate as $row):
                            if ($row['description'] != null) {
                                ?>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="blue">
                                            <?php echo $row['description'] ?>
                                            <p><?php echo $row['name'] ?>, <?php echo $row['office_donate'] ?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        endforeach;}
                    ?>


                    <div class="text-center">
                        <?php echo \yii\widgets\LinkPager::widget([
                            'pagination' => $pages,
                            'maxButtonCount' => 5
                        ]) ?>
                    </div>
                <?php }?>
            </div><!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- sky -->