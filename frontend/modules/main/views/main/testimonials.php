<?php $this->title = "Отзывы наших клиентов";?>




<?php
function write_years($data){

//                                                var_dump($age);
    $age = mb_substr($data, -2);
    $age = (int)$age;
//                                                var_dump($age);die;
    if($age >= 11 && $age <=  14){
        $years = ' лет';
//                                                    var_dump($age);die;
    }else{
//                                                    var_dump('hello');die;
        $age = mb_substr($age, -1);
        $age = (int)$age;
//                                                    var_dump($age);die;
        if($age == 1){
            $years = ' год';
        }elseif($age > 1 && $age < 5){
            $years = ' годa';
        }else{
            $years = ' лет';
        }
    }
    echo($years);
}

//    $test_str = "Купили дом в деревне, всё очень понравилось. Удобное расположение комнат, много пространства, хватает места для всего. В доме тепло. Перезимовали уже две зимы.  Удобно, что парковка на две машины. Посадил свой огород, очень доволен. Живем с женой круглогодично, в Москву почти не ездим. Спасибо за дом, ни сколько не разочарованы в покупке.";
//
//    $begin_str = mb_substr($test_str, 0, 400);
//
//    $end_str = mb_substr($test_str, 400);
//
//    var_dump($test_str);
//    var_dump($begin_str);
//    var_dump($end_str);
//    die;


function findSpace($str, $num_com){

    mb_internal_encoding("UTF-8");
    $short_str = mb_substr($str, 0, 273);

    $simbol = " ";

    $pos = mb_strripos($short_str, $simbol);
    $new_str = mb_substr($str, 0, $pos);
//        $new_str = mb_substr($str, 0, 270);
    $new_str = $new_str.'<span class="text_dots">...</span><a class="three_dots collapsed" data-toggle="collapse" href="#collapseOne' . $num_com . '" aria-expanded="false" style="transition: 0.5s;display: inline !important;"> далее</a>';

    return $new_str;
}

function echoStr($str){
    mb_internal_encoding("UTF-8");
    $short_str = mb_substr($str, 0, 273);

    $simbol = " ";

    $pos = mb_strripos($short_str, $simbol);


    $new_str = mb_substr($str, 0, $pos);
//        $new_str = mb_substr($str, 0, 400);

    $last_str = mb_substr($str, $pos);
//        $last_str = mb_substr($str, 270);

    return $last_str;

}
?>

<?php
$this->registerJs("
//    $(document).ready(function() {
//		$('.fancybox').fancybox();
//	});
");
?>
<script>
    var screenWidthJs = window.innerWidth;
    var screenHeightJs = window.innerHeight;
</script>
<?php
$screenWidth = '<script>document.write(screenWidthJs);</script>';
$screenHeight = '<script>document.write(screenHeightJs);</script>';

$str_link = $screenWidth.'x'.$screenHeight;

?>

<section class="sky inner">
    <div class="wrap_menu">
        <img class="img img-responsive margin_auto padding_top logo_img2" src="/images/main/all_inclusive.png">
        <?php

        echo \frontend\widgets\Menu::widget();

        ?>
    </div><!-- wrap_menu -->
    <div class="container">
        <div class="row">
            <p class="text_upper text_bold text-center font_18 text_title"><?php echo $this->title;?></p>

            <?php if($model != null){?>

                <div class="col-sm-12 our_projects testimonials common_test text-center">

                    <?php
                    foreach($model as $row):
                        ?>



                        <?php
                        if(mb_strlen($row['text_comment'])>273){
                            ?>

                            <div class="row">
                                <div class="col-sm-4">
                                    <a class="fancybox-thumb" href="<?php if(isset($row['comment_img']) && $row['comment_img'] != null){ echo "/uploads/comment/".date('d.m.Y_H-i-s',$row['created_at']) ."/wtmarked_". $row['comment_img'];} else {echo "/images/main/sqr_nophoto.jpg";}?>" class="block_img">

                                        <?php if(isset($row['comment_img']) && $row['comment_img'] != null){?>
                                            <img class="img img-responsive img-circle img_house" src="<?php echo "/uploads/comment/".date('d.m.Y_H-i-s',$row['created_at']) ."/small_". $row['comment_img'];?>" alt="<?php echo $row['comment_img']."_".$row['id_comment']?>">
                                        <?php } else {?>
                                            <img class="img img-responsive img-circle img_house" src="<?php echo "/images/main/sqr_nophoto.jpg";?>" alt="<?php echo "nophoto";?>" style="margin:auto;transform:scale(1.1)">
                                        <?php }?>

                                        <!--                                    <img class="img img-responsive img-circle img_house" src="--><?php //if(isset($row['comment_img']) && $row['comment_img'] != null){ echo "/uploads/comment/".date('d.m.Y_H-i-s',$row['created_at']) ."/small_". $row['comment_img'];}else{echo "/images/main/sqr_nophoto.jpg";}?><!--" alt="--><?php //if(isset($row['comment_img']) && $row['comment_img'] != null){ echo $row['comment_img']."_".$row['id_comment'];}else{echo "nophoto";}?><!--">-->
                                        <!--                                    --><?php //if(isset($row['comment_img']) && $row['comment_img'] != null){?><!--<img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">--><?php //}?>
                                        <img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle">
                                    </a>
                                </div>
                                <div class="col-sm-8 testims text-left">
                                    <div class="wrap_p">
                                        <p><?php echo $row['guest_name']?><?php echo ($row['age'] == 0) ? "" : ', '.$row['age']?> <?php ($row['age'] == 0) ? "" : write_years($row['age']);?></p>
                                        <br>
                                        <div class="desc" style="display: inline;"><p style="display: inline-block;font-size: 14px; text-align: justify; text-align-last: justify;"><?php echo findSpace($row['text_comment'], $row['id_comment'])?></p></div>
                                        <div id="collapseOne<?php echo $row['id_comment']?>" class="panel-collapse collapse">
                                            <p style="display: inline;font-size: 14px; text-align: justify; width: 100%;"><?php echo echoStr($row['text_comment'])?></p>
                                        </div>
                                        <a class="arr_circle" data-toggle="collapse" href="#collapseOne<?php echo $row['id_comment']?>"></a>
                                    </div>
                                </div>
                            </div>

                        <?php }else{?>

                            <div class="row">
                                <div class="col-sm-4">

                                    <a class="fancybox-thumb" href="<?php if(isset($row['comment_img']) && $row['comment_img'] != null){ echo ("/uploads/comment/".date('d.m.Y_H-i-s',$row['created_at']) ."/wtmarked_". $row['comment_img']);}else{ echo "/uploads/nophoto.jpg";}?>" class="block_img">
                                        <img class="img img-responsive img-circle img_house" src="<?php if(isset($row['comment_img']) && $row['comment_img'] != null){ echo "/uploads/comment/".date('d.m.Y_H-i-s',$row['created_at']) ."/small_". $row['comment_img'];}else{echo "/uploads/nophoto.jpg";}?>" alt="<?php if(isset($row['comment_img']) && $row['comment_img'] != null){ echo $row['comment_img']."_".$row['id_comment'];}else{echo "nophoto";}?>">
                                        <?php if(isset($row['comment_img']) && $row['comment_img'] != null){?><img class="img img-responsive img-circle only_circle" src="/images/inner/blue_circle.png" alt="blue_circle"><?php }?>
                                    </a>
                                </div>
                                <div class="col-sm-8 testims text-left">
                                    <div class="wrap_p">
                                        <p><?php echo $row['guest_name']?><?php echo ($row['age'] == 0) ? "" : ', '.$row['age']?> <?php ($row['age'] == 0) ? "" : write_years($row['age']);?></p>
                                        <br>
                                        <p><?php echo $row['text_comment']?></p>
                                    </div>
                                </div>
                            </div>
                        <?php }?>

                        <?php
                    endforeach;
                    ?>

                </div>
                <div class="text-center">
                    <?php echo \yii\widgets\LinkPager::widget([
                        'pagination' => $pages,
                        'maxButtonCount' => 5
                    ]) ?>
                </div>

            <?php }else{ ?>

                <div class="col-sm-12 our_projects testimonials text-center">

                    <h3>Здесь пока никто ничего не написал...</h3>

                </div>

            <?php }?>

        </div><!-- col-sm-12 -->
    </div><!-- row -->
    </div><!-- container -->
</section><!-- sky -->

<script type="text/javascript" src="http://yandex.st/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        var arrALinks = $('a.fancybox-thumb');
        for(var t = 0; t < arrALinks.length; t++){
            var nameLink = arrALinks[t].href.split('/wtmarked_',-1);
            var idLink = arrALinks[t].href.split('/',-1);
            var str_nameLink = '';
            var str_idLink = '';

            for (var s = nameLink.length-1; s < nameLink.length; s++) {
                str_nameLink = nameLink[s];
            }

            for (var f = idLink.length-2; f < idLink.length-1; f++) {
                str_idLink = idLink[f];
            }

            arrALinks[t].href = "/main/main/manywatermarktestim/?id="+str_idLink+"&wsc="+screenWidthJs+"&hsc="+screenHeightJs+"&n=" + str_nameLink;;
        }

        $('.fancybox-thumb')
            .fancybox({
                padding : 0
            });

    });
</script>