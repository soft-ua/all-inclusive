<?php $this->title = 'All Inclusive';?>
<section id="bg_video" class="sky home">
    <video autoplay loop muted>
        <source src="resource/video/movie.mp4" type="video/mp4">
    </video>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <img class="img img-responsive margin_auto padding_top logo_img2" src="/images/main/all_inclusive.png">
                <p class="text_upper text_bold text-center font_18 text_title">Cтроительство и продажа готовых домов</p>
                <a class="main_img" href="/list">
                    <div class="col-xs-2 padding_none"><img class="img img-responsive fork" src="/images/main/fork.png"></div>
                    <div class="col-xs-8 padding_none"><img class="img img-responsive margin_auto img_plate" src="/images/main/house_plate.png"></div>
                    <div class="col-xs-2 padding_none"><img class="img img-responsive knife" src="/images/main/knife.png"></a></div>
            </div>
        </div>
    </div>
</section>