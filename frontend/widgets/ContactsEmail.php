<?php

namespace frontend\widgets;

use common\models\ContactsInfo;
use common\models\LoginForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Widget;
use yii\web\Response;
use yii\helpers\Url;
use yii\db\Query;

class ContactsEmail extends Widget{

    public function run(){

        $model = ContactsInfo::find()->all();

        return $this->render("contacts_email", ['model' => $model]);

    }
}