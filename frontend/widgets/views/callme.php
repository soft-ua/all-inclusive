

<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Large modal</button>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div id="modal">

                <div class="">
                    <?php $form = \yii\bootstrap\ActiveForm::begin(['id' => 'callme']); ?>

                    <?= $form->field($model, 'name') ?>

                    <?= $form->field($model, 'phone') ?>

                    <div class="form-group">
                        <?= \yii\bootstrap\Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                    <?php \yii\bootstrap\ActiveForm::end(); ?>
                </div>

            </div>
        </div>
    </div>
</div>