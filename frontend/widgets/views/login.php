<div id="loginpop" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
            <div class="row">
                <div class="col-sm-6 login">
                    <h4>Войти своим пользователем</h4>


                    <?php
                    $form = \yii\bootstrap\ActiveForm::begin([
                        'enableAjaxValidation' => true,
                        'validationUrl' => \yii\helpers\Url::to(['/validate/index']),
                    ]);
                    ?>

                    <?=$form->field($model,'username') ?>
                    <?=$form->field($model,'password')->passwordInput() ?>
                    <?=$form->field($model,'rememberMe')->checkbox() ?>

                    <?=\yii\helpers\Html::submitButton('Войти',['class' => 'btn btn-success']) ?>


                    <?php
                    \yii\bootstrap\ActiveForm::end();
                    ?>

                </div>
                <div class="col-sm-6">
                    <h4>Зарегистрировать нового пользователя</h4>
                    <button type="submit" class="btn btn-info"  onclick="window.location.href='<?=\yii\helpers\Url::to('main/main/registration/') ?>'">Зарегистрироваться сейчас</button>
                </div>

            </div>
        </div>
    </div>
</div>