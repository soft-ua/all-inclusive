<?php
use yii\bootstrap\Nav;
?>
<div class="navbar" role="navigation">
    <div class="navbar-header">

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

    </div>


    <!-- Nav Starts -->
    <div class="block_menu">
        <div class="container">
            <div class="navbar-collapse collapse padding_none">
                <?php
                $menuItems = [
                    ['label' => 'Главная страница', 'url' => '/'],
                    ['label' => 'О нас', 'url' => '/about'],
                    ['label' => 'Готовые дома к продаже', 'url' => '/list'],
                    ['label' => 'Реализованные проекты', 'url' => '/list-projects'],
                    ['label' => 'Отзывы', 'url' => ['/testimonials']],
                    ['label' => 'Дари добро', 'url' => ['/give-good']],
                    ['label' => 'Контакты', 'url' => '/contact'],
                ];

        //        $guest = Yii::$app->user->isGuest;
        //        if($guest) {
        //            $menuItems[] =  ['label' => 'Войти', 'url' => '#', 'linkOptions' => ['data-target' => '#loginpop', 'data-toggle' => "modal"]];
        //        }
        //        else{
        ////                        $menuItems[] =  ['label' => 'Админ панель', 'url' => '/#'];
        //            $menuItems[] = ['label' => 'Выйти',  'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']];
        //        }
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav nav-justified'],
                    'items' => $menuItems,
                ]);
                ?>

            </div>
            <!-- #Nav Ends -->
        </div>
    </div>
</div>
