<?php

namespace frontend\widgets;

use common\models\LoginForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Widget;
use yii\web\Response;
use yii\helpers\Url;
use yii\db\Query;
use yii\web\Controller;

class Callme extends Widget{

    public function run(){
        $model = new \frontend\models\Callme();

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $name = \Yii::$app->request->post("Callme")['name'];
            $phone = \Yii::$app->request->post("Callme")['phone'];

            if (mail(\Yii::$app->params['adminEmail'],'Заказ обратного звонка на сайте All Inclusive '.date('d.m.Y H:i:s', time()), "\n
Посетитель: ".$name." заказал обратный звонок! \n
Телефон : ".$phone)) {
                \Yii::$app->session->setFlash('success', 'Вашa заявка была отправлена. Мы Вам перезвоним в ближайщее время!');
            } else {
                \Yii::$app->session->setFlash('error', 'Произошла ошибка при отправке заявки. Повторите попытку попозже!');
            }

            \Yii::$app->controller->refresh();
        } else {
            return $this->render('callme', [
                'model' => $model,
            ]);
        }

    }
}