<?php

namespace frontend\widgets;

use common\models\ContactsInfo;
use yii\bootstrap\Widget;

class ContactsAPhone extends Widget{

    public function run(){

        $model = ContactsInfo::find()->all();

        return $this->render("contacts_aphone",['model' => $model]);
    }
}