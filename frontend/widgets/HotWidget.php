<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\db\Query;

class HotWidget extends Widget{

    private $_query;

    public function init(){
        $this->_query = new Query();
    }

    public function run(){

        $query = $this->_query;
        $result = $query->from('image')
            ->innerJoin('house', 'house.id = image.entity_id')
            ->andWhere('house.type = 1')
            ->andWhere('image.main = 1')
            ->andWhere('house.available = 1')
            ->andWhere('image.available = 1')
            ->orderBy('house.id desc')
                        ->limit(5)->all();




        return $this->render('hot',['result' => $result]);
    }
}