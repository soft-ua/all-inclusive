<?php

namespace frontend\widgets;

use common\models\ContactsInfo;
use yii\bootstrap\Widget;

class ContactsPhone extends Widget{

    public function run(){

        $model = ContactsInfo::find()->all();

        return $this->render("contacts_phone",['model' => $model]);
    }
}