<?php

namespace frontend\widgets;

use common\models\LoginForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Widget;
use yii\web\Response;
use yii\helpers\Url;
use yii\db\Query;

class Menu extends Widget{

    public function run(){

        return $this->render("menu");

    }
}