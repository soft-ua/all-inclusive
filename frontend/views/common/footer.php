<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p class="text_upper text_bold text-center font_18 ">Продажа домов: <a href="tel:<?php echo \frontend\widgets\ContactsAPhone::widget();?>"><?php echo \frontend\widgets\ContactsPhone::widget();?></a></p>
            </div>
        </div>
    </div>

    <div class="social text-center more_350">

        <div class="insta_block">
            <a href="https://www.instagram.com" target="_blank" title="Instagram" class="pluso-instagram"><img src="/images/main/instagram.png" ></a>
        </div>

        <script type="text/javascript">
            (function() {
                if (window.pluso)if (typeof window.pluso.start == "function") return;
                if (window.ifpluso==undefined) { window.ifpluso = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                    var h=d[g]('body')[0];
                    h.appendChild(s);
                }})();
        </script>
        <div class="pluso" data-background="transparent" data-options="big,square,line,horizontal,nocounter,theme=03" data-services="vkontakte,twitter,facebook,google,instagram,email,print"></div>
    </div>

    <div class="social text-center less_350">

<!--        <div class="insta_block">-->
<!--            <a href="https://www.instagram.com" target="_blank" title="instagram" class="pluso-instagram"><img src="/images/main/instagram.png" ></a>-->
<!--        </div>-->

        <script type="text/javascript">
            (function() {
                if (window.pluso)if (typeof window.pluso.start == "function") return;
                if (window.ifpluso==undefined) { window.ifpluso = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                    var h=d[g]('body')[0];
                    h.appendChild(s);
                }})();
        </script>
        <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=03" data-services="vkontakte,twitter,facebook,google,instagram,email,print"></div>
    </div>

</footer>

<script type='text/javascript'>
    (function(){
        var widget_id = '5488396177';
        var s = document.createElement('script');
        s.type = 'text/javascript'; s.async = true;
        s.src = 'http://сайтофициально.рф/script/js/callback/'+widget_id+'/callback_st_of.js';
        var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);
    })();
</script>