<?php
use yii\bootstrap\Nav;
?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-4"><a href="/"><img class="img img-responsive logo" src="/images/main/logo.png"></a></div>
            <div class="col-sm-4 padding_top">
                <p class="inline">
                    <span class="goal"></span>
                    <span class="text_upper text_bold font_18">Наша цель:</span><br>
                    теплый, уютный, полностью обставленный и со всеми коммуникациями дом, где можно жить уже в день приобретения!
                </p>
            </div>
            <div class="col-sm-4 padding_top font_18 text-right">
                <p class="ph">
                    <span class="phone"></span>
                    <a href="tel:<?php echo \frontend\widgets\ContactsAPhone::widget();?>"><?php echo \frontend\widgets\ContactsPhone::widget();?></a>
                </p>
                <p class="em"><span class="envelope"></span>
                    <a href="mailto:<?php echo \frontend\widgets\ContactsEmail::widget();?>"><?php echo \frontend\widgets\ContactsEmail::widget();?></a>
                </p>
            </div>
        </div>
    </div>
</header>