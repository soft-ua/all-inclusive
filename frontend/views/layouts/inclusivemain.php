<?php
    use \yii\helpers\Html;

    \frontend\assets\MainAsset::register($this);
?>
<?php $this->beginPage();?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?php \Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Favicons -->
    <link rel="shortcut icon" href="/images/main/favicon.ico" type="image/x-icon" />
<!--    <link rel="shortcut icon" href="/images/main/favicon_32x32.ico" type="image/x-icon" />-->
<!--    <link rel="apple-touch-icon" href="/images/main/favicon_57x57.ico">-->
<!--    <link rel="apple-touch-icon" sizes="72x72" href="/images/main/favicon_72x72.ico">-->
<!--    <link rel="apple-touch-icon" sizes="114x114" href="/images/main/favicon_114x114.ico">-->

    <?php echo Html::csrfMetaTags()?>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody();?>

<?php
if(Yii::$app->session->hasFlash('success')):
    ?>

    <?php
    $success = \Yii::$app->session->getFlash('success');
    if(\Yii::$app->session->getFlash('success')){
        echo \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-info',
                'style' => ['margin-bottom'=>'0px','text-align'=>'center'],
            ],
            'body' => $success,
        ]);
    }


    $error = \Yii::$app->session->getFlash('error');
    if(\Yii::$app->session->getFlash('error')){
        echo \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-danger',
                'style' => ['margin-bottom'=>'0px','text-align'=>'center'],
            ],
            'body' => $error,
        ]);
    }
    ?>

    <?php
endif;
?>

<?php echo $this->render('//common/header') ?>

<?php echo $content; ?>

<?php echo $this->render('//common/footer') ?>

<?php $this->endBody();?>
</body>
</html>
<?php $this->endPage();?>




